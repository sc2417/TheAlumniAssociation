package com.example.spring_security;

import java.util.List;

/**
 * Created by Administrator 2018/8/8 12:53
 */
public interface PermissionDao {
    public List<Permission> finAll();

    public List<Permission> findByAdminUserId(int userId);


}