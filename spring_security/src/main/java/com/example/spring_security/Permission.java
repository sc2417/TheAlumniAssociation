package com.example.spring_security;

/**
 * Created by Administrator 2018/8/8 12:49
 */
public class Permission {
    private int id;

    private String name;

    private String descritpion;

    private String url;

    private int pid;


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescritpion() {
        return descritpion;
    }

    public String getUrl() {
        return url;
    }

    public int getPid() {
        return pid;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescritpion(String descritpion) {
        this.descritpion = descritpion;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }
}