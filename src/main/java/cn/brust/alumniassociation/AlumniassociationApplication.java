package cn.brust.alumniassociation;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@SpringBootApplication
@ServletComponentScan
@EnableRedisHttpSession
@MapperScan("cn.brust.alumniassociation.mapper")
public class AlumniassociationApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlumniassociationApplication.class, args);
    }
}
