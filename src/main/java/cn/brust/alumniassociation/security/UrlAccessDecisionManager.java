package cn.brust.alumniassociation.security;

import cn.brust.alumniassociation.entity.Permission;
import cn.brust.alumniassociation.mapper.PermissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Created by yangyibo on 17/1/19.
 */
@Service
public class UrlAccessDecisionManager implements AccessDecisionManager {

    @Autowired
    PermissionMapper permissionMapper;

    //数据库里所有的权限url
    List<UrlGrantedAuthority> list = null;

    //初始化
    public void init(){
        List<Permission> permissions = permissionMapper.selectAll();
        list = new ArrayList<>();
        permissions.forEach(permission -> {
            UrlGrantedAuthority grantedAuthority = new UrlGrantedAuthority(permission.getUrl(),permission.getMethod());
            list.add(grantedAuthority);
        });
    }

    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        HttpServletRequest request = ((FilterInvocation) object).getHttpRequest();
        String url, method;
        if ("anonymousUser".equals(authentication.getPrincipal())
                || matchers("/images/**", request)
                || matchers("/js/**", request)
                || matchers("/css/**", request)
                || matchers("/fonts/**", request)
                || matchers("/", request)
                || matchers("/index.html", request)
                || matchers("/favicon.ico", request)
                || matchers("/login", request)) {
            return;
        } else {
            if(list == null){
                init();
            }

            for (GrantedAuthority ga : authentication.getAuthorities()) {
                if (ga instanceof UrlGrantedAuthority) {
                    UrlGrantedAuthority urlGrantedAuthority = (UrlGrantedAuthority) ga;
                    url = urlGrantedAuthority.getPermissionUrl();
                    method = urlGrantedAuthority.getMethod();
                    if (matchers(url, request)) {
                        if (method.equals(request.getMethod()) || "ALL".equals(method)) {
                            return;
                        }
                    }
                }
            }
            //如果运行到这里 证明当前URL是用户没有的权限  这里判断当前URL是否是权限表里的 如果不是则放行 是的话则抛异常
            boolean isPermission = true;
            for (UrlGrantedAuthority urlGrantedAuthority : list) {
                url = urlGrantedAuthority.getPermissionUrl();
                method = urlGrantedAuthority.getMethod();
                if(url.equals("/**")){
                    continue;
                }
                if (matchers(url, request)) {
                    if (method.equals(request.getMethod()) || "ALL".equals(method)) {
                        isPermission = false;
                        continue;
                    }
                }
            }
            if(isPermission){
                return;
            }
        }
        throw new AccessDeniedException("no right");
    }


    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }


    private boolean matchers(String url, HttpServletRequest request) {
        AntPathRequestMatcher matcher = new AntPathRequestMatcher(url);
        if (matcher.matches(request)) {
            return true;
        }
        return false;
    }
}
