package cn.brust.alumniassociation.security;

import cn.brust.alumniassociation.entity.Member;
import cn.brust.alumniassociation.entity.Permission;
import cn.brust.alumniassociation.entity.User;
import cn.brust.alumniassociation.mapper.MemberMapper;
import cn.brust.alumniassociation.mapper.PermissionMapper;
import cn.brust.alumniassociation.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 郭逸峰
 * @since
 * @since
 */
@Service
public class UrlUserService implements UserDetailsService {

    @Autowired
    MemberMapper memberMapper;

    @Autowired
    PermissionMapper permissionMapper;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Member member = memberMapper.findMemberPhone(name);
        if(member != null){
            System.out.println(member);
            List<Permission> permissions = permissionMapper.selectAllByMemberId(member.getId());
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            for (Permission permission : permissions) {
                if (permission != null && permission.getName() != null) {
                    GrantedAuthority grantedAuthority = new UrlGrantedAuthority(permission.getUrl(), permission.getMethod());
                    grantedAuthorities.add(grantedAuthority);
                }
            }
            return new org.springframework.security.core.userdetails.User(member.getId(), new BCryptPasswordEncoder().encode(member.getPassword()), grantedAuthorities);

        }else{
            throw new UsernameNotFoundException("admin: " + name + " do not exist!");
        }
    }
}
