package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 微信端用户表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    /**
     * 主键
     */
    private String wxId;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 用户真实姓名
     */
    private String name;

    /**
     * 登陆密码
     */
    private String passwd;

    /**
     * 性别
     */
    private String sex;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 用户所属班级
     */
    private Integer classId;

    /**
     * 班级角色(用户随意填写)
     */
    private String classRole;

    /**
     * 城市
     */
    private String city;

    /**
     * 用户来源
     */
    private String from;

    /**
     * 审核状态
     */
    private Integer auditStatus;

    /**
     * 是否显示
     */
    private Integer isEnable;

    /**
     * 登陆总次数
     */
    private Integer loginTimes;

    /**
     * 最后登陆时间
     */
    private Date lastLogin;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 更新时间
     */
    private Date updateDate;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 软删时间
     */
    private Date delDate;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;
}