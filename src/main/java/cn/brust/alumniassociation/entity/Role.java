package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 角色表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    /**
     * 主键
     */
    private String id;

    /**
     * 角色名
     */
    private String name;

    /**
     * 为0或者为空代表非平台管理角色
     */
    private String classId;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 是否启用
     */
    public Integer isEnable;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}