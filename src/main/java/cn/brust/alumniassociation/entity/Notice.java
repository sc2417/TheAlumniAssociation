package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 系统公告表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notice {
    /**
     * 主键
     */
    private String id;

    /**
     * 发布人员(逻辑关联成员表)
     */
    private String memberId;

    /**
     * 公告标题
     */
    private String title;

    /**
     * 公告内容
     */
    private String content;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 更新日期
     */
    private Date updateDate;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 软删日期
     */
    private Date delDate;

    /**
     * 创建人
     */
    private String createPsrson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}