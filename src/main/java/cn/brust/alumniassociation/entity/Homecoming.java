package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 同学会介绍表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Homecoming {
    /**
     * 主键
     */
    private String id;

    /**
     * 学院介绍
     */
    private String details;

    /**
     * 学院介绍是否显示
     */
    private Integer detailsEnable;

    /**
     * 同学会介绍
     */
    private String classmate;

    /**
     * 同学会介绍是否显示
     */
    private Integer classmateEnable;

    /**
     * 同学会章程
     */
    private String statutes;

    /**
     * 同学会章程是否显示
     */
    private Integer statusEnable;

    /**
     * 修改日期
     */
    private Date updateDate;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}