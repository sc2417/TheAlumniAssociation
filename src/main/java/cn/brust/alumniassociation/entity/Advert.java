package cn.brust.alumniassociation.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 广告表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Advert {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 广告标题
     */
    private String title;

    /**
     * 广告位置
     */
    private Integer site;

    /**
     * 广告封面
     */
    private String cover;

    /**
     * 广告链接
     */
    private String url;

    /**
     * 广告备注
     */
    private String desc;

    /**
     * 是否显示
     */
    private Integer isStatus;

    /**
     * 访问量
     */
    private Integer visit;

    /**
     * 创建日期(上线时间)
     */
   @JsonFormat(pattern="yyyy-MM-dd")
    private Date cteateDate;

    /**
     * 是否置顶
     */
    private Integer isTop;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 软删日期
     */
    private Date delDate;

    /**
     * 更新日期
     */
    private Date updateDate;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

    /**
     *
     */
    private Integer counts;
}