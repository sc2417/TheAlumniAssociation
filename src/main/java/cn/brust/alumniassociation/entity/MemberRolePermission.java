package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberRolePermission {
    /**
     * 成员角色权限ID
     */
    private Integer id;
    /**
     * 成员ID
     */
    private Integer memberId;
    /**
     * 成员角色ID
     */
    private Integer memberRoleId;
    /**
     * 权限ID
     */
    private Integer permissionId;

}