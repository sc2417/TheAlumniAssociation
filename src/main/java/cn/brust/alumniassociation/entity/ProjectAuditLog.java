package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 项目审核表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectAuditLog {
    /**
     * 主键
     */
    private String id;

    /**
     * 项目ID
     */
    private String projectId;

    /**
     * 审核详情
     */
    private String msg;

    /**
     * 审核状态
     */
    private Integer auditStatus;

    /**
     * 审核时间
     */
    private Date auditDate;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}