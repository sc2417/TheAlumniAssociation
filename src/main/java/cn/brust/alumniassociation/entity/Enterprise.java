package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 企业表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Enterprise {
    /**
     * 主键
     */
    private String id;

    /**
     * 企业LOGO
     */
    private String logo;

    /**
     * 企业名称
     */
    private String name;

    /**
     * 企业网站
     */
    private String website;

    /**
     * 企业电话1
     */
    private String tel1;

    /**
     * 企业电话2
     */
    private String tel2;

    /**
     * 主营业务
     */
    private String business;

    /**
     * 企业简介
     */
    private String introduction;

    /**
     * 企业地址
     */
    private String address;

    /**
     * 企业所属班级(逻辑关联班级表)
     */
    private String classId;

    /**
     * 企业推介人(逻辑关联用户表)
     */
    private String userId;

    /**
     * 职位
     */
    private String positions;

    /**
     * 手机号码1
     */
    private String phone1;

    /**
     * 手机号码2
     */
    private String phone2;

    /**
     * 访问量
     */
    private Integer visit;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 添加时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 状态(0未发布  1发布中)
     */
    private Integer status;

    /**
     * 审核状态
     */
    private Integer auditStatus;

    /**
     * 是否显示
     */
    private Integer isEnable;

    /**
     * 是否置顶
     */
    private Integer isTop;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 软删日期
     */
    private Date delDate;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}