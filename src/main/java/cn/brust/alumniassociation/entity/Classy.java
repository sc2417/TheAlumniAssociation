package cn.brust.alumniassociation.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 班级表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Classy {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 班级名称
     */
    private String name;

    /**
     * 班级管理员(逻辑链接t_user表)
     */
    private String userId;

    /**
     * 是否启用
     */
    private Integer isEnable;

    /**
     * 创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd ")
    private Date createDate;

    /**
     * 软删时间
     */
    private Date delDate;

    /**
     * 修改时间
     */
    private Date updateDate;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

    /**
     * 管理员名字
     */
    private String uname;

    /**
     * 管理员电话
     */
    private String umobile;
    /**
     * 管理员邮件
     */
    private String uemail;
    /**
     * 管理员密码
     */
    private String upasswd;
    /**
     * 管理员备注
     */
    private String udescribe;
    /**
     * 班级人员
     */
    private Integer cnumber;
    /**
     * 管理员Id
     */
    private String wxId;
}