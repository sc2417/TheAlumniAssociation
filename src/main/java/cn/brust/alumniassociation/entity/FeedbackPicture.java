package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 反馈图片中间表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackPicture {
    /**
     * 主键
     */
    private String id;

    /**
     * 图片ID
     */
    private String pictureId;

    /**
     * 反馈表主键
     */
    private String feedbackId;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}