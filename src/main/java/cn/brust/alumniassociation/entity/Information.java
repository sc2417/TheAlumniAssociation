package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 资讯表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Information {
    /**
     * 主键
     */
    private String id;

    /**
     * 资讯标题
     */
    private String title;

    /**
     * 资讯封面
     */
    private String cover;

    /**
     * 文章来源
     */
    private String from;

    /**
     * 外部URL
     */
    private String url;

    /**
     * 资讯正文
     */
    private String content;

    /**
     * 班级外键
     */
    private String classId;

    /**
     * 资讯分类外键
     */
    private String topicId;

    /**
     * 访问量
     */
    private Integer visit;

    /**
     * 转发量
     */
    private Integer share;

    /**
     * 是否显示
     */
    private Integer isEnable;

    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 软删日期
     */
    private Date delDate;

    /**
     * 更新日期
     */
    private Date updateDate;

}