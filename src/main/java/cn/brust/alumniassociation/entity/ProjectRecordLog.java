package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 项目跟进表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectRecordLog {
    /**
     * 主键
     */
    private String id;

    /**
     * 项目ID
     */
    private String projectId;

    /**
     * 跟进详情
     */
    private String msg;

    /**
     * 跟进状态
     */
    private Integer status;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}