package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 活动表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Activity {
    /**
     * 主键
     */
    private String id;

    /**
     * 活动封面
     */
    private String cover;

    /**
     * 活动地点
     */
    private String address;

    /**
     * 活动标题
     */
    private String title;

    /**
     * 活动性质
     */
    private String type;

    /**
     * 阅读量
     */
    private Integer visit;

    /**
     * 点赞数
     */
    private Integer zan;

    /**
     * 关联班级
     */
    private Integer classId;

    /**
     * 报名状态
     */
    private Integer status;

    /**
     * 是否显示
     */
    private Integer isEnable;

    /**
     * 是否软删
     */
    private Integer isDel;

    /**
     * 软删时间
     */
    private Date delDate;

    /**
     * 活动发布时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private String createDate;

    /**
     * 活动开始时间
     */
    private String startDate;

    /**
     * 活动结束时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private String endDate;

    /**
     * 活动修改时间
     */
    private Date updateDate;

    /**
     * 活动人数
     */
    private Integer staff;

    /**
     * 转发量
     */
    private Integer forward;

    /**
     * 描述
     */
    private String describe;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 活动内容
     */
    private String content;

}