package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassRole {
    /**
     * 班级职务ID
     */
    private Integer id;

    /**
     * 班级ID
     */
    private Integer classId;
    /**
     * 班级职务名称
     */
    private  String name;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 删除时间
     */
    private Date delTime;
    /**
     * 软删标识列
     */
    private Integer del;
}