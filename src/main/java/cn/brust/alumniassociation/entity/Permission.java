package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 权限表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Permission {
    /**
     * 主键
     */
    private String id;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 权限描述
     */
    private String desc;

    /**
     * 授权链接
     */
    private String url;

    /**
     * 父节点ID
     */
    private String pid;

    /**
     * 请求类型
     */
    private String method;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}