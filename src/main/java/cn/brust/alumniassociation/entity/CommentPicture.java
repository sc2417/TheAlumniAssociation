package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 评论图片中间表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentPicture {
    /**
     * 主键
     */
    private String id;

    /**
     * 评论表ID
     */
    private String commentId;

    /**
     * 图片表ID
     */
    private String pictureId;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}