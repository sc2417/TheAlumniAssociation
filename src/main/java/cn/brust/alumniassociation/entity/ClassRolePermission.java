package cn.brust.alumniassociation.entity;

/**
 * Created by Administrator 2018/8/8 15:07
 */
public class ClassRolePermission {
    /**
     * 班级职务权限ID
     */
    private  Integer id;
    /**
     * 班级职务ID
     */
    private Integer classRoleId;
    /**
     * 权限ID
     */
    private  Integer permissionId;

    /**
        用户ID
     */
    private String userId;
}