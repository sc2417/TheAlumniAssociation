package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 平台成员表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Member {
    /**
     * 主键
     */
    private String id;

    /**
     * 班级ID
     */
    private String classId;
    /**
     * `头像
     */
    private String avatar;

    /**
     * 成员姓名
     */
    private String name;

    /**
     * 成员昵称
     */
    private String nickname;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 登陆密码
     */
    private String password;

    /**
     * 备注信息
     */
    private String desc;

    /**
     * 最后登陆时间
     */
    private Date lastLogin;

    /**
     * 是否显示
     */
    private Integer isEnable;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 更新日期
     */
    private Date updateDate;

    /**
     * 软删时间
     */
    private Date delDate;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}