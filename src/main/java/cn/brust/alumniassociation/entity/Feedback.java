package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 用户反馈表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Feedback {
    /**
     * 主键
     */
    private String id;

    /**
     * 反馈内容
     */
    private String opinion;

    /**
     * 提交人(关联用户表)
     */
    private String userId;

    /**
     * 反馈时间(提交时间)
     */
    private Date createDate;

    /**
     * 处理状态
     */
    private Integer status;

    /**
     * 处理时间
     */
    private Date processDate;

    /**
     * 处理意见
     */
    private String reply;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 软删日期
     */
    private Date delDate;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}