package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 附件表(可共用)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Enclosure {
    /**
     * 主键
     */
    private String id;

    /**
     * 附件URL地址
     */
    private String enclosureUrl;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}