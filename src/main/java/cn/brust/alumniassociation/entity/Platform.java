package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 平台信息表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Platform {
    /**
     * 主键
     */
    private String id;

    /**
     * 平台LOGO
     */
    private String logo;

    /**
     * 平台名称
     */
    private String name;

    /**
     * 平台标题
     */
    private String title;

    /**
     * 平台热线
     */
    private String tel;

    /**
     * 平台邮箱
     */
    private String email;

    /**
     * 平台QQ
     */
    private String qq;

    /**
     * 更新日期
     */
    private Date updateDate;

    /**
     * 主句状态
     */
    private Integer state;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

    /**
     * 用户协议
     */
    private String agreement;

}