package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 资讯分类表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Topic {
    /**
     * 主键
     */
    private String id;

    /**
     * 分类名称
     */
    private String title;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否显示
     */
    private Integer isEnable;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 更新日期
     */
    private Date updateDate;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}