package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 用户审核记录表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAuditLog {
    /**
     * 主键
     */
    private String id;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 审核详情
     */
    private String msg;

    /**
     * 审核状态
     */
    private Integer auditStatus;

    /**
     * 审核人员
     */
    private String auditPerson;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

    /**
     * 数据状态
     */
    private Integer state;

}