package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 项目表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Project {
    /**
     * 主键
     */
    private String id;

    /**
     * 项目简介
     */
    private String introduction;

    /**
     * 项目预算
     */
    private Integer budget;

    /**
     * 项目附件
     */
    private String enclosure;

    /**
     * 其他要求
     */
    private String other;

    /**
     * 项目所属班级(逻辑关联班级表)
     */
    private Integer classId;

    /**
     * 项目发布人(逻辑关联用户表)
     */
    private String userId;

    /**
     * 访问量
     */
    private Integer visit;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 跟进记录(JSON存储)
     */
    private String record;

    /**
     * 审核状态
     */
    private Integer auditStatus;

    /**
     * 审核记录(JSON存储)
     */
    private String auditLog;

    /**
     * 是否显示
     */
    private Integer enable;

    /**
     * 是否置顶
     */
    private Integer top;

    /**
     * 发布时间
     */
    private Date createTime;

    /**
     * 项目开始时间
     */
    private Date startTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 项目结束时间
     */
    private Date endTime;

    /**
     * 是否软删
     */
    private Integer del;

    /**
     * 软删时间
     */
    private Date delTime;
}