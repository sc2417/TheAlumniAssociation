package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 活动报名表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Enroll {
    /**
     * 主键
     */
    private String id;

    /**
     * 报名用户ID
     */
    private String wxId;

    /**
     * 活动ID
     */
    private String activityId;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}