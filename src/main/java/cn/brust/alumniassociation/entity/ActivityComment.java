package cn.brust.alumniassociation.entity;

/**
 * 活动评论表
 */
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityComment {
    /**
     * 主键
     */
    private String id;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 活动ID
     */
    private String activityId;

    /**
     * 评论用户姓名
     */
    private String name;

    /**
     * 评论级别表示(一级 二级)
     */
    private Integer level;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 点赞数
     */
    private Integer fabulous;

    /**
     * 评论XX用户ID
     */
    private String father;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

    /**
     * 创建日期
     */
    private Date createDate;

}