package cn.brust.alumniassociation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 项目附件中间表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectEnclosure {
    /**
     * 主键
     */
    private String id;

    /**
     * 附件表ID
     */
    private String enclosureId;

    /**
     * 项目表
     */
    private String projectId;

    /**
     * 数据状态
     */
    private Integer state;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String describe;

}