package cn.brust.alumniassociation.utils;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *  多图上传
 * @author 孙超
 * @since 1.0
 * @since JDK 8.0
 * @return : null
 */
@Controller
public class UploadImage {

    @PostMapping("/testController/uploadImg")
    @ResponseBody
    public List<String> uploadImg(MultipartFile file[], String areaName,HttpServletRequest request) throws Exception {
        // 设置上传的路径是D盘下的picture
        ServletContext context = request.getSession().getServletContext();
        String realPath = context.getRealPath("/upload/activity/");
        List<String> strings = new ArrayList<>();
        int i = 0;
        for (MultipartFile f : file) {
            i++;
            // 图片的名字用毫秒数+图片原来的名字拼接
            System.out.println(file.length);
            String imgName = System.currentTimeMillis() + f.getOriginalFilename();
            //上传文件
            strings.add(uploadFileUtil(f.getBytes(), realPath, imgName));
            if(i == file.length){
                return strings;
            }
        }
        return strings;
    }

    /**
     * 上传文件的方法
     * @param file：文件的字节
     * @param imgPath：文件的路径
     * @param imgName：文件的名字
     * @throws Exception
     */
    public String uploadFileUtil(byte[] file, String imgPath, String imgName) throws Exception {
        File targetFile = new File(imgPath);
        String s = imgPath+imgName;
        System.out.println(s);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        FileOutputStream out = new FileOutputStream(imgPath + imgName);
        out.write(file);
        out.flush();
        out.close();
        return s;
    }
}

