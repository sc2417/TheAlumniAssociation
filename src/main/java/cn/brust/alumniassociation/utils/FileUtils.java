package cn.brust.alumniassociation.utils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;
/**
 * <pre>
 *     @author 马敏婷
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public class FileUtils {
	public static String SEPARATOR = "_";
	/**
	 * 
	 * @param file 文件
	 * @param request 
	 * @param path	要放在哪个文件夹
	 * @return upload/+path/+file
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public static String saveFile(MultipartFile file,HttpServletRequest request,String path) throws IllegalStateException, IOException {
        if (file == null || file.isEmpty()) return null;
        ServletContext context = request.getSession().getServletContext();
        String realPath = context.getRealPath("/upload/"+path);
        File f = new File(realPath + "/" + UUID.randomUUID() + SEPARATOR + file.getOriginalFilename());
        if(!f.getParentFile().exists()) {
        	f.getParentFile().mkdirs();
        }
        file.transferTo(f);
        return "/upload/"+path+"/" + f.getName();
    }
}
