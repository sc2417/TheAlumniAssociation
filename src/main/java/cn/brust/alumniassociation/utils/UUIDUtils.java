package cn.brust.alumniassociation.utils;

import java.util.UUID;

/**  <pre>
 * UUID
 * @author 郭逸峰
 * @since 1.0
 * @since JDK 8.0
 * </pre>
 */
public class UUIDUtils {
    public static String getUUID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
}
