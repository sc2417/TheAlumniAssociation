package cn.brust.alumniassociation.utils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Component
public class SendTemplate {
    public static boolean sendTemplateMsg(Template template) throws IOException {
        AccessToken accessToken = GetAccess.getAccessToken();
        System.out.println(accessToken.getToken());
        boolean flag=false;
        String requestUrl="https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        requestUrl=requestUrl.replace("ACCESS_TOKEN", accessToken.getToken());
        JSONObject jsonResult=CommonUtil.httpsRequest(requestUrl, "POST", template.toJSON());
        System.out.println(jsonResult);
        System.out.println(template.toJSON());
        if(jsonResult!=null){
            int errorCode=Integer.parseInt(jsonResult.getString("errcode"));
            String errorMessage=jsonResult.getString("errmsg");
            if(errorCode==0){
                flag=true;
            }else{
                System.out.println("模板消息发送失败:"+errorCode+","+errorMessage);
                flag=false;
            }
        }
        return flag;
    }
    /**
     * 注册模块通用模板
     * @param openID : 用户openID
     * @param templateID : 微信模板ID
     * @param first
     * @param key2
     * @param remark
     * @return
     */
    public Template register(String openID,String templateID,String first,String key1,String key2,String remark){
        Template tem=new Template();
        tem.setTemplateId(templateID);
        tem.setTopColor("#00DD00");
        tem.setToUser(openID);
        tem.setUrl("http://weixin.qq.com/download");
        List<TemplateParam> paras=new ArrayList<TemplateParam>();
        paras.add(new TemplateParam("first",first,"#FF3333"));
        paras.add(new TemplateParam("keyword1",key1,"#0044BB"));
        paras.add(new TemplateParam("keyword2",key2,"#0044BB"));
        paras.add(new TemplateParam("remark",remark,"#AAAAAA"));
        tem.setTemplateParamList(paras);
        return tem;
    }
    /**
     * 项目审核通用模板
     * @param openID : 用户openID
     * @param templateID : 微信模板ID
     * @param first
     * @param key1
     * @param key2
     * @param remark
     * @return
     */
    public Template projectAudit(String openID,String templateID,String first,String key1,String key2,String remark){
        Template tem=new Template();
        tem.setTemplateId(templateID);
        tem.setTopColor("#00DD00");
        tem.setToUser(openID);
        tem.setUrl("http://weixin.qq.com/download");
        List<TemplateParam> paras=new ArrayList<TemplateParam>();
        paras.add(new TemplateParam("first",first,"#FF3333"));
        paras.add(new TemplateParam("keyword1",key1,"#0044BB"));
        paras.add(new TemplateParam("keyword2",key2,"#0044BB"));
        paras.add(new TemplateParam("keyword3",new SimpleDateFormat("yyyy-MM-dd").format(new Date()) ,"#0044BB"));
        paras.add(new TemplateParam("remark",remark,"#AAAAAA"));
        tem.setTemplateParamList(paras);
        return tem;
    }
    public static void main(String[] arg) throws Exception {
        /*Template tem=new Template();
        tem.setTemplateId("Z_W4lXriXTRNrtvV61OnV609NFx6BGcSKNd7v5yTLz4");
        tem.setTopColor("#00DD00");*/
        //黄烁彬os6oy1mBO7W6dX8Iv4Zjm5hO9Mac
        //黄华德os6oy1t3m4BOtGNke-W1V2q5DCI0
        //我os6oy1v5OE1rc24ECDw6FinXPnNo
        /*tem.setToUser("os6oy1v5OE1rc24ECDw6FinXPnNo");
        tem.setUrl("http://weixin.qq.com/download");
        List<TemplateParam> paras=new ArrayList<TemplateParam>();
        paras.add(new TemplateParam("first","我们已收到您的货款，开始为您打包商品，请耐心等待: )","#FF3333"));
        paras.add(new TemplateParam("keyword1","¥20.00","#0044BB"));
        paras.add(new TemplateParam("keyword2","火烧牛干巴","#0044BB"));
        paras.add(new TemplateParam("remark","感谢你对我们商城的支持!!!!","#AAAAAA"));
        tem.setTemplateParamList(paras);
        boolean result=sendTemplateMsg(tem);
        System.out.println(result);*/
        SendTemplateUtil util = new SendTemplateUtil();
        boolean b = util.activityFair("os6oy1v5OE1rc24ECDw6FinXPnNo", "暴走安图恩", "球杆太老,洞口太小,桌子太高,状态不好");
        System.out.println(b);
        /*SendTemplate template = new SendTemplate();
        Template register = template.register("22222222", "11111111111", "11111111", "sssssss", "qqqqqqqq", "sssssss");
        System.out.println(register);*/
    }
}