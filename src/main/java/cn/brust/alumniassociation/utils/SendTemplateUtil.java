package cn.brust.alumniassociation.utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * <pre>
 *     消息推送类
 *     @author  陈润颖
 *     @since   1.0
 *     @since   JDK 1.8
 * </pre>
 */
@Component
public class SendTemplateUtil {
    @Autowired
    SendTemplate template;
    /**  <pre>
     * 注册提交成功,待审核
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @return : boolean
     * </pre>
     */
    public boolean registering(String openID)throws Exception{
        //微信模板ID
        String templateID = "nUjvUBa1dlst0JankhqVqb0g1K0KQTD5rsIF76Wik";
        Template tem = template.register(openID,templateID,"亲爱的用户,我们已收到您的注册请求，开始为您进行审核，请耐心等待: )",
                new SimpleDateFormat("yyyy-MM-dd").format(new Date()),"正在审核中","感谢你对我们校友会的支持!!!!");
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 注册成功
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @return : boolean
     * </pre>
     */
    public boolean registerSuccess(String openID)throws Exception{
        //微信模板ID
        String templateID = "nUjvUBa1dlst0JankhqVqb0g1K0KQTD5rsIF76Wik";
        Template tem = template.register(openID,templateID,"尊敬的用户,您的注册已经审核通过，祝您在校友会一切愉快: )",
                new SimpleDateFormat("yyyy-MM-dd").format(new Date()),"审核通过","感谢您的注册!!!!");
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 项目提交成功,待审核
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @param key1 : 项目名称
     * @return : boolean
     * </pre>
     */
    public boolean projectSubmit(String openID,String key1,String remark )throws Exception{
        //微信模板ID
        String templateID = "N_t2RNxiQ2FnxOHvJ-NJc446gr537_IGgaYE4at7oRg";
        Template tem = template.register(openID,templateID,"您的项目已经成功提交,开始为您进行审核,请耐心等候: )",
                key1,"受理成功","如果申请失败,请重新申请");
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 项目审核成功
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @param key1 : 项目名称
     * @return : boolean
     * </pre>
     */
    public boolean projectSuccess(String openID,String key1)throws Exception{
        //微信模板ID
        String templateID = "N_t2RNxiQ2FnxOHvJ-NJc446gr537_IGgaYE4at7oRg";
        Template tem = template.register(openID,templateID,"您的项目已经审核通过: )",
                key1,"审核成功","感谢您对我们校友会平台的支持!!!!");
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 项目审核失败
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @param key1 : 项目名称
     * @param remark : 审核不通过的原因
     * @return : boolean
     * </pre>
     */
    public boolean projectFair(String openID,String key1,String remark )throws Exception{
        //微信模板ID
        String templateID = "N_t2RNxiQ2FnxOHvJ-NJc446gr537_IGgaYE4at7oRg";
        Template tem = template.register(openID,templateID,"对不起,您的项目审核不通过.",
                key1,"审核不通过",remark);
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 活动提交成功,待审核
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @param key1 : 活动名称
     * @return : boolean
     * </pre>
     */
    public boolean activitySubmit(String openID,String key1 )throws Exception{
        //微信模板ID
        String templateID = "Nyg0bI9crcYQpX3CUw3qgdZhNHcvnrD84_UogNXZcKw";
        Template tem = template.register(openID,templateID,"您好,您已经成功提交活动申请,正在进行审核,请耐心等候: )",
                key1,"正在审核","预计一个工作日内完成");
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 活动审核成功
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @param key1 : 活动名称
     * @return : boolean
     * </pre>
     */
    public boolean activitySuccess(String openID,String key1)throws Exception{
        //微信模板ID
        String templateID = "Nyg0bI9crcYQpX3CUw3qgdZhNHcvnrD84_UogNXZcKw";
        Template tem = template.register(openID,templateID,"您提交的活动申请已经成功通过审核: )",
                key1,"审核通过","请留意活动的截止日期");
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 活动审核失败
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @param key1 : 活动名称
     * @param remark : 失败原因
     * @return : boolean
     * </pre>
     */
    public boolean activityFair(String openID,String key1,String remark)throws Exception{
        //微信模板ID
        String templateID = "Nyg0bI9crcYQpX3CUw3qgdZhNHcvnrD84_UogNXZcKw";
        Template tem = template.register(openID,templateID,"对不起,您的活动审核不通过",
                key1,"审核失败",remark);
        System.out.println(tem);
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 企业资料提交成功,正在审核
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @param key1 : 企业名称
     * @return : boolean
     * </pre>
     */
    public boolean enterpriseSubmit(String openID,String key1)throws Exception{
        //微信模板ID
        String templateID = "DzlCTIR145PfCM7D9rGkVmHLnGXgnebrqOrrD4auYrA";
        Template tem = template.register(openID,templateID,"您的企业信息认证审核结果如下：",
                key1,"正在审核","预计3个工作日内完成,请耐心等候:)");
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 企业资料审核成功
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @param key1 : 企业名称
     * @return : boolean
     * </pre>
     */
    public boolean enterpriseSuccess(String openID,String key1)throws Exception{
        //微信模板ID
        String templateID = "DzlCTIR145PfCM7D9rGkVmHLnGXgnebrqOrrD4auYrA";
        Template tem = template.register(openID,templateID,"您的企业信息认证审核结果如下：",
                key1,"审核通过","如有疑问请联系客服.");
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
    /**  <pre>
     * 企业资料提审核失败
     * @author 陈润颖
     * @since 1.0
     * @since JDK 1.8
     * @param openID : 接受用户的openID
     * @param key1 : 企业名称
     * @param remark : 失败原因及解决办法
     * @return : boolean
     * </pre>
     */
    public boolean enterpriseFair(String openID,String key1,String remark)throws Exception{
        //微信模板ID
        String templateID = "DzlCTIR145PfCM7D9rGkVmHLnGXgnebrqOrrD4auYrA";
        Template tem = template.register(openID,templateID,"您的企业信息认证审核结果如下：",
                key1,"审核不通过",remark);
        boolean result=SendTemplate.sendTemplateMsg(tem);
        return result;
    }
}