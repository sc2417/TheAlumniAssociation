package cn.brust.alumniassociation.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

/**
 * Created by Administrator on 2018/8/14 0014.
 */
public class WXAccreditUtils {

    //appId
    public static final String APPID="wx0f0a4bbe2fc2fc3a";

    //获取用户信息
    public static String userInfo(String openId,String at) throws IOException {
        OkHttpClient okHttpClient = new OkHttpClient();
        String getInfo="https://api.weixin.qq.com/sns/userinfo?access_token="+at+"&openid="+openId+"&lang=zh_CN";
        Request getInforequest = new Request.Builder().url(getInfo).build();
        Response getInforesponse = okHttpClient.newCall(getInforequest).execute();
        String getInfostring = getInforesponse.body().string();
        return getInfostring;
    }


    //刷新access_token
    public static String refreshAT(String refresh_token) throws IOException {
        String url="https://api.weixin.qq.com/sns/oauth2/refresh_token?appid="+APPID+"&grant_type=refresh_token&refresh_token="+refresh_token;
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        Response response = okHttpClient.newCall(request).execute();
        String string = response.body().string();
        JSONObject parseObject = JSON.parseObject(string);
        Object errmsg = parseObject.get("access_token");
        return errmsg.toString();
    }


    //检验授权凭证（access_token）是否有效
    public static String isAT(String openid,String access_token) throws IOException {
        String url="https://api.weixin.qq.com/sns/auth?access_token="+access_token+"&openid="+openid;
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        Response response = okHttpClient.newCall(request).execute();
        String string = response.body().string();
        JSONObject parseObject = JSON.parseObject(string);
        Object errmsg = parseObject.get("errmsg");
        return errmsg.toString();
    }

}
