package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Notice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     @author 陈佳婷、马丹萍
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Mapper
public interface NoticeMapper {

    /**  <pre>
     *  查询所有系统公告
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回系统公告对象集合
     * </pre>
     */
    List<Notice> findAll();

    /**  <pre>
     *  查询所有系统公告
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回结果数据集合
     * </pre>
     */
    List<Map<String,String>> queryNotices(@Param("title") String title, @Param("startTime") String startTime, @Param("endTime") String endTime);
    /**  <pre>
     *  根据id删除系统公告(软删)
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回int数据
     * </pre>
     */
    int updateDel(String id);
    /**  <pre>
     *  添加公告
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回int数据
     * </pre>
     */
    int add(Notice notice);
    /**  <pre>
     *  根据id查询公告
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回结果数据集合
     * </pre>
     */
    Map<String,String> selectById(@Param("id") String id);
}