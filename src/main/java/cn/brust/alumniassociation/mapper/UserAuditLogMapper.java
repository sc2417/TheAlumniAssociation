package cn.brust.alumniassociation.mapper;


import cn.brust.alumniassociation.entity.UserAuditLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserAuditLogMapper {
    int insertAuditLog(@Param("id") String id, @Param("userId") String userId, @Param("datails") String msg, @Param("auditStatus") Integer auditStatus, @Param("auditPerson") String auditPerson, @Param("dateString") String createDate);

    List<UserAuditLog> queryUserAuditLog(@Param("userId") String userId);
}