package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator 2018/8/13 15:58
 */
@Mapper
public interface RoleMapper {
    Role selectById(@Param("id") String id);

    List<Map<String,String>> selectAll( @Param("classId") String classId);

    int update(Role role);

    int delete(String id);

    int add(Role role);

    List<Map<String,String>> selectPermissionByRoleId(String id);
}