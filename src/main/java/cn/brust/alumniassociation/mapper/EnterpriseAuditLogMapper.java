package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.EnterpriseAuditLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
@Mapper
public interface EnterpriseAuditLogMapper {
    int add(EnterpriseAuditLog enterpriseAuditLog);

    List<EnterpriseAuditLog> selectByEnterpriseId(@Param("enterpriseId")String enterpriseId);

    EnterpriseAuditLog selectById(String id);

    int addAuditLog(@Param("id") String  id,@Param("enterpriseId")String enterpriseId,@Param("msg")String msg,@Param("auditStatus")Integer  auditStatus,@Param("auditDate")String  auditDate,@Param("createPerson") String createPerson);
}