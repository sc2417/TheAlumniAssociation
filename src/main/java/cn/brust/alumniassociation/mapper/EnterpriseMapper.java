package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Enterprise;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     @author 马敏婷
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Mapper
public interface EnterpriseMapper {

    /**  <pre>
     *  根据ID查询数据
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @return :企业信息
     * </pre>
     */
    Enterprise selectById(String id);


    /**  <pre>
     *  添加企业
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param enterprise : 企业对象
     * @return : 返回受影响的行数
     * </pre>
     */
    int add(Enterprise enterprise);


    /**  <pre>
     * 修改企业信息
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param enterprise : 企业对象
     * @return : 返回受影响的行数
     * </pre>
     */
    int update(Enterprise enterprise);


    /**  <pre>
     *  删除企业（软删）
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @return : 返回受影响的行数
     * </pre>
     */
    int delete(String id);


    /**  <pre>
     *  企业列表-筛选查询
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @param name : 企业名称
     * @param username : 企业推介人
     * @param startTime : 发布时间的查询开始范围
     * @param endTime : 发布时间的查询结束范围
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String,String>> queryEnterprises(@Param("classId") String classId, @Param("name") String name,
                                              @Param("userName") String username, @Param("startTime") String startTime,
                                              @Param("endTime") String endTime);
/**  <pre>
     *  企业审核列表-筛选查询
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @param name : 企业名称
     * @param username : 企业推介人
     * @param startTime : 发布时间的查询开始范围
     * @param endTime : 发布时间的查询结束范围
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String,String>> queryEnterprisesByAudit(@Param("classId") String classId, @Param("name") String name,
                                                     @Param("userName") String username, @Param("startTime") String startTime,
                                                     @Param("endTime") String endTime);


    /**  <pre>
     *  查询企业ID是否存在
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业id
     * @return : 企业信息
     * </pre>
     */
    Enterprise queryIdIsExists(String id);


    /**  <pre>
     *  修改企业审核状态
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @param auditStatus : 修改之后的状态
     * @return : 返回受影响的行数
     * </pre>
     */
    int updateAuditStatus(@Param("id") String id, @Param("auditStatus") Integer auditStatus);

    /**
     * <pre>
     *  修改状态：上架、下架
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @param status : 企业状态
     * @return : 返回受影响的行数
     * </pre>
     */
    int updateStatus(String id, String status);

    /**  <pre>
     *  根据ID查询  微信端显示
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @return : 返回查询的数据
     * </pre>
     */
    List<Map<String,String>> queryEnterprisesId(String id);

    /**  <pre>
     *  微信端 根据用户ID查询
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param userId : 用户ID
     * @return : 返回查询的数据
     * </pre>
     */
    Enterprise queryInfo(String userId);

    /**  <pre>
     *  微信端 修改用户修改的企业信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param enterprise 要修改的企业对象
     * @return :
     * </pre>
     */
    int updateInfo(Enterprise enterprise);


    /**  <pre>
     *  微信端 用户添加企业信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param enterprise 要添加的企业对象
     * @return :
     * </pre>
     */
    int insertInfo(Enterprise enterprise);
}