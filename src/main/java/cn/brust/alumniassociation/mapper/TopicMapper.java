package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Topic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     @author
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Mapper
public interface TopicMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Topic record);

    int insertSelective(Topic record);

    Topic selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Topic record);

    int updateByPrimaryKey(Topic record);

    /**  <pre>
     * 后台管理 -  查询所有分类（分页查询）
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回结果数据集合
     * </pre>
     */
    List<Map<String,String>> getTopics();


    /**  <pre>
     * 后台管理 - 分类显示状态的修改
     * 根据分类ID和enable显示状态去修改
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param id :分类ID
     * @param enable :显示状态
     * @return : int
     * </pre>
     */
    int updateEnable(@Param("enable") Integer enable, @Param("id") String id);

    /**  <pre>
     * 后台管理 - 分类修改
     * 根据分类ID和enable显示状态和sort排序和title标题去修改
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param id :分类ID
     * @param enable :显示状态
     * @param sort :排序
     * @param title :标题
     * @return : int
     * </pre>
     */
    int updateTopic(@Param("enable") Integer enable, @Param("id") String id, @Param("sort") Integer sort, @Param("title") String title);

    /**  <pre>
     * 后台管理 - 分类删除
     * 根据分类ID去修改del的状态 （软删）
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param id :分类ID
     * @return : int
     * </pre>
     */
    int updateDel(@Param("id") String id);


    /**  <pre>
     * 后台管理 - 添加分类
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param title :标题
     * @param enable :显示状态
     * @param sort :排序
     * @param title :标题
     * @return : int
     * </pre>
     */
    int add(@Param("id")String id,@Param("title") String title, @Param("sort") Integer sort, @Param("enable") Integer enable);

    /**  <pre>
     * 后台管理 - 查询分类
     * 根据分类ID查询分类详情
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param id :分类ID
     * @return : java.util.Map
     * </pre>
     */
    Map<String,String> selectById(@Param("id") String id);

    /**
     * 陈佳婷
     * @return
     */
    List<Topic> selectTopic();

    List<Topic> getAllTopic();
}