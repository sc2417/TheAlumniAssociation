package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Advert;
import org.apache.ibatis.annotations.*;

import org.apache.ibatis.annotations.Select;

import java.util.List;

import java.util.Map;

/**
 * <pre>
 *     @author 郑锦波、孙超
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Mapper
public interface AdvertMapper {

    /**  <pre>
     *  查询所有广告
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回广告对象集合
     * </pre>
     */
    List<Advert> findAll();


    /**  <pre>
     *  添加广告
     * 必须字段 - 广告名称、广告位置、广告图片、广告链接、广告备注、
     * 上线/下线、是否置顶
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param advert : 广告对象
     * @return : 返回受影响的行数
     * </pre>
     */
    int addAdvert(Advert advert);


    /**  <pre>
     *  根据id删除广告信息(软删)
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param id : 广告ID
     * @return : 返回受影响的行数
     * </pre>
     */
    int delAdvert(Integer id);


    /**  <pre>
     *  根据广告名称和广告位置(单独位置)查询
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param title : 广告名称
     * @param title : 广告位置
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String,String>> vagueQuery(@Param("title")String title, @Param("site")Integer site);


    /**  <pre>
     *  根据广告名称和广告位置(全部)查询
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param title : 广告名称
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String,String>> vagueQueryAll(@Param("title")String title);

    
    /**  <pre>
     *  根据广告id查询
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param id : 广告ID
     * @return : 返回广告对象
     * </pre>
     */
    Advert queryById(Integer id);

    
    /**  <pre>
     *  根据广告id 修改广告信息(广告名称、地址、连接、备注、状态)
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param title : 广告名称
     * @param site : 广告地址
     * @param cover : 广告图片
     * @param url : 广告链接
     * @param desc : 广告备注
     * @param isStatus：广告状态
     * @param isTop：是否置顶
     * @param id : 广告ID
     * @return : 返回受影响的行数
     * </pre>
     */
    int updateAdvert(@Param("title")String title,@Param("site")Integer site,@Param("cover")String cover,@Param("url")String url,@Param("desc")String desc,@Param("isStatus")Integer isStatus,@Param("isTop")Integer isTop,@Param("id")Integer id);

    /**
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * 根据id置顶广告
     * @param id
     */
    int settop(@Param("id")Integer id);

    /**
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * 根据id取消置顶
     * @param id
     */
    int canceltop(@Param("id")Integer id);

    /**
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * 查询置顶条数
     */
    Advert queryCount();
}