package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Activity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     @author 黄华德、孙超
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */

@Mapper
public interface ActivityMapper {

    /**
     * 发布动态
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @param userId :
     * @param activityId :
     * @param name :
     * @param level :
     * @param content :
     * @return : int
     */
    int addDynamics(@Param("id")String id,@Param("userId")String userId,@Param("activityId")String activityId,@Param("name")String name,@Param("level")String level,@Param("content") String content);

    /**
     * 动态评论（wx）
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * */
    List<Map<String,String>> dynamicQuery(@Param("id")Integer id);

    /**
     * 点赞修改
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     * @param zan :
     * @param id :
     * @return : int
     */
    int updateFabulous(@Param("zan") int zan,@Param("id")String id);

    /**
     *  查看评论
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.entity.Activity
     */
    Activity queryComment(@Param("id") int id);

    /**
     * 查看活动（评论）
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     */
    List<Map<String,String>> detailsFindById(int id);

    /**
     *  编辑活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param title :
     * @param cover :
     * @param address :
     * @param type :
     * @param createDate :
     * @param startDate :
     * @param endDate :
     * @param content :
     * @param id :
     * @return : int
     */
    int modifyActivity(@Param("title")String title, @Param("cover")String cover,@Param("address")String address,@Param("type")String type, @Param("createDate")String createDate,@Param("startDate")String startDate, @Param("endDate")String endDate,@Param("content")String content,@Param("id")String id);

    /**
     * ID查询
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     */
    Activity selectById(String id);

    /**
     * 修改报名状态
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : int
     */
    int modifyEnroll(String id);

    /**
     *  删除活动（软删）
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : int
     *
     */
    int delActivity(String id);

    /**
     *  添加活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @param title :
     * @param cover :
     * @param address :
     * @param type :
     * @param createDate :
     * @param startDate :
     * @param endDate :
     * @param content :
     * @return : int
     */
    int addActivity(@Param("id")String id,@Param("title")String title, @Param("cover")String cover, @Param("address")String address, @Param("type")String type, @Param("createDate") String createDate, @Param("startDate") String startDate, @Param("endDate")String endDate, @Param("content")String content);

    /**
     * 活动展示（weixin）
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     *
     */
    List<Map<String,String>> findAll();

    /**
     * 筛选查询活动信息
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param classId :
     * @param title :
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     */
    List<Map<String,String>> queryVague(@Param("classId") Integer classId, @Param("title") String title);

    /**
     *  筛选查询活动信息Time
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param classId :
     * @param title :
     * @param createDate :
     * @param endDate :
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     *
     */
    List<Map<String,String>> queryVagueTime(@Param("classId") Integer classId, @Param("title") String title, @Param("createDate") String createDate, @Param("endDate") String endDate);

    /**
     *  查询活动详情(班级)
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     */
    List<Map<String,String>> lookupActivityDetails(@Param("id")Integer id);

    /**  <pre>
     * 展示本班级进行中活动
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    List<Map<String,String>> queryConductActivity(@Param("classId") Integer classId);

    /**  <pre>
     * 展示本班级已结束活动
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    List<Map<String,String>> queryEndActivity(@Param("classId") Integer classId);


    /**  <pre>
     *  查询活动详情
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */

    Activity queryActivityDetails(@Param("id")Integer id);

    /**  <pre>
     * 微信首页获取一条活动（班级没有进行中就查询从所有活动中查询一条进行中的，进行中没有的就查询历史的）
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    Activity getActivityOne(@Param("cId") String cId);


    /**  <pre>
     *  根据用户id查询进行中的活动
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */
    List<Map<String,String>> getMyInActivity(@Param("uId") String uId);

    /**  <pre>
     *  根据用户id查询活动回顾的活动
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */
    List<Map<String,String>> getMyEndActivity(@Param("uId") String uId);
}