package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Project;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface ProjectMapper {

    /**  <pre>
     * wx端查询
     * 根据班级ID查询  项目对接
     * 需要 项目ID,状态,简介,用户头像,用户名
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>> 暂时使用
     * </pre>
     */
    List<Map<String,String>> queryProjects(@Param("classId") String classId);

    /**  <pre>
     * wx端查询
     * 根据用户ID  我的项目
     * 需要 项目ID,状态,简介,用户头像,用户名
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param userId :用户ID
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     * </pre>
     */
    List<Map<String,String>> getProjectsByUserid(@Param("userId") String userId);

    /**  <pre>
     * 后台管理-项目列表
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param classId :班级ID
     * @param budget : 项目预算
     * @param nameOrMobile : 会员名称/手机号
     * @param startTime : 发布时间-开始
     * @param endTime : 发布时间-结束
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     * </pre>
     */
    List<Map<String,String>> adminQueryProjectByKeyWord(@Param("classId") String classId,
                                                        @Param("budget") Integer budget,
                                                        @Param("nameOrMobile") String nameOrMobile,
                                                        @Param("startTime") String startTime,
                                                        @Param("endTime") String endTime);
    /**  <pre>
     * 后台管理-项目列表
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param classId :班级ID
     * @param budget : 项目预算
     * @param nameOrMobile : 会员名称/手机号
     * @param startTime : 发布时间-开始
     * @param endTime : 发布时间-结束
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     * </pre>
     */
    List<Map<String,String>> adminQueryAuditProjectByKeyWord(@Param("classId") String classId,
                                                             @Param("budget") Integer budget,
                                                             @Param("nameOrMobile") String nameOrMobile,
                                                             @Param("startTime") String startTime,
                                                             @Param("endTime") String endTime);

    /**  <pre>
     *  添加项目
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param project :
     * @return : int
     * </pre>
     */
    int addProject(Project project);

    /**  <pre>
     * wx前端
     * 根据项目ID查询项目详情
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @return : java.util.Map
     * </pre>
     */
    Map getProjectById(String id);

    /**  <pre>
     * 后台管理
     * 根据项目ID查看跟进状态和项目详情
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : java.util.Map
     * </pre>
     */
    Map getProjectRecord(String id);

    /**  <pre>
     * 后台管理 - 项目列表 - 项目跟进 - 录入进度
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id : 项目ID
     * @param status : 项目状态
     * @param msg : 跟进消息
     * @return : int
     * </pre>
     */
    int updateProjectRecord(@Param("id") String id,@Param("status") Integer status,@Param("msg") String msg);

    /**  <pre>
     * 后台管理
     * 根据项目id和状态status 去修改
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @param status :状态
     * @return : int
     * </pre>
     */
    int updateStatus(@Param("id") String id, @Param("status") Integer status);

    /**  <pre>
     * 后台管理 - 修改项目
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param project : 项目
     * @return : int
     * </pre>
     */
    int updateProject(Project project);

    /**  <pre>
     * 后台管理 - 删除
     * 根据项目ID 修改 del =1 标志为删除
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @return : int
     * </pre>
     */
    int deleteProjectById(@Param("id") String id);

    /**  <pre>
     * 后台管理 审核列表 - 审核状态的更改
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @param auditStatus :审核状态
     * @param auditLog :审核消息
     * @return : int
     * </pre>
     */
    int updateAudit(@Param("id") String id, @Param("auditStatus") Integer auditStatus, @Param("auditLog") String auditLog);

    /**  <pre>
     * 后台管理  审核列表 - 审核记录
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id : 项目ID
     * @return : java.util.Map
     * </pre>
     */
    Map getAuditLogById(String id);

    /**  <pre>
     * 微信前端 - 查看项目的时候 浏览次数+1
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @return : int
     * </pre>
     */
    int addVisit(String id);

    /**  <pre>
     * 微信前端 - 修改项目
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @param project :项目
     * @return : int
     * </pre>
     */
    int wxUpdateProject(Project project);
}