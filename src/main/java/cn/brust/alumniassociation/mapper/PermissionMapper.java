package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PermissionMapper {

    List<Permission> selectAll();

    List<Permission> selectAllByMemberId(String memberId);
}