package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Member;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
@Mapper
public interface MemberMapper {

    Member findMemberPhone(String phone);

    /*根据手机号码和密码和班级ID查询用户*/
    Member selectPhonePasswdClassId(@Param("phone") String mobile, @Param("password") String passwd, @Param("class_id") String classId);

    /**
     * 账号设置-修改密码
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email       邮箱账号
     * @param
     * @return
     */
    Member findMember(@Param("email") String email, @Param("password") String password);

    int updatePwd(@Param("email") String email,@Param("password") String newPwd);

    int updateAuatar(@Param("email") String email,@Param("avatar") String avatar);

    Member findMemberEmail(@Param("email") String email);

}