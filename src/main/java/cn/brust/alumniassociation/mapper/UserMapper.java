package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.security.core.parameters.P;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     @author 王永泽
 *     @since 1.0
 *     @since JDK 8.0
 * </pre>
 */
@Mapper
public interface UserMapper {
    /**  <pre>
     *  添加微信端用户信息
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */

    int addWXUser(@Param("openId")String openId);

    /**  <pre>
     *  查询用户是否存在
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */
    User queryByWxId(String openId);


    /**  <pre>
     *  根据手机号查询用户
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param mobile :手机号
     * @return :  Map<String,String>
     * </pre>
     */
    /*Map<String,String> selectByMobile(@Param("mobile") String mobile);*/
    User selectByMobile(@Param("mobile") String mobile);

    /**  <pre>
     * 申请加入
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param user
     * </pre>
     */
    int applyToJoin(User user);

    /**
     * <pre>
     *  修改用户密码
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param user : 用户表
     * @return : 返回受影响的行数
     * </pre>
     */
    int updatePassword(User user);


    /**
     * <pre>
     *  根据邮箱和密码查询信息
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email : 用户邮箱
     * @param passwd : 用户密码
     * @return : 返回查询的用户对象
     * </pre>
     */
    User selectUser(@Param("email") String email, @Param("passwd") String passwd);


    /**
     * <pre>
     *  修改图片链接地址
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email : 用户邮箱
     * @param avatar : 用户上传的头像地址
     * @return : 返回受影响的行数
     * </pre>
     */
    int updateAvatar(@Param("email") String email, @Param("avatar") String avatar);


    /**
     * <pre>
     *  查询所有会员列表
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String, String>> selectAll();


    /**
     * <pre>
     * 查询会员信息
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId  用户Id
     * @return : 根据用户Id查询，返回查询到的会员信息
     * </pre>
     */
    Map<String, String> queryHomecomingDynamic(String wxId);



    /**
     * 会员管理-会员列表信息-筛选查询
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param name        用户名
     * @param mobile      手机号
     * @param classId     班级Id
     * @param beginTime     注册时间（开始）
     * @param beginTime     注册时间（结束）
     * @param suditStatus 审核状态
     * @param suditStatu  审核状态
     * @return
     */
    List<Map<String, String>> filterQuery(@Param("name") String name, @Param("mobile") String mobile, @Param("classId") Integer classId, @Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("status") String suditStatus, @Param("statu") String suditStatu);

    /*根据手机号码和密码查查询用户*/
    User selectPhonePasswd(String phone, String passwd);

    /**
     * 查询该班级的所有用户
     *
     * @param classId 班级Id
     * @return
     * @author 王永泽
     */
    List<Map<String, String>> classyAllUser(Integer classId);

    /**
     * <pre>
     *  根据班级ID查询 改班级下所有人员  wx_id,name
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param classId :班级ID
     * @return : java.util.List<cn.brust.alumniassociation.entity.User>
     * </pre>
     */
    List<User> selectAllByClassId(@Param("classId") Integer classId);

    /**
     * 会员管理-会员审核
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId        用户Id
     * @param status      审核状态
     * @return
     */
    int updateAuditStatus(@Param("wxId") String wxId, @Param("status")String status);

    /**
     * 会员管理-会员状态冻结
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId        用户Id
     * @param enable      用户冻结状态
     * @return
     */
    int updateUserEnable(@Param("wxId")String wxId,@Param("enable")Integer enable);

    /**
     * 会员管理-删除会员信息
     *
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param wxId        用户Id
     * @return
     */
    int delUser(@Param("wxId")String wxId);

    /**  <pre>
     * 增加班级管理员
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param wxId
     * @param name
     * @param email
     * @param classId
     * @param passwd
     * @param describe
     * </pre>
     */
    int addClassRole(@Param("wxId") String wxId, @Param("name") String name, @Param("mobile") String mobile, @Param("email") String email, @Param("classId") Integer classId, @Param("passwd") String passwd, @Param("describe") String describe);

    /**  <pre>
     * 根据班级id查找用户
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param classId
     * </pre>
     */
    List<User> queryByClassId(@Param("classId") Integer classId);

    /**  <pre>
     * 根据wxid查找用户
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param wxId
     * </pre>
     */
    User findByWxId(@Param("wxId") String wxId);

    /**
     * 个人中心- 个人信息修改
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId      会员id
     * @param nickname  昵称
     * @param  mobile   手机号码
     * @param  email     邮箱
     * @param  classrole  班级角色名称
     * @return
     */
    int updateMyInfo(@Param("wxId") String wxId,@Param("nickname")String nickname,@Param("mobile") String  mobile,@Param("email")String email,@Param("classrole")String classrole);

    /**
     * <pre>
     *  根据邮箱或手机号码登录
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param accounts 邮箱或者手机号码
     * @param passwd 密码
     * @return
     * </pre>
     */
    Map pwdCorrect(@Param("accounts") String accounts, @Param("passwd") String passwd);

    /**
     * <pre>
     * 查询此邮箱是否有信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param email : 用户邮箱
     * @return : 返回查询的用户对象
     * </pre>
     */
    User selectEmail(@Param("email") String email);

    /**  <pre>
     * 根据wxid修改班级管理员信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param name
     * @param mobile
     * @param email
     * @param passwd
     * @param describe
     * @param wxId
     * </pre>
     */
    int updateClassRole(@Param("name") String name, @Param("mobile") String mobile, @Param("email") String email, @Param("passwd") String passwd, @Param("describe") String describe, @Param("wxId") String wxId);
}