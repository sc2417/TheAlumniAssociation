package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Enroll;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface EnrollMapper {

    /**  <pre>
     *  查询这人是否已经报名此活动
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */
    Enroll queryById(@Param("wxId") String wxId,@Param("acId")String acId);

    /**  <pre>
     *  活动报名
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    int activityEnroll(@Param("eId") String id, @Param("wxId")String uId,@Param("acId")String acId);



}