package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Information;
import org.apache.ibatis.annotations.Mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;
/**
 * <pre>
 *     @author 陈润颖
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Mapper
public interface InformationMapper {


    /**  <pre>
     * 查询所有资讯信息
     * @author 陈润颖
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回资讯对象集合
     * </pre>
     */
    List<Information> findAll();
    /**  <pre>
     *  查询前五条资讯信息
     * @author 陈润颖
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回资讯对象集合
     * </pre>
     */
    List<Information> queryTheLatestInformation();

    /**  <pre>
     *  资讯列表-筛选查询(包括时间)
     * @author 陈润颖
     * @since 1.0
     * @since JDK 8.0
     * @param cid  : 班级ID
     * @param title : 资讯标题
     * @param starttime : 发布时间起始范围
     * @param endtime : 发布时间结束范围
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String,String>> queryInfoWithTime(@Param("cid") Integer cid, @Param("title") String title,
                                               @Param("starttime")String starttime,
                                               @Param("endtime") String endtime);

    /**  <pre>
     *  资讯列表-筛选查询
     * @author 陈润颖
     * @since 1.0
     * @since JDK 8.0
     * @param cid  : 班级ID
     * @param title : 资讯标题
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String,String>> queryInfomation(@Param("cid") Integer cid,@Param("title") String title);

    /**
     * 资讯列表-筛选查询
     * @param id   ID
     * @param enable 状态
     * @return
     */
    //@Update("update t_information set enable=${enable} where id=${id}")
    int updStaById(@Param("id") Integer id,@Param("enable") Integer enable);

    int addInformation(Information information);


    /**  <pre>
     * 后台管理 - 删除
     * 根据资讯ID 修改 del =1 标志为删除
     * @author 陈润颖
     * @since 1.0
     * @since JDK 8.0
     * @param id :资讯ID
     * @return : int
     * </pre>
     */
    int deleteInformationById(@Param("id") String id);


    /**
     * 资讯列表-筛选查询
     * @param information  需要修改的行
     * @return
     */
    int update(Information information);

    /**
     * 根据ID查询一条资讯
     * @param id   ID
     * @return
     */
    Information queryInformationById(Integer id);

    /**
     * 修改资讯
     * 同上
     * @param information 资讯
     * @return
     */
    int updateInfomationById(Information information);

    List<Map<String,String>> queryInfomation(@Param("cid") Integer cid,@Param("title") String title,@Param("topicId")String topicId);

    List<Map<String,String>> queryInfoWithTime(@Param("cid") Integer cid, @Param("title") String title,
                                               @Param("starttime")String starttime,
                                               @Param("endtime") String endtime,@Param("topicId")String topicId);

    /**
     * 修改访问量+1
     */
    int updVisitById(String id);

}