package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Classy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
/**
 * <pre>
 *     @author 马敏婷、郑锦波
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Mapper
public interface ClassyMapper {

    /**  <pre>
     *  查询显示所有的班级信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回班级集合信息
     * </pre>
     */
    List<Classy> selectAll();

    /**
     * 添加班级信息
     * @return
     */
    int addClassy(Classy classy);

    /**
     * 根据 班级id查询班级信息
     * @param id 编号
     * @return
     */
    Classy selectById(@Param("id") Integer id);

    /**
     * 根据班级id 修改班级信息(班级名称、班级状态)
     * @param name 名称
     * @param userId  管理员id
     * @param isEnable  状态
     * @param id    编号
     * @return
     */
    int updateClassy(@Param("name") String name, @Param("userId") String userId, @Param("isEnable") Integer isEnable, @Param("id") Integer id);

    /**
     * 根据id删除班级信息(软删)
     * @param id 编号
     * @return
     */
    int delClassy(@Param("id") Integer id);

    /**  <pre>
     * 查询所有班级
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0

     * @return : java.util.List<cn.brust.alumniassociation.entity.Classy>
     * </pre>
     */
    List<Classy> queryAll();

    /**
     *  根据id查询班级信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param id 编号
     * @return
     */
    Classy queryById(@Param("id") Integer id);
}