package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Feedback;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface FeedbackMapper {

    /**  <pre>
     * WX端添加
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param record :反馈对象
     * @return : int
     * </pre>
     */
    int insert(@Param("id") String id, @Param("opinion") String opinion, @Param("user_id") String userId);

    /**  <pre>
     * 后台管理 - 删除
     * 根据反馈ID 修改 del =1 标志为删除
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param id :反馈ID
     * @return : int
     * </pre>
     */
    int deleteByPrimaryKey(Integer id);

    /**  <pre>
     * 后台管理 - 修改
     * 根据反馈ID 修改 处理状态为1和修改处理备注,处理时间
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param id :反馈ID
     * @return : int
     * </pre>
     */
    int update(@Param("id") Integer id, @Param("reply") String reply);

    /**  <pre>
     * 后台管理 - 查询全部
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String,String>> queryAll();

    /**  <pre>
     * 后台管理 - 查询全部
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String,String>> queryId(String id);

    /**  <pre>
     * 后台管理 - 根据发布时间查询全部
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回结果对象集合
     * </pre>
     */
    List<Map<String,String>> queryTimeAll(@Param("starttime")String starttime,@Param("endtime") String endtime);
}