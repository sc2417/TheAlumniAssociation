package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Homecoming;
import org.apache.ibatis.annotations.Mapper;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Mapper
public interface HomecomingMapper {

    /**  <pre>
     *  查询同学会信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return : null
     * </pre>
     */
    Homecoming query();

    
    /**  <pre>
     *  修改学院介绍内容和是否显示
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param record : 待修改的同学会对象
     * @return : 返回受影响的行数
     * </pre>
     */
    int updateDetailsByPrimaryKey(Homecoming record);

    
    /**  <pre>
     *  修改学生会介绍内容和是否显示
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param record : 待修改的同学会对象
     * @return : 返回受影响的行数
     * </pre>
     */
    int updateClassmateByPrimaryKey(Homecoming record);

    
    /**  <pre>
     *  修改学生会章程内容和是否显示
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param record : 待修改的同学会对象
     * @return : 返回受影响的行数
     * </pre>
     */
    int updateStatutesByPrimaryKey(Homecoming record);
}