package cn.brust.alumniassociation.mapper;

import cn.brust.alumniassociation.entity.Platform;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface PlatformMapper {

    /**
     * 修改平台信息
     * @param record
     * @return
     */
    int updateByPrimaryKey(Platform record);

    /**
     * 查询平台
     * @return
     */
    Platform queryPlatformInfo();
}