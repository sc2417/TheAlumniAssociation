/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: UserController
 * Author:   Administrator
 * Date:     2018/7/25 22:09
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.code.VerificationCodeSend;
import cn.brust.alumniassociation.entity.User;
import cn.brust.alumniassociation.service.ClassyService;
import cn.brust.alumniassociation.service.UserAuditLogService;
import cn.brust.alumniassociation.service.UserService;
import cn.brust.alumniassociation.utils.FileUtils;
import cn.brust.alumniassociation.utils.UUIDUtils;
import cn.brust.alumniassociation.utils.WXAccreditUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author Administrator
 * @create 2018/7/25
 * @since 1.0.0
 */
@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    ClassyService classyService;

    @Autowired
    UserAuditLogService userAuditLogService;


    //获取code
    @GetMapping("/openId")
    public String index() throws IOException {
        String url_get_webpage_token = "https://open.weixin.qq.com/connect/oauth2/authorize?"
                + "appid="+WXAccreditUtils.APPID
                + "&redirect_uri=http://www.woailol.cn/wxUserInfo"
                + "&response_type=code"
                + "&scope=snsapi_base"
                + "&state=777#wechat_redirect".trim();
        return "redirect:"+url_get_webpage_token;
    }


    /**  <pre>
     *  授权登录(每次进入公众号就根据openId查询用户是否在数据库存在，不存在就添加用户一系列信息)
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wxUserInfo")
    @ResponseBody
    public String addUser(String code) throws IOException {
        //code换openId
        String getToken = "https://api.weixin.qq.com/sns/oauth2/access_token?"
                + "appid="+ WXAccreditUtils.APPID
                + "&secret=a0224f4912b19dbda937fc56c2fd03fd"
                + "&code="+code
                +"&grant_type=authorization_code".trim();
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(getToken).build();
        Response response = okHttpClient.newCall(request).execute();
        String string = response.body().string();
        JSONObject parseObject = JSON.parseObject(string);
        String openid = parseObject.get("openid").toString();
        String access_token = parseObject.get("access_token").toString();
        String refresh_token = parseObject.get("refresh_token").toString();
        //调用验证access_tokend并获取用户信息
        String at = WXAccreditUtils.isAT(openid, access_token);
        ServiceResult serviceResult = userService.queryByWxId(openid);
        if(serviceResult.getResult()!=null){
            return serviceResult.getResult().toString();
        }else {
            if(at.equals("ok")) {
                String userInfo = WXAccreditUtils.userInfo(openid, access_token);
                JSONObject wx = JSON.parseObject(userInfo);
                String wxopenid = wx.get("openid").toString();
                userService.addWXUser(wxopenid);
                return userInfo+"--"+wxopenid;
            }else {
                //否则刷新access_token并获取用户信息
                String refreshAT = WXAccreditUtils.refreshAT(refresh_token);
                String userInfo = WXAccreditUtils.userInfo(openid, access_token);
                JSONObject wx = JSON.parseObject(userInfo);
                String wxopenid = wx.get("openid").toString();
                userService.addWXUser(wxopenid);
                return userInfo+"--"+wxopenid;
            }
        }

    }

    @GetMapping("/wxtest")
    @ResponseBody
    public String testadd(String openid){
        ServiceResult serviceResult = userService.queryByWxId(openid);
        if(serviceResult.getResult()==null){
            userService.addWXUser(openid);
            return "111";
        }
        System.out.println(serviceResult.getResult());
        return "222";

    }

    /**  <pre>
     *  注册
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param mobile : 手机号码
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @PostMapping("/user/register")
    @ResponseBody
    public ApiResponse register(String mobile, String name, Integer class_id, String code, String pwd, String position, HttpSession session){
        String verifyCode = (String) session.getAttribute("verifyCode");
        System.out.println(verifyCode+"verifyCode");
        if(!verifyCode.equals(code)){
            return ApiResponse.ofMessage(-1,"验证码有误!");
        }else{
            ServiceResult result = userService.selectByMobile(mobile);
            if(result.getResult() == null){
                String wxId = UUIDUtils.getUUID();
                User user = new User();
                user.setWxId(wxId);
                user.setMobile(mobile);
                user.setName(name);
                user.setClassId(class_id);
                user.setPasswd(pwd);
                user.setClassRole(position);
                userService.applyToJoin(user);
                return ApiResponse.ofMessage(200,"注册成功!待审核!");
            }else{
                User user = (User) result.getResult();
                Integer auditStatus = user.getAuditStatus();
                if(auditStatus == 0){
                    System.out.println("未审核");
                    return ApiResponse.ofMessage(0,"此账号已注册过，正在审核中!");
                }else if(auditStatus == 1){
                    System.out.println("审核不通过");
                    return ApiResponse.ofMessage(1,"此账号审核不通过!");
                }else if(auditStatus == 2){
                    System.out.println("审核通过");
                    return ApiResponse.ofMessage(2,"此账号已审核通过!请直接登录！");
                }
            }
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
    /**  <pre>
     *  发送手机验证码
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param mobile : 手机号码
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @RequestMapping("/user/code")
    @ResponseBody
    public ApiResponse code(String mobile, HttpSession session)  {
        VerificationCodeSend.getRequest2(mobile, session);
        return  ApiResponse.ofStatus(ApiResponse.Status.SUCCESS);
    }
    /**
     * 登录
     * @param name
     * @param passwd
     * @param cid
     * @return
     */

    @PostMapping("/user/login")
    public ApiResponse login(String name, String passwd, Integer cid){
        return new ApiResponse();
    }
    /**
     * 会员列表
     * @param page
     * @param limit
     * @return  所有会员信息(包括审核与未审核的会员)
     */
    @RequestMapping("/user/selectAll")
    @ResponseBody
    public ApiResponse selectAll(Integer page, Integer limit){
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }

        ServiceMultiResult projects = userService.selectAll(page,limit);
        return ApiResponse.ofPage(projects.getPage());
    }

    /**
     * 个人信息
     * @param wxId
     * @return
     */
    @PostMapping("/user/myInfo")
    @ResponseBody
    public ApiResponse myInfo(String wxId){
        ServiceResult<User> result= userService.queryHomecomingDynamic(wxId);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
    /*班级通讯录*/
    @PostMapping("/user/classyAllUser")
    @ResponseBody
    public ApiResponse classyAllUser(Integer cid,Integer page,Integer limit){
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult users = userService.classAllUser(cid,page,limit);
        return ApiResponse.ofPage(users.getPage());

    }

    /**  <pre>
     *  添加企业 - 企业所属班级模块
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @return : 所属班级的所有学员
     * </pre>
     */
    @GetMapping("/user/queryUser")
    @ResponseBody
    public ApiResponse classyAllUser(Integer classId){
        ServiceMultiResult users = userService.classyAllUser(classId);
        if (users.getTotal() == 0){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofSuccess(users.getResult());
    }

    /**
     * 会员管理-会员列表信息-筛选查询
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param name        用户名
     * @param mobile      手机号
     * @param classId     班级Id
     * @param createTime  注册时间
     * @param suditStatus 审核状态
     * @param suditStatu  审核状态
     * @return
     */
    @GetMapping("/user/filtrateQuery")
    @ResponseBody
    public ApiResponse filtrateQuery(String name,String mobile,Integer classId,String createTime,String suditStatus,String suditStatu,Integer page,Integer limit) {
        System.out.println(suditStatus);
        System.out.println(suditStatu);
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }else if (createTime != null && createTime != "") {
            ServiceMultiResult result = result = userService.filtrateQuery(name, mobile, classId, createTime,suditStatus,suditStatu, page, limit);
            return ApiResponse.ofPage(result.getPage());
        } else {
            ServiceMultiResult result = result = userService.filtrateQuery(name, mobile, classId, createTime,suditStatus,suditStatu, page, limit);
            if (result.getPage() == null) {
                return ApiResponse.ofMessage(102, "没有更多信息");
            }
            return ApiResponse.ofPage(result.getPage());
        }
    }
    /**
     * 修改密码
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email         邮箱
     * @param oldPasswd     旧密码
     * @param newPasswd     新密码
     * @return
     */
    @PostMapping("/user/updatePasswd")
    @ResponseBody
    public ApiResponse updatePasswd(String email,String oldPasswd,String newPasswd){
        System.out.println("修改密码");
        ServiceResult<User> result= userService.updatePasswd(email,oldPasswd,newPasswd);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }


    /**
     * 修改头像
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param servletRequest
     * @param file
     * @param email
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/upload/img" , method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> upload(HttpServletRequest servletRequest,@RequestParam("file") MultipartFile file,String  email) throws IOException {
        //如果文件内容不为空，则写入上传路径
        if (!file.isEmpty()) {
            //上传文件路径
            String filepath = FileUtils.saveFile(file, servletRequest, "avatar");
            Map<String, Object> res = new HashMap<>();
            //返回的是一个url对象
            res.put("url", filepath);
            String ress=res.toString();
            System.out.println();
            userService.updateAvatar(email, filepath);
            return res;
        } else {
            return null;
        }
    }

    @PostMapping("/admin/user/list/{classId}")
    @ResponseBody
    public ApiResponse getUsers(@PathVariable("classId")Integer classId){
        ServiceMultiResult<User> result = userService.selectAllByClassId(classId);
        List<User> users = result.getResult();
        if(users == null){
            return ApiResponse.ofMessage(1024,"我想应该是该班级下没人,所以不是BUG");
        }
        return ApiResponse.ofSuccess(users);
    }

    /**
     * 会员管理-会员审核
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId        用户Id
     * @param status      审核状态
     * @param auditlog     审核详细信息
     * @return
     */
    @PostMapping("/user/updateAuditStatus")
    @ResponseBody
    public ApiResponse updateAuditStatus(String userId, String auditStatus, String datails){
        //String id,String userId,String msg,String auditStatus,String auditPerson,String createDate
        String id=UUID.randomUUID().toString();

        String auditPerson="admin";
        System.out.println("审核id"+id);
        System.out.println("用户id"+userId);
        System.out.println("审核状态"+auditStatus);

        int i = Integer.parseInt(auditStatus);
        System.out.println("审核详情"+datails);
        System.out.println("审核人员"+auditPerson);
        Date createTime=new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(createTime);
        System.out.println("创建时间"+dateString);


        ServiceResult serviceResult = userAuditLogService.addAuditLog(id, userId, datails, i, auditPerson, dateString);
        System.out.println(serviceResult);

        ServiceResult<User> result= userService.updateAuditStatus( userId, auditStatus);

        if (result.isSuccess()){

            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
    /**
     * 会员管理-会员冻结
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId        用户Id
     * @param enable      用户冻结状态
     * @return
     */
    @PostMapping("/user/updateUserEnable")
    @ResponseBody
    public ApiResponse updateUserEnable(String wxId,Integer enable){
        ServiceResult<User> result= userService.updateUserEnable(wxId,enable);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     * 会员管理-删除会员信息
     *
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param wxId  用户Id
     * @return
     */
    @RequestMapping("/user/del")
    public String delUser(String wxId){
        userService.delUser(wxId);
        return  "admin/user/index";
    }

    /**
     * 会员管理-会员审核- 查看审核详情
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param userId        审核用户Id
     * @return
     */
    @PostMapping("/user/queryAnditLog")
    @ResponseBody
    public  ApiResponse queryAnditLog(String  userId){
        System.out.println(userId);
        ServiceMultiResult serviceMultiResult = userAuditLogService.queryUserAuditLog(userId);
        List<User> users = serviceMultiResult.getResult();
        if(users.size() == 0){
            return ApiResponse.ofMessage(1024,"空空");
        }
        return ApiResponse.ofSuccess(users);
    }
    /**
     * 个人中心- 个人信息修改
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId      会员id
     * @param nickname  昵称
     * @param  mobile   手机号码
     * @param  email     邮箱
     * @param  classrole  班级角色名称
     * @return
     */
    @PostMapping("/user/updateMyInfo")
    @ResponseBody
    public  ApiResponse  updateMyIfon(String wxId,String nickname,String  mobile,String email,String classrole){
        ServiceResult<User> result=userService.updateMyIfon(wxId,nickname,mobile,email,classrole);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
    /**
     * 微信端  会员登录
     *
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param pwd   登录密码
     * @param account   登录账号
     * @param session   存放的session
     * @return
     */
    @GetMapping("/user/login")
    @ResponseBody
    public ApiResponse login(String pwd, String account, HttpSession session) throws IOException {
        ServiceResult pwdCorrect = userService.pwdCorrect(account, pwd);

        if (pwdCorrect.getResult() == null) {
            return ApiResponse.ofMessage(1000002, "帐号或密码不正确");
        } else {
            Map map = (Map) pwdCorrect.getResult();
            Object wx_id = map.get("wx_id");
            Object is_enable = map.get("is_enable");
            Object audit_status = map.get("audit_status");
            if (is_enable.toString().equalsIgnoreCase("1")) {
                return ApiResponse.ofMessage(1000003, "非常抱歉，您已被冻结");
            } else if (audit_status.toString().equalsIgnoreCase("0")) {
                return ApiResponse.ofMessage(1000004, "入会申请正在审核中");
            } else if (audit_status.toString().equalsIgnoreCase("1")) {
                return ApiResponse.ofMessage(1000005, "审核未通过，请稍后在登录");
            } else if (audit_status.toString().equalsIgnoreCase("2")) {
                session.setAttribute("loginID", wx_id);
                return ApiResponse.ofSuccess(pwdCorrect.getResult());
            }else{
                return null;
            }
        }
    }
    /**
     * 微信端  查询邮箱是否存在并发送验证码
     *
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param email
     * @return
     */
    @GetMapping("/user/selectEmail")
    @ResponseBody
    public ApiResponse selectEmail(String email,HttpSession session){
        ServiceResult serviceResult = userService.selectEmail(email);
        if (serviceResult.getResult() == null){
            return ApiResponse.ofMessage(10000001, "没有此用户");
        }else{
            boolean sendEmail = userService.sendEmail(email, session);
            if (sendEmail == true){
                return ApiResponse.ofSuccess(serviceResult.getResult());
            }
            return ApiResponse.ofMessage(10000002, "发验证码失败");
        }
    }

    /**
     * 微信端  判断用户填的验证码与发送的验证码是否一致
     *
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param email
     * @return
     */
    @GetMapping("/user/verificationCode")
    @ResponseBody
    public ApiResponse verificationCode(String email,String code,HttpSession session){
        Object generateCode = session.getAttribute("code");
        if (generateCode.toString().equalsIgnoreCase(code)){
            return ApiResponse.ofMessage(10000011, "验证码正确");
        }else{
            return ApiResponse.ofMessage(10000012, "验证码错误");
        }
    }

    /**
     * 微信端 找回密码-修改密码
     *
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param email 邮箱
     * @param password 新密码
     * @return
     */
    @GetMapping("/user/emailUpatepwd")
    @ResponseBody
    public ApiResponse emailUpatepwd(String email,String password){
        ServiceResult update = userService.emailUpdatePasswd(email, password);
        if (update.isSuccess()){
            return ApiResponse.ofSuccess(update.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
}

