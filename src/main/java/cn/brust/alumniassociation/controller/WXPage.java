package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.service.ActivityService;
import cn.brust.alumniassociation.service.EnterpriseService;
import cn.brust.alumniassociation.service.ProjectService;
import cn.brust.alumniassociation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import cn.brust.alumniassociation.mapper.AdvertMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     微信端页面路由
 *     @author 全体成员
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Controller
public class WXPage {
    @Autowired
    AdvertMapper advertMapper;

    @Autowired
    ProjectService projectService;

    @Autowired
    EnterpriseService enterpriseService;

    @Autowired
    UserService userService;

    @Autowired
    ActivityService activityService;

    /**
     *  发布活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @return : java.lang.String
     */
    @GetMapping("/releaseActivity/{id}")
    public String releaseActivity(ModelMap map, @PathVariable("id")String id){
        map.put("id",id);
        return "wx/activity/add";
    }

    /**  <pre>
     *  微信首页
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping(value={"index","home","index.html",""})
    public String index(ModelMap map){
        return "wx/index";
    }


    /**  <pre>
     *  项目对接
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/project")
    public  String project(){
        return "wx/project/projects";
    }


    /**  <pre>
     *  企业推介
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/enterprise")
    public String enterprise(){
        return "wx/enterprise/enterprise";
    }

    /**
     *  根据活动ID查询活动详情并跳转
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @param model :
     * @return : java.lang.String
     */
    @GetMapping("/activity/{id}")
    public String queryDetails(@PathVariable Integer id, ModelMap model){
        ServiceMultiResult serviceMultiResult = activityService.lookupActivityDetails(id);
        List<Map<String,Object>> result = (List<Map<String,Object>>)serviceMultiResult.getResult();
        Map<String, Object> stringStringMap = result.get(0);
        Integer status = (Integer) stringStringMap.get("status");
        String pic_url =(String) stringStringMap.get("pic_url");
        //model.put("activity",result.get(0));
        System.out.println(">>>>>>>>>>>-"+result);
        if(result==null){
            return "activity";
        }
        if(status == 2){
            model.put("activity",result);
            return "/wx/activity/endactivitys";
        }else if(status == 1){
            model.put("activity",result.get(0));
            return "/wx/activity/signactivitys";
        }
        return "/wx/activity/endactivitys";
    }

    /**  <pre>
     * 活动首页
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/activity")
    public String activity(){
        return  "wx/activity/activitys";
    }

    /**  <pre>
     *  微信我的活动
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */
    @GetMapping("wx/myActivities")
    public  String myActivities(){
        return "wx/activity/myActivities";
    }


    /**  <pre>
     *  个人中心
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/personalCenter")
    public  String personalDetails(){
        return "wx/personalCenter/personalCenter";
    }

    /**  <pre>
     *  个人信息
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("wx/topersonalInfo")
    public  String personalInfo(){
        return "wx/personalCenter/personalInfo";
    }

    /**  <pre>
     *  班级通讯录
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("wx/tocontacts")
    public  String toContacts(){
        return "wx/personalCenter/contacts";
    }


    /**  <pre>
     *  同学会
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/tohomecoming")
    public String tohomecoming(String field){
        return "wx/homecoming/homecoming";
    }

    /**  <pre>
     *  项目详情
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/project/{id}")
    public String projectDetail(@PathVariable("id") String id, Map map){
        ServiceResult project = projectService.getProjectById(id);
        map.put("project",project.getResult());
        return "wx/project/detail";
    }

    /**  <pre>
     *  我的项目
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/myProject")
    public String myProject(){
        return "wx/project/myProjects";
    }

    /**  <pre>
     *  联系我们
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/tocontact")
    public String tocontact(String field){
        return "wx/contactus/contact";
    }

    /**  <pre>
     *  意见反馈
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/tofeedback")
    public String tofeedback(){
        return "wx/feedback/feedback";
    }

    /**  <pre>
     *  名片展示
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/tobusinesscard")
    public String tobusinesscard(String id,Map map){
        ServiceResult serviceResult = enterpriseService.queryEnterprisesId(id);
        map.put("businesscard", serviceResult.getResult());
        return "wx/businesscard/businesscard";
    }

    /**  <pre>
     * 企业详情
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/toenterpriseinfo")
    public String toenterpriseinfo(String id,Map map){
        ServiceResult serviceResult = enterpriseService.queryEnterprisesId(id);
        map.put("enterprise", serviceResult.getResult());
        return "wx/enterpriseinfo/enterpriseinfo";
    }

    /**  <pre>
     * 公告首页
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/notice")
    public String notice(){
        return  "wx/notice/notices";
    }
    /**  <pre>
     * 注册
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/register")
    public String register(){
        return  "wx/register";
    }

    /**  <pre>
     * 登录
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/login")
    public String login(){
        return  "wx/login/login";
    }

    @GetMapping("/wx/notices")
    public String notices(){
        return "wx/notice/notices";
    }

    /**  <pre>
     * 找回密码-修改密码
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/toretrieved")
    public String retrieved(String email,Map map){
        map.put("email", email);
        return  "wx/retrievePassword/retrieved";
    }

    /**  <pre>
     * 找回密码-邮箱验证
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/wx/tovalidate")
    public String validate(){
        return  "wx/retrievePassword/validate";
    }
}
