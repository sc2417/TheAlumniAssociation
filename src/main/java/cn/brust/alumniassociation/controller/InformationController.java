package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Information;
import cn.brust.alumniassociation.service.InformationService;
import cn.brust.alumniassociation.utils.FileUtils;
import cn.brust.alumniassociation.utils.UUIDUtils;
import cn.brust.alumniassociation.wx.servlet.WeiXinServlet;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * <pre>
 *     @author 陈润颖
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Controller
public class InformationController {

    @Autowired
    InformationService service;

    @Autowired
    WeiXinServlet weiXinServlet;

    /**  <pre>
     *  查询最新的资讯信息（按时间排序显示置顶的前三条）
     * @author
     * @since 1.0
     * @since JDK 8.0
     * @return : null
     * </pre>
     */
    @GetMapping("/info/queryTheLatestInformation")
    @ResponseBody
    public ApiResponse queryPlatformInfo(HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        ServiceMultiResult<Information> result = service.queryTheLatestInformation();

        if (result != null){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }


    /**  <pre>
     *  分页查询显示符合条件的信息
     * @author 陈润颖
     * @since 1.0
     * @since JDK 8.0
     * @paramnull :
     * @return : null
     * </pre>
     */
    @GetMapping("/info/queryAllInfo")
    @ResponseBody
    public ApiResponse queryAllInfo(Integer page, Integer limit, Integer cid, String title, String time, HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");

        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = result = service.queryInformation( page, limit, cid, title,time);
        Page pages = result.getPage();
        if(result.getPage()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofPage(pages);
    }

    /**  <pre>
     *  分页查询显示符合条件的信息
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @paramnull :
     * @return : null
     * </pre>
     */
    @GetMapping("/info/queryAllInfo2")
    @ResponseBody
    public ApiResponse queryAllInfo2(Integer page, Integer limit, Integer cid, String title, String time,String topicId, HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");

        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = result = service.queryInformation2( page, limit, cid, title,time,topicId);
        Page pages = result.getPage();
        if(result.getPage()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofPage(pages);
    }

    @GetMapping("/info/updStaById")
    @ResponseBody
    public String updStaById(Integer id,Integer enable,HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        service.updStaById(id,enable);
        return "success";
    }

    @PostMapping(value = "/info/upload/img")
    @ResponseBody
    public Object  upload(HttpServletRequest request,@RequestParam("file") MultipartFile file,HttpServletResponse response
    ) throws IOException {
        //保存上传
        String info = FileUtils.saveFile(file, request, "info");
        Map<String,Object> map2=new HashMap<>();
        Map<String,Object> map=new HashMap<>();
        map.put("code",0);
        map.put("msg","上传成功,返回路径给前台");
        map.put("data",map2);
        map2.put("src",info);
        return map;
    }

    /**
     * LayUI 富文本编辑器内嵌图片路径
     */
    //上传文件
    @ResponseBody
    @RequestMapping(value = "/info/textarea/uploadFile")
    public String uploadFile(HttpServletRequest request,@Param("file") MultipartFile file ,
                             HttpServletResponse response) throws IOException {

        File fi = new File("");
        ServletContext context = request.getSession().getServletContext();
        String realPath = context.getRealPath("/upload/" + "info");

        //判断目标文件所在的目录是否存在
        if (file != null) {
            fi = new File("C:\\Users\\Administrator\\Desktop\\作死自助餐\\校友会\\runying\\TheAlumniAssociation-stage\\src\\main\\resources\\static\\upload\\info" + "/" + UUID.randomUUID() + "_" + file.getOriginalFilename());
            if (!fi.getParentFile().exists()) {
                fi.getParentFile().mkdirs();
            }
            System.out.println(fi);
            //将内存中的数据写入磁盘
            file.transferTo(fi);

        }
        //完整的url
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> map2 = new HashMap<String, Object>();
        map.put("code", 0);//0表示成功，1失败
        map.put("msg", "上传成功");//提示消息
        map.put("data", map2);
        map2.put("src", "/upload/info/" + fi.getName());//图片url
        map2.put("title", fi.getName());//图片名称，这个会显示在输入框里
        String result = new JSONObject(map).toString();
        return result;
    }

    @RequestMapping("/info/{id}")
    public String selectInfoById(@PathVariable Integer id,Model model){
        Information information = service.queryInformationById(id);
        model.addAttribute("info",information);
        return  "wx/info/sigleInfo";
    }

    @RequestMapping("/info/add/addInfo")
    //@ResponseBody
    public String addInfo(String title,String cover,String from,String enable,String url,String content,String topicId,String cid ,HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        //int a = 0;
        int a = enable == null ? 1 : 0;
        Information information = new Information(UUIDUtils.getUUID(),title,cover,from,url,content,cid,topicId,1,1,a,new Date(),0,null,null);
        service.addInformation(information);
        return  "admin/info/infoList";
    }

    @RequestMapping("/wx/info")
    public String wxInfo() {
        return  "wx/info/info";
    }

    /**
     * 查询指定的一条资讯跳转到修改页面
     */

    @RequestMapping("/info/edit/{id}")
    public String infoEdit(@PathVariable Integer id, HttpServletResponse response, Model model) {
        Information information = service.queryInformationById(id);
        model.addAttribute("simpleInfo",information);
        return  "admin/info/updInfo";
    }

    @RequestMapping("/info/code")
    public String code() {
        System.out.println("code");
        return  "admin/info/code";
    }

    @GetMapping("/test")
    @ResponseBody
    public Map<String, Object> test(String code) throws IOException {
        String url_get_webpage_token = "https://api.weixin.qq.com/sns/oauth2/access_token?"
                + "appid=wx0f0a4bbe2fc2fc3a"
                + "&secret=a0224f4912b19dbda937fc56c2fd03fd"
                + "&code="+code
                +"&grant_type=authorization_code".trim();
        System.out.println(url_get_webpage_token);

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(url_get_webpage_token).build();
        Response response = okHttpClient.newCall(request).execute();
        String string = response.body().string();
        JSONObject parseObject = JSON.parseObject(string);
        Object openid = parseObject.get("openid");
        Object access_token = parseObject.get("access_token");
        Map<String, Object> map=new HashMap<>();
        map.put("openid", openid);
        map.put("access_token", access_token);
        return map;
    }

    /**
     * 修改
     * @return
     */
    @PostMapping("/info/update/updInfo")
    //@ResponseBody
    public String updInfo(String id,String title,String cover,String from,
                                   String isEnable,String classId,String url,String content,HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        Information information = new Information();
        information.setId(id);
        information.setTitle(title);
        information.setCover(cover);
        information.setFrom(from);
        information.setUrl(url);
        information.setClassId(classId);
        information.setIsEnable(isEnable == null ? 1 : 0);
        information.setContent(content);

        int i =service.updateInfomationById(information);
        System.out.println(i);
        /*ServiceResult serviceResult = service.deleteInformationById(id);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(null);
        }else{
            return ApiResponse.ofStatus(ApiResponse.Status.INTERNAL_SERVER_ERROR);
        }*/
        return  "admin/info/infoList";
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("/info/del/{id}")
    @ResponseBody
    public ApiResponse del(@PathVariable String id,HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        ServiceResult serviceResult = service.deleteInformationById(id);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(null);
        }else{
            return ApiResponse.ofStatus(ApiResponse.Status.INTERNAL_SERVER_ERROR);
        }
    }

    /*@RequestMapping("queryList")
    @ResponseBody
    public ApiResponse queryList(Integer page,Integer limit,Integer cid,String title,String time){
        System.out.println(page);
        System.out.println(limit);
        System.out.println(cid);
        System.out.println(title);System.out.println(time);

        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = result = service.queryInformation( page, limit, cid, title, time);
        if(result.getPage()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofPage(result.getPage());
    }*/
}
