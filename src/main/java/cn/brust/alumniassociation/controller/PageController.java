package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Enterprise;
import cn.brust.alumniassociation.entity.Project;
import cn.brust.alumniassociation.entity.User;
import cn.brust.alumniassociation.service.*;
import cn.brust.alumniassociation.utils.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * 后台页面路由
 */
@Controller
public class PageController {

    @Autowired
    ProjectService projectService;

    @Autowired
    EnterpriseService enterpriseService;

    @Autowired
    ClassyService classyService;

    @Autowired
    UserService userService;

    @Autowired
    ActivityService activityService;
    private String id;
    private Map<String, Object> map;

    /**  <pre>
     *  后台首页
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping(value = {"/admin/index","/admin","/admin/index.html","/admin/home"})
    public  String index(){
        return "admin/index";
    }


    /**  <pre>
     *  企业 - 企业列表
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/admin/enterprise")
    public String enterprise(){
        return "admin/enterprise/list";
    }


    /**  <pre>
     *  企业 - 企业审核
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/admin/enterprise/audit")
    public String enterpriseAudit(){
        return "admin/enterprise/audit";
    }
    /**  <pre>
     *  企业 - 添加企业
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/admin/enterprise/add")
    public String addEnterprise(){
        return "admin/enterprise/add";
    }

    /**  <pre>
     *  企业 - 编辑企业
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/admin/enterprise/edit/{id}")
    public String editEnterprise(@PathVariable("id") String id,Map<String,Object> map){
        ServiceResult result = enterpriseService.queryEnterpriseInfoById(id);
        map.put("enterprise",result.getResult());
        return "admin/enterprise/edit";
    }

    /**  <pre>
     *  企业审核 -- 查看 -- 企业审核信息
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param  :
     * @return :
     * </pre>
     */
    @GetMapping("/admin/enterprise/auditInfo/{id}")
    public String queryAuditInfo(@PathVariable("id") String id,Map<String,Object> map){
        ServiceResult result = enterpriseService.queryEnterpriseInfoById(id);
        map.put("enterprise",result.getResult());
        return "admin/enterprise/auditInfo";
    }

    /**  <pre>
     *  添加企业信息
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param enterprise : 企业对象
     * @param file : 企业LOGO
     * @param request : HttpServletRequest
     * @return :
     * </pre>
     */
    @PostMapping("/enterprise/add")
    public String add(Enterprise enterprise, MultipartFile file, HttpServletRequest request) throws IOException {
        ServiceResult result = enterpriseService.add(enterprise, file, request);
        return  "admin/enterprise/list";
    }


    /**  <pre>
     *  编辑企业
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @PutMapping("/enterprise/edit")
    public String edit(Enterprise enterprise, MultipartFile file, HttpServletRequest request) throws  IOException{
        ServiceResult result = enterpriseService.update(enterprise, file, request);
        return  "admin/enterprise/list";
    }

    /**  <pre>
     * 后台管理 - 项目列表
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0

     * @return : java.lang.String
     * </pre>
     */
    @GetMapping("/admin/project")
    public String projectLlist(){
        return "admin/project/list";
    }

    /**  <pre>
     * 后台管理 - 审核列表
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @return : java.lang.String
     * </pre>
     */
    @GetMapping("/admin/project/audit")
    public String projectAudit(){
        return "admin/project/audit";
    }

    /**  <pre>
     * 后台管理 - 项目列表['编辑edit','查看-跟进']
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param path : 编辑 edit 查看check -跟进 record
     * @param id : 项目ID
     * @param map :
     * @return : java.lang.String
     * </pre>
     */
    @GetMapping("/admin/project/{path}/{id}")
    public String projectDetail(@PathVariable("path") String path,@PathVariable("id") String id, Map map){
        //加入security后再处理
//        if(path != "edit"){
////            return "URL有误";
////        }

        ServiceResult project = projectService.getProjectRecord(id);
        ServiceMultiResult classy = classyService.queryAllClassy();
        Map<String, Object> record = (Map<String, Object>) project.getResult();
        Integer classId  = (Integer)record.get("classId");
        ServiceMultiResult<User> users = userService.selectAllByClassId(classId);
        map.put("path",path);
        map.put("project",project.getResult());
        map.put("classy",classy.getResult());
        map.put("users",users.getResult());
        return "admin/project/detail";
    }

    /**  <pre>
     * 后台管理 - 添加项目
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param map :
     * @param session :
     * @return : java.lang.String
     * </pre>
     */
    @GetMapping("/admin/project/insert")
    public String projectDetail(Map map, HttpSession session){
        ServiceMultiResult classy = classyService.queryAllClassy();
        map.put("classy",classy.getResult());
        map.put("path","insert");
        return "admin/project/insert";
    }

    @PostMapping("/admin/project/insert")
    public String insertProject(HttpServletRequest request, Project project, MultipartFile file){
        String path = null;
        try {
            path = FileUtils.saveFile(file, request, "projectFile");
        } catch (IOException e) {
            e.printStackTrace();
        }
        ServiceResult result = projectService.addProject(project, path);
        return "redirect:/admin/project/check/"+result.getResult();
    }

    @PutMapping("/admin/project/edit/{id}")
    public String updateProject(HttpServletRequest request, Project project, MultipartFile file,@PathVariable("id")String id){
        String path = null;
        try {
            path = FileUtils.saveFile(file, request, "projectFile");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //project.setEnclosure(path);
        project.setId(id);
        ServiceResult result = projectService.updateProjectById(project);
        return "redirect:/admin/project/check/"+id;
    }


    /**
     *  运营 - 广告管理 - 广告列表
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     */
    @GetMapping("/advert/index")
    public  String advert(){
        return  "admin/advert/index";
    }


    /**  <pre>
     *  运营 - 广告管理 - 添加广告
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/advert/form")
    public String addAdvert(){
        return  "admin/advert/add";
    }

    /**  <pre>
     *  运营 - 广告管理 - 编辑广告
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/admin/advert/edit")
    public String editAdvert(){
        return  "admin/advert/edit";
    }

    /**
     * 班级 - 班级列表
     * @return
     */
    @RequestMapping("/classy/index")
    public String classy(){ return  "admin/classy/index";}

    /**
     * 班级 - 添加班级
     * 班级 - 编辑班级
     * @return
     */
    @RequestMapping("/classy/form")
    public String addClass(){ return  "admin/classy/add";}

    /**
     * 班级 - 班级角色列表
     * @return
     */
    @RequestMapping("/user/index")
    public String user(){ return  "admin/user/index";}

    /**
     * 班级 - 编辑班级
     * @return
     */
    @RequestMapping("/classy/edit")
    public String editClassy(){ return  "admin/classy/edit"; }


    /**
     *  活动 - 活动管理 - 活动列表
     *  活动 - 活动管理 - 活动列表
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @return : java.lang.String
     *
     */
    @GetMapping("/admin/activity")
    public String activityLlist(){
        return  "admin/activity/list";}

    /**
     * 活动 - 活动管理 - 发布活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @return : java.lang.String
     *
     */
    @GetMapping("admin/activityadd")
    public String activityAdd(){
        return  "admin/activity/add";
    }

    /**
     * 活动 - 编辑活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id : ID标识
     * @param map : 页面数据
     * @return : java.lang.String
     */
    @GetMapping("/admin/activity/edit/{id}")
    public String activityEdit(@PathVariable("id") String id, ModelMap map){
        ServiceResult result = activityService.selectById(id);
        map.put("activity",result.getResult());
        return  "admin/activity/edit";
    }

    /**
     *  活动 - 活动详情
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @param map :
     * @return : java.lang.String
     */
    @GetMapping("/admin/activity/see/{id}")
    public String activitySee(@PathVariable("id") int id, ModelMap map){
        ServiceMultiResult result = activityService.detailsFindById(id);
        System.err.println(result.getResult());
        map.put("activity",result.getResult());
        return  "admin/activity/see";
    }


    /**  <pre>
     *  内容 - 关于同学会 - 学院介绍
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/admin/todetails")
    public String tohomecoming(){
        return  "admin/homecoming/details";
    }


    /**  <pre>
     *  内容 - 关于同学会 - 同学会介绍
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return : 同学会介绍页面
     * </pre>
     */
    @GetMapping("/admin/toclassmate")
    public String toclassmate(){
        return  "admin/homecoming/classmate";
    }


    /**  <pre>
     *  内容 - 关于同学会 - 同学会章程
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return : 同学会章程页面
     * </pre>
     */
    @GetMapping("/admin/tostatutes")
    public String tostatutes(){
        return  "admin/homecoming/statutes";
    }


    /**  <pre>
     *  首页 - 账户设置
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @return : 账户设置页面
     * </pre>
     */
    @GetMapping("/admin/toMemberSetting")
    public String toupdatePasswd(){
        return "admin/member/updatepasswd";
    }

    /**  <pre>
     *
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/admin/notice")
    public String notice(){
        return  "admin/notice/notice";
    }


    /**  <pre>
     *  系统首页
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @return : 系统首页
     * </pre>
     */
    @GetMapping("/admin/index/main")
    public String main(){
        return "admin/index/main";
    }


    /**  <pre>
     *  资讯列表
     * @author 陈润颖
     * @since 1.0
     * @since JDK 8.0
     * @return : 资讯列表页面
     * </pre>
     */
    @GetMapping("/admin/infomation")
    public String infoList(){
        return "admin/info/infoList";
    }

    /**  <pre>
     *  添加资讯
     * @author 陈润颖
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/admin/addInfo")
    public String addInfo(){
        return "admin/info/addInfo";
    }

    /**  <pre>
     *  内容 平台信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return : 平台信息页面
     * </pre>
     */
    @GetMapping("/admin/toplatform")
    public String toplatform(){
        return  "admin/platform/platform";
    }

    /**  <pre>
     *  会员管理 会员列表
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @return : 会员管理 会员列表页面
     * </pre>
     */
    @GetMapping("/admin/user/list")
    public String menberList(){
        return "admin/user/list";
    }
    /**  <pre>
     *  会员管理 会员审核
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @return : 会员管理 会员审核页面
     * </pre>
     */
    @GetMapping("/admin/user/audit")
    public String autid(){
        return "admin/user/audit";
    }

    /**  <pre>
     *  会员管理 - 会员列表 -会员详情
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @return : 会员管理 - 会员列表 -会员详情页面
     * </pre>
     */
    @GetMapping("/admin/user/toinfo/{wxId}")
    public  String Ifon(@PathVariable("wxId") String  wxId,Map<String,Object> map){
        System.out.println(wxId);
        ServiceResult result = userService.queryHomecomingDynamic(wxId);
        map.put("users",result.getResult());
        System.out.println(map);
        return "admin/user/info";
    }

    @GetMapping("/admin/user/toauditinfo/{wxId}")
    public  String toauditInfo(@PathVariable("wxId") String  wxId,Map<String,Object> map){
        System.out.println("1111"+wxId);
        ServiceResult result = userService.queryHomecomingDynamic(wxId);

        map.put("users",result.getResult());
        System.out.println(map);
        return "admin/user/auditInfo";
    }


    @GetMapping("/admin/notice/addNotice")
    public String addNotice(){
        return "admin/notice/addNotice";
    }


    /**  <pre>
     *  反馈列表
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * </pre>
     */
    @GetMapping("/admin/tofeedback")
    public String tofeedback(){
        return  "admin/feedback/feedback";
    }

    /**  <pre>
     *  添加分类页面
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : 添加分类页面
     * </pre>
     */
    @GetMapping("/admin/topic/addTopic")
    public String addTopic(){
        return "admin/topic/addTopic";
    }

    /**  <pre>
     *  分类管理首页
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : 分类管理首页
     * </pre>
     */
    @GetMapping("/admin/topic")
    public String topic(){
        return  "admin/topic/topic";
    }


    /**  <pre>
     * 文件下载
     *      后期云存储不能这样使用
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param path : 文件路径
     * @param request :
     * @return : org.springframework.http.ResponseEntity<byte[]>
     * </pre>
     */
    @GetMapping(value = "/down")
    public ResponseEntity<byte[]> download(String path, HttpServletRequest request) throws IOException {
        String pre = request.getSession().getServletContext().getRealPath("/");
        String filePath = pre + path;
        File file = new File(filePath);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", StringUtils.substringAfter(file.getName(),FileUtils.SEPARATOR));
        System.out.println(file.getPath());
        return new ResponseEntity<byte[]>(org.apache.commons.io.FileUtils.readFileToByteArray(file),headers, HttpStatus.CREATED);
    }
    /**  <pre>
     *  企业审核 -- 查看 -- 企业审核信息
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param  :
     * @return :
     * </pre>
     */
   /* @GetMapping("/admin/enterprise/auditInfo/{id}")
    public String queryAuditInfo(@PathVariable("id") String id,Map<String,Object> map){
        ServiceResult result = enterpriseService.queryEnterpriseInfoById(id);
        map.put("enterprise",result.getResult());
        return "admin/enterprise/auditInfo";
    }*/


    /**
     * <pre>
     *     权限 - 角色列表
     *     @author 马敏婷
     *     @since   1.0
     *     @since   JDK 8.0
     * </pre>
     */
    @GetMapping("/admin/role/list")
    public  String roleList(){
        return "/admin/role/list";
    }

    /**  <pre>
     *  登录首页
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : 登录首页
     * </pre>
     */
    @GetMapping("/admin/login")
    public String login(){
        return  "admin/login";
    }
}
