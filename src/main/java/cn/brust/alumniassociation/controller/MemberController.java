package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.service.ClassyService;
import cn.brust.alumniassociation.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator 2018/8/15 17:44
 */
@Controller
public class MemberController {
    @Autowired
    MemberService memberService;
    @Autowired
    ClassyService classyService;

    @PostMapping("/member/login")
    @ResponseBody
    public ApiResponse login(String mobile, String passwd, String class_id, HttpSession session){
        ServiceResult result = memberService.login(mobile,passwd,class_id);
        if(result.getResult() != null){
            session.setAttribute("memberId",result.getResult());
            return ApiResponse.ofMessage(200,"登录成功!");
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
}