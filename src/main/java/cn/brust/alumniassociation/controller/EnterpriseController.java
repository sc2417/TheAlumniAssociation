package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Enterprise;
import cn.brust.alumniassociation.service.EnterpriseAuditLogService;
import cn.brust.alumniassociation.service.EnterpriseService;
import cn.brust.alumniassociation.utils.FileUtils;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.text.Style;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
* <pre>
*     @author 马敏婷
*     @since   1.0
*     @since   JDK 8.0
* </pre>
*/
@Controller
public class EnterpriseController {

    @Autowired
    EnterpriseService service;

    @Autowired
    EnterpriseAuditLogService enterpriseAuditLogService;



    @GetMapping("/enterprise/PageEnterprise")
    public @ResponseBody ApiResponse PageEnterprise(Integer page,Integer limit){
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = result = service.findPageEnterprise(page,limit);
        if(result.getPage()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofPage(result.getPage());
    }


    /**  <pre>
     *  企业列表展示（筛选查询）
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/enterprise/query")
    public @ResponseBody ApiResponse query(Integer page,Integer limit,String cid,String name,String userName,String releaseTime,String status){
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = result = service.query(cid, name, userName, releaseTime, page, limit);
        if(result.getPage()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofPage(result.getPage());
    }
    /**  <pre>
     *  企业列表展示（筛选查询）
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/enterprise/queryEnterprisesByAudit")
    public @ResponseBody ApiResponse queryEnterprisesByAudit(Integer page,Integer limit,String cid,String name,String userName,String releaseTime,String status){
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = result = service.queryEnterprisesByAudit(cid, name, userName, releaseTime, page, limit);
        if(result.getPage()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofPage(result.getPage());
    }



    /**  <pre>
     *  删除企业信息
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @return : null
     * </pre>
     */
    @DeleteMapping("/enterprise/delete/{id}")
    public @ResponseBody ApiResponse delete(@PathVariable("id") String id){
        ServiceResult result = service.delete(id);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return  ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**  <pre>
     *  根据企业ID查询企业详情
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @return : null
     * </pre>
     */
    @GetMapping("/enterprise/queryById/{id}")
    public @ResponseBody ApiResponse queryById(@PathVariable String id){
        ServiceResult result = service.queryEnterpriseInfoById(id);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }



    /**  <pre>
     *  根据企业id修改企业审核状态
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @param auditStatus : 审核状态
     * @param datails : 审核信息
     * </pre>
     */
    @RequestMapping("/enterprise/updateAuditStatus")
    public @ResponseBody ApiResponse updateAuditStatus(String eid,Integer auditStatus,String datails){
        String id=UUID.randomUUID().toString();
        System.out.println(eid);
        System.out.println(auditStatus);
        System.out.println(datails);

        Date createTime=new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(createTime);
        System.out.println(dateString);

        String name="admin";

        ServiceResult serviceResult = enterpriseAuditLogService.addAuditLog(id, eid, datails,auditStatus, dateString, name);
        if (serviceResult.isSuccess()){

            return ApiResponse.ofSuccess(serviceResult.getResult());
        }
        return ApiResponse.ofMessage(500,"状态修改失败");
    }
    /**  <pre>
     *  修改企业状态：上架、下架
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @param status : 企业状态
     * @return : null
     * </pre>
     */
    @PutMapping("/enterprise/updateStatus/{id}")
    public @ResponseBody ApiResponse updateStatus(@PathVariable("id") String id,String status){
        ServiceResult result = service.updateStatus(id,status);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofMessage(500,"状态修改失败");
    }

    /**  <pre>
     *  根据用户ID查询企业详情
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return : null
     * </pre>
     */
    @GetMapping("/enterprise/queryInfoUserId")
    public @ResponseBody ApiResponse queryInfoById(String userId){
        System.out.println("---"+userId);
        ServiceResult result = service.queryInfo(userId);
        System.out.println("---"+result.getResult());
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }

        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**  <pre>
     *  用户提交数据之后的添加或修改
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param logo  企业LOGO
     * @param name  企业名称
     * @param website   企业网站
     * @param tel1  企业电话
     * @param business 公司业务
     * @param introduction  企业简介
     * @param classId   企业所属班级
     * @param userId    企业所属人
     * @param positions 职位
     * @return : null
     * </pre>
     */
    @GetMapping("/enterprise/submitInfo")
    public @ResponseBody ApiResponse submitInfo(String logo,String name,String website,String tel1,String business,String introduction,String classId,String userId,String positions){
        ServiceResult result = service.queryInfo(userId);
        String UUIDs = UUID.randomUUID().toString();
        ServiceResult queryEnterprisesId = service.queryEnterprisesId(UUIDs);
        if (queryEnterprisesId.getResult().toString().equalsIgnoreCase("[]")){
            Enterprise enterprise = new Enterprise();
            enterprise.setId(UUIDs);
            enterprise.setLogo(logo);
            enterprise.setName(name);
            enterprise.setWebsite(website);
            enterprise.setTel1(tel1);
            enterprise.setBusiness(business);
            enterprise.setIntroduction(introduction);
            enterprise.setClassId(classId);
            enterprise.setUserId(userId);
            enterprise.setPositions(positions);
            ServiceResult serviceResult = null;
            if (result.getResult() == null){
                serviceResult = service.insertInfo(enterprise);
            }else{
                serviceResult = service.updateInfo(enterprise);
            }
            if (serviceResult.isSuccess()){
                return ApiResponse.ofSuccess(result.getResult());
            }
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
        }else{
            submitInfo(logo, name, website, tel1, business, introduction, classId, userId, positions);
        }
        return null;
    }
}