package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Project;
import cn.brust.alumniassociation.service.ProjectService;
import cn.brust.alumniassociation.utils.FileUtils;
import com.github.pagehelper.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2018/7/26 0026.
 */
@Controller
public class ProjectController {

    @Autowired
    ProjectService projectService;

    /**  <pre>
     * 前端api 项目分页
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param page :页数
     * @param limit :页数显示条数
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @GetMapping("/project/queryProjects")
    public @ResponseBody ApiResponse queryProjects(Integer page, Integer limit) {
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult projects = projectService.getProjects(page, limit, "1");
        Page result = projects.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

    /**  <pre>
     * wx前端 - 我的项目
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param page : 页数
     * @param limit : 页码
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @GetMapping("/project/my")
    public @ResponseBody ApiResponse queryMyProjects(Integer page, Integer limit) {
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        //  得到用户
        ServiceMultiResult projects = projectService.getProjectsByUserid(page,limit,"1");
        Page result = projects.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

    /**  <pre>
     *  后台管理-项目列表
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param page : 页数
     * @param limit : 页码
     * @param classId : 班级ID
     * @param budget : 项目预算
     * @param nameOrMobile : 名字或者手机
     * @param time : 开始时间-结束时间
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @PostMapping("/amdin/project/getProjectsByKeyWord")
    public @ResponseBody ApiResponse getProjectsByKeyWord(Integer page, Integer limit,
                                                          String classId,Integer budget,
                                                          String nameOrMobile,String time){
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        String startTime ="",endTime = "";
        if(StringUtils.isNotBlank(time)){
            String[] split = StringUtils.split(time,"~");
            startTime = split[0];
            endTime = split[1];
        }

        ServiceMultiResult projects = projectService.getProjectsByKeyWord(page, limit, classId, budget, nameOrMobile, startTime, endTime);
        Page result = projects.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

    /**  <pre>
     *  后台管理 - 审核列表
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param page : 页数
     * @param limit : 页码
     * @param classId : 班级ID
     * @param budget : 项目预算
     * @param nameOrMobile : 名字和手机
     * @param time : 开始时间-结束时间
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @PostMapping("/amdin/project/getAuditProjectsByKeyWord")
    public @ResponseBody ApiResponse getAuditProjectsByKeyWord(Integer page, Integer limit,
                                                               String classId,Integer budget,
                                                               String nameOrMobile,String time){
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        String startTime ="",endTime = "";
        if(StringUtils.isNotBlank(time)){
            String[] split = StringUtils.split(time,"~");
            startTime = split[0];
            endTime = split[1];
        }
        ServiceMultiResult projects = projectService.getAuditProjectsByKeyWord(page, limit, classId, budget, nameOrMobile, startTime, endTime);
        Page result = projects.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

//    删除del/下架out/发布release
    /**  <pre>
     * 后台管理 - 项目列表 -删除
     * 根据项目ID删除
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id : 项目ID
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @PostMapping("/admin/project/del/{id}")
    @ResponseBody
    public ApiResponse del(@PathVariable String id){
        ServiceResult serviceResult = projectService.deleteProjectById(id);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(null);
        }else{
            return ApiResponse.ofStatus(ApiResponse.Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**  <pre>
     * 后台管理 - 项目列表 -下架
     * 根据项目ID下架
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id : 项目ID
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @PostMapping("/project/out/{id}")
    @ResponseBody
    public ApiResponse out(@PathVariable String id){
        ServiceResult serviceResult = projectService.updateStatus(id, 1);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(null);
        }else{
            return ApiResponse.ofStatus(ApiResponse.Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**  <pre>
     * 后台管理 - 项目列表 - 发布项目
     * 根据项目ID修改项目状态
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id : 项目ID
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @PostMapping("/project/release/{id}")
    @ResponseBody
    public ApiResponse release(@PathVariable String id){
        ServiceResult serviceResult = projectService.updateStatus(id, 2);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(null);
        }else{
            return ApiResponse.ofStatus(ApiResponse.Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**  <pre>
     * 后台管理 - 项目跟进 - 跟进记录
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @param msg :录入信息
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @PostMapping("/admin/project/continue/{id}")
    @ResponseBody
    public ApiResponse recordContinue(@PathVariable("id")String id,String msg){
        //项目状态 0未发布 1已取消 2发布中 3跟进中 4已签单
        ServiceResult serviceResult = projectService.updateProjectRecord(id, 3, msg);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(null);
        }else{
            return ApiResponse.ofStatus(ApiResponse.Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**  <pre>
     * 后台管理 - 项目跟进 - 确认签单
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @param msg :录入信息
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @PostMapping("/admin/project/enter/{id}")
    @ResponseBody
    public ApiResponse recordEnter(@PathVariable("id")String id,String msg){
        //项目状态 0未发布 1已取消 2发布中 3跟进中 4已签单
        ServiceResult serviceResult = projectService.updateProjectRecord(id, 4, msg);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(null);
        }else{
            return ApiResponse.ofStatus(ApiResponse.Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**  <pre>
     * 后台管理 - 项目跟进 - 取消签单
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @param msg :录入信息
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @PostMapping("/admin/project/cancel/{id}")
    @ResponseBody
    public ApiResponse recordCancel(@PathVariable("id")String id,String msg){
        //项目状态 0未发布 1已取消 2发布中 3跟进中 4已签单
        ServiceResult serviceResult = projectService.updateProjectRecord(id, 1, msg);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(null);
        }else{
            return ApiResponse.ofStatus(ApiResponse.Status.INTERNAL_SERVER_ERROR);
        }
    }

    /*@PutMapping("/admin/project/auditLog")
    @ResponseBody
    public ApiResponse updateAuditLog(Project project){
        ServiceResult serviceResult = projectService.updateAuditStatu(project.getId(), project.getAuditStatus(), project.getAuditLog());
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(null);
        }else{
            return ApiResponse.ofStatus(ApiResponse.Status.INTERNAL_SERVER_ERROR);
        }
    }*/
    @PostMapping("/wx/project/updateProject")
    public String wxUpdateProject(HttpServletRequest request, String id, String status, String introduction, String  budget, String endTime, String other, String fileName, MultipartFile enclosure){
        System.out.println("update");
        String path = null;
        if(enclosure != null && enclosure.getSize()!= 0){
            try {
                path = FileUtils.saveFile(enclosure, request, "projectFile");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            path = fileName;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Project project = new Project();
        project.setId(id);
        project.setEndTime(date);
        project.setIntroduction(introduction);
        project.setBudget(Integer.valueOf(budget));
        project.setOther(other);
        project.setStatus(Integer.valueOf(status));
        project.setEnclosure(path);
        System.out.println("project===="+project);
        projectService.wxUpdateProject(project);
        return "";
    }
}
