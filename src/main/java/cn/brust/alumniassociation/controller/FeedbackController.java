package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.service.FeedbackService;
import cn.brust.alumniassociation.utils.FileUtils;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *     @author
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Controller
public class FeedbackController {

    @Autowired
    FeedbackService feedbackService;

    /**  <pre>
     *  删除反馈信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param id 反馈ID
     * @return
     */
    @GetMapping("/feedback/delete")
    @ResponseBody
    public ApiResponse delete(Integer id){
        ServiceResult delete = feedbackService.delete(id);
        if (delete.isSuccess()){
            return ApiResponse.ofSuccess(delete.getResult());
        }
        return  ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     *  修改处理状态以及处理备注
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param id 反馈ID
     * @param handle 处理备注
     * @return
     */
    @GetMapping("/feedback/handle")
    @ResponseBody
    public ApiResponse handle(Integer id, String handle){
        ServiceResult update = feedbackService.handle(id, handle);
        if (update.isSuccess()){
            return ApiResponse.ofSuccess(update.getResult());
        }
        return  ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     *  添加一条反馈信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param opinion 反馈内容
     * @param picture 反馈照片
     * @param userId 反馈者ID
     * @return
     */
    @GetMapping("/feedback/add")
    @ResponseBody
    public ApiResponse add(String opinion,String picture) {
        String userId = "UID82589216";
        HashMap map = new HashMap();
        ServiceResult add = feedbackService.add(opinion, userId);
        if (add.isSuccess()) {
            return ApiResponse.ofSuccess(add.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     *  查询反馈信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param page 分页的第几页
     * @param limit 分页几条数据
     * @param releaseTime 要查询的时间段
     * @return
     */
    @GetMapping("/feedback/query")
    public @ResponseBody ApiResponse query(Integer page, Integer limit, String releaseTime){
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = result = feedbackService.queryAll(releaseTime, page, limit);
        if(result.getPage()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofPage(result.getPage());
    }

    /**
     * 图片上传
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param servletRequest
     * @param file
     * @return
     * @throws IOException
     */
    @RequestMapping(value="/feedback/uploading",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> upload(HttpServletRequest servletRequest,@RequestParam("file") MultipartFile file) throws IOException {
        String platform = FileUtils.saveFile(file, servletRequest, "feedback");
        Map<String, Object> res = new HashMap<>();
        res.put("url", platform);
        return res;
    }
}
