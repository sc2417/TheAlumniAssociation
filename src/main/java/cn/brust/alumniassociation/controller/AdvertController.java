package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.entity.Advert;
import cn.brust.alumniassociation.service.AdvertService;
import cn.brust.alumniassociation.utils.FileUtils;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * <pre>
 *     @author 郑锦波、孙超
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Controller
public class AdvertController {
    @Autowired
    AdvertService advertService;

    @GetMapping("/advert/queryAdverts")
    public @ResponseBody ApiResponse queryAdverts(){
        ServiceMultiResult<Advert> result = advertService.queryAdverts();
        if (result != null){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**  <pre>
     *  查询所有广告信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param page : 显示的页数
     * @param limit: 每页显示的条数
     * @return : ApiResponse，封装接口返回信息
     * </pre>
     */
    @GetMapping("/advert/findAll")
    public @ResponseBody ApiResponse findAll(Integer page, Integer limit,Model model){
        if(page == null || limit == null){
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult advert  = advertService.findAllLimit(page, limit);
        model.addAttribute("adverts",advert);
        Page result = advert.getPage();
        if(result == null){
            return ApiResponse.ofMessage(102,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

    /**  <pre>
     *  筛选查询符合条件的广告信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @return : null
     * </pre>
     */
    @GetMapping("/advert/vagueQuery")
    public @ResponseBody ApiResponse vagueQuery(String title,Integer site,Integer page, Integer limit){
        //0表示查询全部
        if(site == 0){
            ServiceMultiResult result = result = advertService.vagueQueryAll(title, page, limit);
            if(result.getPage() == null){
                return ApiResponse.ofMessage(102,"没有更多了");
            }
            return ApiResponse.ofPage(result.getPage());
        }else {
            ServiceMultiResult result = result = advertService.vagueQuery(title,site,page,limit);
            if(result.getPage() == null){
                return ApiResponse.ofMessage(102,"没有更多了");
            }
            return ApiResponse.ofPage(result.getPage());
        }
    }

    /**  <pre>
     *  添加广告信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @return : null
     * </pre>
     */
    @RequestMapping("/advert/add")
    public String add(String title,Integer site,String url,String desc,Integer isStatus,Integer isTop,String cover,HttpServletRequest request) throws IOException {
        Date date = new Date();
        Advert advert = new Advert();
        advert.setTitle(title);
        advert.setSite(site);
        advert.setCover(cover);
        advert.setUrl(url);
        advert.setDesc(desc);
        advert.setIsStatus(isStatus);
        advert.setCteateDate(date);
        advert.setIsTop(isTop);
        advertService.addAdvert(advert);
        return  "admin/advert/index";
    }

    /**  <pre>
     *  根据ID删除广告
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @return : null
     * </pre>
     */
    @GetMapping("/advert/del")
    public String del(int id){
        advertService.delAdvert(id);
        return  "admin/advert/index";
    }

    
    /**  <pre>
     *  根据ID查询广告信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @return : null
     * </pre>
     */
    @GetMapping("/advert/queryById")
    public String queryById(Integer id, Model model){
        Advert advert = advertService.queryById(id);
        model.addAttribute("advertlist",advert);
        return  "admin/advert/edit";
    }

    
    /**  <pre>
     *  编辑广告信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @return : 跳转到广告列表页面
     * </pre>
     */
    @RequestMapping("/advert/update")
    public String update(String title,Integer site,String cover,String url,String desc,Integer isStatus,Integer isTop,Integer id){
        advertService.updateAdvert(title,site,url,cover,desc,isStatus,isTop,id);
        return  "admin/advert/index";
    }

    /**
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * 根据id置顶广告
    */
    @RequestMapping("/advert/settop")
    public String settop(Integer id,HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter writer = response.getWriter();
        Advert advert = advertService.queryCount();
        System.out.println(advert.getCounts());
        if(advert.getCounts() >= 5){
            writer.print("<script>alert('您的置顶个数已达上限，置顶失败！');window.location.href='/advert/index';</script>");
        }else {
            advertService.settop(id);
            return  "admin/advert/index";
        }
        return  null;
    }

    /**
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * 根据id取消置顶
     */
    @RequestMapping("/advert/canceltop")
    public String canceltop(Integer id){
        advertService.canceltop(id);
        return  "admin/advert/index";
    }
    @RequestMapping(value = "/advert/upload")
    @ResponseBody
    public Object  upload(HttpServletRequest request,@RequestParam("file") MultipartFile file,HttpServletResponse response
    ) throws IOException {
        //保存上传
        OutputStream out = null;
        InputStream fileInput=null;
        File fi = new File("");
        ServletContext context = request.getSession().getServletContext();
        String realPath = context.getRealPath("/upload/"+"info");
        try{
            if(file!=null){
                fi = new File("E:\\TheAlumniAssociation-zjb - 副本\\src\\main\\resources\\static\\upload\\info" + "/" + UUID.randomUUID() + "_" + file.getOriginalFilename());
                if(!fi.getParentFile().exists()) {
                    fi.getParentFile().mkdirs();
                }
                file.transferTo(fi);
            }
        }catch (Exception e){
        }finally{
            try {
                if(out!=null){
                    out.close();
                }
                if(fileInput!=null){
                    fileInput.close();
                }
            } catch (IOException e) {
            }
        }
        Map<String,Object> map2=new HashMap<>();
        Map<String,Object> map=new HashMap<>();
        map.put("code",0);
        map.put("msg","上传成功,返回路径给前台");
        map.put("data",map2);
        map2.put("src","/upload/info/"+fi.getName());
        return map;
    }

}
