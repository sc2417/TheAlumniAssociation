package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.service.TopicService;
import cn.brust.alumniassociation.utils.UUIDUtils;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Administrator on 2018/8/2 0002.
 */
@Controller
public class TopicController {
    @Autowired
    TopicService topicService;
    /**  <pre>
     *  后台管理 -  查询所有分类（分页查询）
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @param page :分页
     * @param limit :分页
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @RequestMapping("/topic/findTopics")
    public @ResponseBody
    ApiResponse findNotices(Integer page, Integer limit) {
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult topics = topicService.getTopics(page,limit);
        Page result = topics.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

    @RequestMapping("/topic/getAllTopic")
    public @ResponseBody
    ApiResponse getAllTopic() {
        ServiceMultiResult allTopic = topicService.getAllTopic();
        return ApiResponse.ofSuccess(allTopic.getResult());
    }

    /**
     * 软删
     * @author 陈佳婷
     * @param id 分类ID
     * @return
     */
    @RequestMapping("/topic/delete")
    public @ResponseBody
    ApiResponse delete(String id) {
        ServiceResult result = topicService.updateDel(id);
        if (result.isSuccess()) {
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     * 更改分类显示状态
     * @author 马丹萍
     * @param id 分类ID
     * @param enable 0显示 1不显示
     * @return: cn.brust.alumniassociation.base.ApiResponse
     */
    @RequestMapping("/topic/updateEnable")
    public @ResponseBody
    ApiResponse updateEnable(Integer enable, String id) {
        System.out.println("upd");
        ServiceResult result = topicService.updateEnable(enable,id);
        if (result.isSuccess()) {
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     * 添加分类
     * @author 马丹萍
     * @param title :标题
     * @param enable :显示状态
     * @param sort :排序
     * @param title :标题
     * @return: cn.brust.alumniassociation.base.ApiResponse
     */
    @RequestMapping("/topic/add")
    @ResponseBody
    public ApiResponse add(String title, Integer sort, Integer enable){
        String id = UUIDUtils.getUUID();
        ServiceResult result = topicService.add(id,title,sort,enable);
        if(result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return  ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     * 根据分类ID查询分类详情
     * @author 马丹萍
     * @param id 分类ID
     * @return: cn.brust.alumniassociation.base.ApiResponse
     */
    @GetMapping("/topic/queryById")
    public @ResponseBody
    ApiResponse queryById(String id){
        ServiceResult result = topicService.queryTopicInfoById(id);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     * 编辑分类
     * @author 马丹萍
     * @param id :分类ID
     * @param enable :显示状态
     * @param sort :排序
     * @param title :标题
     * @return: cn.brust.alumniassociation.base.ApiResponse
     */
    @RequestMapping("/topic/update")
    public @ResponseBody
    ApiResponse update(Integer enable, String id, Integer sort, String title) {
        ServiceResult result = topicService.updateTopic(enable,id,sort,title);
        if (result.isSuccess()) {
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
    @GetMapping("/topic/queryTopic")
    public @ResponseBody ApiResponse queryTopic(){
        ServiceResult result=topicService.selectTopic();
        if(result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
}
