package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Role;
import cn.brust.alumniassociation.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator 2018/8/13 16:08
 */
@Controller
public class RoleController {

    @Autowired
    RoleService roleService;

    @GetMapping("/role/selectById/{id}")
    public @ResponseBody ApiResponse selectById(@PathVariable String id){
        ServiceResult result = roleService.selectById(id);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofMessage(500,"找不到了");
    }

    @GetMapping("/role/selectAll")
    public @ResponseBody ApiResponse selectAll(Integer page,Integer limit,String classId){
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = roleService.selectAll(page, limit, classId);
        if (result.getPage() == null){
            return ApiResponse.ofMessage(500,"找不到了");
        }
        return ApiResponse.ofPage(result.getPage());
    }

    @PutMapping("/role/update/{id}")
    public @ResponseBody ApiResponse update(@PathVariable String id,Role role){
        ServiceResult result = roleService.update(role, id);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess("编辑成功");
        }
        return ApiResponse.ofMessage(500,"编辑失败");
    }

    @DeleteMapping("/role/delete/{id}")
    public @ResponseBody ApiResponse delete(@PathVariable String id){
        ServiceResult result = roleService.delete(id);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess("删除成功");
        }
        return ApiResponse.ofMessage(500,"删除失败");
    }

    @PostMapping("/role/add")
    public @ResponseBody ApiResponse add(Role role){
        ServiceResult result = roleService.add(role);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess("添加成功");
        }
        return ApiResponse.ofMessage(500,"添加失败");
    }

    @PutMapping("/role/updateIsEnable/{id}")
    public @ResponseBody ApiResponse updateIsEnable(@PathVariable("id") String id,Role role){
        ServiceResult result = roleService.update(role, id);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess("修改状态成功");
        }
        return ApiResponse.ofMessage(500,"修改状态失败");
    }

    @GetMapping("/role/selectPermissionByRoleId/{id}")
    public String selectPermissionByRoleId(@PathVariable("id")String id, ModelMap modelMap){
        ServiceMultiResult result = roleService.selectPermissionByRoleId(id);
        modelMap.put("permissionList",result.getResult());
        System.out.println(result.getResult());
        return  "admin/role/permissionSettings";
    }

}