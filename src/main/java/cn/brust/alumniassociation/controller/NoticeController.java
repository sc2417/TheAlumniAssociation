package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Notice;
import cn.brust.alumniassociation.service.NoticeService;
import cn.brust.alumniassociation.utils.UUIDUtils;
import com.github.pagehelper.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <pre>
 *     @author 陈佳婷、马丹萍
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Controller
public class NoticeController {
    @Autowired
    private NoticeService noticeService;


    /**
     * <pre>
     *  查询所有的公告信息
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @paramnull :
     * @return : null
     * </pre>
     */
    @GetMapping("/notice/findAllNotice")
    @ResponseBody
    public List<Notice> findAllNotice() {
        return noticeService.findAll();
    }

    @RequestMapping("/notice/findNotices")
    public @ResponseBody
    ApiResponse findNotices(Integer page, Integer limit, String title, HttpServletRequest request, String date) {
        String startTime ="",endTime = "";
        if(StringUtils.isNotBlank(date)){
            String[] split = StringUtils.split(date,"~");
            startTime = split[0];
            endTime = split[1];
        }
        System.out.println(date);
        if (page == null || limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult notices = noticeService.getNotices(page, limit,title,startTime,endTime);
        Page result = notices.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

    @RequestMapping("/notice/delete")
    public @ResponseBody
    ApiResponse delete(String id) {
        System.out.println("id=====" + id);
        ServiceResult result = noticeService.updateDel(id);
        if (result.isSuccess()) {
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    @RequestMapping("/notice/add")
    @ResponseBody
    public ApiResponse add(String title, String content){
        Notice notice = new Notice();
        notice.setId(UUIDUtils.getUUID());
        notice.setTitle(title);
        notice.setContent(content);
        System.out.println("content"+content);
        ServiceResult result = noticeService.add(notice);

        if(result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return  ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
    /**  <pre>
     *  公告详情
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */

    @GetMapping("/notice/queryById")
    public @ResponseBody
    ApiResponse queryById(String id){
        ServiceResult result = noticeService.queryNoticeInfoById(id);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

}
