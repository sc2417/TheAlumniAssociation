package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Activity;
import cn.brust.alumniassociation.entity.Enterprise;
import cn.brust.alumniassociation.service.ActivityService;
import cn.brust.alumniassociation.service.EnrollService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class ActivityController {

    @Autowired
    ActivityService activityService;

    @Autowired
    EnrollService enrollService;

    /**
     * 参加报名人
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ApiResponse
     */
    @GetMapping("/admin/activity/enrolment/{id}")
    public @ResponseBody ApiResponse enrolment(@PathVariable("id") int id){
        ServiceMultiResult result = activityService.detailsFindById(id);
        if(result.getResult()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofSuccess(result.getResult());
    }

    /**
     *  查看评论
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceResult
     */
    @GetMapping("/admin/activity/comment")
    public @ResponseBody ApiResponse queryComment(int id){
        ServiceResult serviceResult = activityService.queryComment(id);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(serviceResult.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     *  展示动态评论（wx）
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ApiResponse
     */
    @GetMapping("/activity/dynamicQuery")
    public @ResponseBody ApiResponse dynamicQuery(Integer id){
        ServiceMultiResult result = activityService.dynamicQuery(id);
        if(result==null){
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
        }
        return ApiResponse.ofSuccess(result.getResult());
    }

    /**  <pre>
     *  活动点赞(wx)
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param zan :
     * @param id :
     * @return : cn.brust.alumniassociation.base.ApiResponse
     * </pre>
     */
    @GetMapping("/activity/updatefabulous")
    public @ResponseBody ApiResponse updateFabulous(int zan,String id){
        ServiceResult serviceResult = activityService.updateFabulous(zan, id);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(serviceResult.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     * 编辑活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param title :
     * @param cover :
     * @param address :
     * @param type :
     * @param createDate :
     * @param endDate :
     * @param content :
     * @param id :
     * @return : cn.brust.alumniassociation.base.ApiResponse
     */
    @PutMapping("/admin/activity/update")
    public @ResponseBody ApiResponse modifyActivity(String title, String cover, String address, String type, String createDate, String endDate, String content, String id){
        ServiceResult result = activityService.modifyActivity(title,cover,address,type,createDate,endDate,content,id);
        if(result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     * 修改报名状态
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id : ID标识
     * @return : cn.brust.alumniassociation.base.ApiResponse
     */
    @GetMapping("/admin/activity/modify/{id}")
    public @ResponseBody ApiResponse modifyEnroll(@PathVariable(value="id") String id){
        ServiceResult result = activityService.modifyEnroll(id);
        if(result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     *  删除活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id : ID标识
     * @return : cn.brust.alumniassociation.base.ApiResponse
     */
    @GetMapping("/admin/activity/del/{id}")
    public @ResponseBody ApiResponse del(@PathVariable(value="id") String id){
        ServiceResult result = activityService.delActivity(id);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return  ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     *  发布活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param title :
     * @param cover :
     * @param type :
     * @param createDate :
     * @param endDate:
     * @param address :
     * @return : java.lang.String
     */
    @PostMapping("/admin/activity/add")
    public @ResponseBody ApiResponse add(String title,String cover,String address,String type,String createDate,String endDate,String content) throws IOException {
        ServiceResult serviceResult = activityService.addActivity(title, cover,address,type,createDate,endDate,content);
        if(serviceResult.isSuccess()){
            return ApiResponse.ofSuccess(serviceResult.getResult());
        }
        return  ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     *  分页展示活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param page : 显示的页数
     * @param limit : 每页显示的条数
     * @return : cn.brust.alumniassociation.base.ApiResponse
     */
    @GetMapping("/admin/activity/queryVague")
    public @ResponseBody ApiResponse pageQuery(Integer page,Integer limit){
        if(page == null||limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = activityService.queryVague(page,limit);
        if(result.getPage()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofPage(result.getPage());
    }

    /**
     *  筛选查询活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @param title : 标题
     * @param time : 时间
     * @param page : 显示的页数
     * @param limit : 每页显示的条数
     * @param Identification :
     * @return : cn.brust.alumniassociation.base.ApiResponse
     */
    @GetMapping("/admin/activity/pagingquery")
    public @ResponseBody ApiResponse queryVague(Integer classId,String title,String time,Integer page,Integer limit,Integer Identification){
        if(page == null||limit == null) {
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = activityService.queryVagueTime(classId,title,time,page,limit,Identification);
        if(result.getPage()==null){
            return ApiResponse.ofMessage(102,"没有了");
        }
        return ApiResponse.ofPage(result.getPage());
    }


    /**  <pre>
     * 查询进行中活动
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param page 页数
     * @param limit 每页显示几条 :
     * @return : null
     * </pre>
     */
    @GetMapping("/activity/host")
    public @ResponseBody ApiResponse queryActivity(Integer page,Integer limit,Integer cid){
        if(page==null || limit==null || cid==null){
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }

        ServiceMultiResult activity= activityService.queryConductActivity(page,limit,cid);
        Page result=activity.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

    /**  <pre>
     * 查询结束的活动
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param page页数 limit 每页显示几条 :
     * @return : null
     * </pre>
     */
    @GetMapping("/activity/endt")
    public @ResponseBody ApiResponse queryEndActivity(Integer page,Integer limit,Integer cid){
        if(page==null || limit==null || cid==null){
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult activity= activityService.queryEndActivity(page,limit,cid);
        Page result=activity.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

    /**  <pre>
     *  活动报名
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */

    @PostMapping("/enroll")
    @ResponseBody
    public int enroll(String wxId,String acId){
        if(enrollService.queryById(wxId,acId).getResult()==null){
            return enrollService.activityEnroll(wxId,acId);
        }
        return 0;
    }




    @GetMapping("/activityOne")
    @ResponseBody
    public ApiResponse ccc(String cId){
        ServiceResult serviceResult = activityService.getActivityOne(cId);
        return ApiResponse.ofSuccess(serviceResult);
    }


    @GetMapping("/myinactivity")
    @ResponseBody
    public ApiResponse myInActivity(Integer page,Integer limit,String  uId){
        if(page==null || limit==null || uId==null){
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }

        ServiceMultiResult activity= activityService.getMyInActivity(page,limit,uId);
        Page result=activity.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }

    @GetMapping("/myendactivity")
    @ResponseBody
    public ApiResponse myEndActivity(Integer page,Integer limit,String  uId){
        if(page==null || limit==null || uId==null){
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }

        ServiceMultiResult activity= activityService.getMyEndActivity(page,limit,uId);
        Page result=activity.getPage();
        if(result==null){
            return ApiResponse.ofMessage(1024,"没有更多了");
        }
        return ApiResponse.ofPage(result);
    }


}
