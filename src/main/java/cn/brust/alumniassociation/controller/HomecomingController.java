package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Homecoming;
import cn.brust.alumniassociation.service.HomecomingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Controller
public class HomecomingController {

    @Autowired
    HomecomingService homecomingService;


    /**  <pre>
     *  根据field判断查询同学会信息中的哪个模块的内容
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @PostMapping("/homecoming/queryhomecoming")
    @ResponseBody
    public ApiResponse queryhomecoming(String field){
        ServiceResult<Homecoming> result = homecomingService.queryAll();
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }


    /**  <pre>
     *  修改同学会信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @GetMapping("/homecoming/updatehomecoming")
    @ResponseBody
    public ApiResponse updatehomecoming(String field,String onoffswitch,String content){
        int display = 0;
        if("on".equals(onoffswitch)){
            display = 1;
        }else{
            display = 0;
        }
        ServiceResult<Homecoming> result = homecomingService.update(field,display,content);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
}
