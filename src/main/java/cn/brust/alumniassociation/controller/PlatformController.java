/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: PlatformController
 * Author:   Administrator
 * Date:     2018/7/25 20:47
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Platform;
import cn.brust.alumniassociation.service.PlatformService;
import cn.brust.alumniassociation.service.PlatformService;
import cn.brust.alumniassociation.utils.FileUtils;
import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 〈一句话功能简述〉<br> 
 * 〈〉
 *
 * @author Administrator
 * @create 2018/7/25
 * @since 1.0.0
 */
@Controller
public class PlatformController {

    @Autowired
    PlatformService platformService;

    @RequestMapping("/platform/queryPlatformInfo")
    @ResponseBody
    public ApiResponse queryPlatformInfo(){
        ServiceResult<Platform> result = platformService.queryPlatformInfo();
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    @PostMapping("/platform/updatePlatformInfo")
    @ResponseBody
    public ApiResponse updatePlatformInfo(String names,String title,String filename,String tel,String email,String qq,String agreement){
        Platform platform = new Platform();
        platform.setName(names);
        platform.setTitle(title);
        platform.setLogo(filename);
        platform.setTel(tel);
        platform.setEmail(email);
        platform.setQq(qq);
        platform.setAgreement(agreement);

        platform.setId("PF"+ UUID.randomUUID());
        ServiceResult<Platform> result = platformService.update(platform);
        if (result.isSuccess()){
            return ApiResponse.ofSuccess(result.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }
    @RequestMapping(value = "/platform/upload" , method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> upload(HttpServletRequest servletRequest,@RequestParam("file") MultipartFile file) throws IOException {
        String platform = FileUtils.saveFile(file, servletRequest, "platform");
        Map<String, Object> res = new HashMap<>();
        System.out.println(platform);
        res.put("url", platform);
        return res;
    }
}