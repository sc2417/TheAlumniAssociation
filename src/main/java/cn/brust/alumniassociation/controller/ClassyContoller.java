/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: ClassyContoller
 * Author:   Administrator
 * Date:     2018/7/28 17:08
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.entity.Classy;
import cn.brust.alumniassociation.entity.User;
import cn.brust.alumniassociation.service.ClassyService;
import cn.brust.alumniassociation.service.UserService;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.ibatis.annotations.Param;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * <pre>
 *     @author 马敏婷、郑锦波
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Controller
public class ClassyContoller {

    @Autowired
    ClassyService classyService;
    @Autowired
    UserService userService;


    /**  <pre>
     *  查询所有班级信息
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @return : null
     * </pre>
     */
    @GetMapping("/classy/queryAllClassy")
    public @ResponseBody ApiResponse queryAllClassy(HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        ServiceMultiResult serviceMultiResult = classyService.queryAllClassy();
        return ApiResponse.ofSuccess(serviceMultiResult.getResult());
    }
    @GetMapping("/classy/selectAll")
    public @ResponseBody ApiResponse selectAll(Integer page, Integer limit){
        if(page == null || limit == null){
            return ApiResponse.ofStatus(ApiResponse.Status.NOT_VALID_PARAM);
        }
        ServiceMultiResult result = result = classyService.selectAllLimit(page,limit);
        if(result.getPage() == null){
            return ApiResponse.ofMessage(102,"没有更多了");
        }
        return ApiResponse.ofPage(result.getPage());
    }

    @RequestMapping("/classy/add")
    public String add(String cname,String uname,String mobile,String email,String passwd,String describe,String state){
        String wxId = "";
        Integer isEnable;
        for(int i=0;i<8;i++) {
            wxId += new Random().nextInt(10);
        }
        wxId = "UID"+wxId;
        if("on".equals(state)){
            isEnable = 0;
        }else {
            isEnable = 1;
        }
        Date date = new Date();
        Classy classy = new Classy();
        classy.setName(cname);
        classy.setUserId(wxId);
        classy.setIsEnable(isEnable);
        classy.setCreateDate(date);
        classyService.addClassy(classy);

        System.out.print("ID="+classy.getId());
        userService.addClassRole(wxId,uname,mobile,email,classy.getId(),passwd,describe);
        return "/admin/classy/index";
    }

    @RequestMapping("/classy/update")
    public String update(String cname,String userId,String state,Integer id,String uname,String mobile,String email,String passwd,String describe){
        Integer isEnable;
        if("on".equals(state)){
            isEnable = 0;
        }else {
            isEnable = 1;
        }
        classyService.updateClassy(cname,userId,isEnable,id);
        userService.updateClassRole(uname,mobile,email,passwd,describe,userId);
        return "/admin/classy/index";
    }

    @GetMapping("/classy/del")
    public String del(Integer id){
        classyService.delClassy(id);
        return "/admin/classy/index";
    }

    @RequestMapping("/classy/queryById")
    public String queryById(Integer id, Model model){
        Classy classy = classyService.queryById(id);
        ServiceMultiResult<User> userlist = userService.queryByClassId(id);
        model.addAttribute("classylist",classy);
        model.addAttribute("userlist",userlist.getResult());
        return "admin/classy/modify";
    }

    @RequestMapping("/classy/findByWxId")
    public @ResponseBody User queryByWxId(String wxId){
        return userService.findByWxId(wxId);
    }
}