package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.service.MemberService;
import cn.brust.alumniassociation.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author
 * @since
 * @since
 */
@Controller
public class MemberCotroller {
    @Autowired
    MemberService memberService;

    /**
     * 查询用户
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email         邮箱
     * @param pwd     密码
     * @return
     */
    @PostMapping("/menber/findmenberInfo")
    @ResponseBody
    public ApiResponse findPasswd(String email,String pwd){
        System.out.println(email);
        System.out.println(pwd);
        ServiceResult member = memberService.findMember(email, pwd);
        System.out.println(member);
        return ApiResponse.ofSuccess(member.getResult());
    }
    /**
     * 查询用户
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email         邮箱
     * @return
     */
    @PostMapping("/menber/findmenberEmailInfo")
    @ResponseBody
    public ApiResponse findPasswd(String email){
        System.out.println(email);
        ServiceResult member = memberService.findMemberemail(email);
        System.out.println(member);
        return ApiResponse.ofSuccess(member.getResult());
    }



    /**
     * 修改密码
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email         邮箱
     * @param oldPwd     旧密码
     * @param newPwd     新密码
     * @return
     */
    @PostMapping("/member/updatePasswd")
    @ResponseBody
    public ApiResponse updatePasswd(String email,String oldPasswd,String newPasswd){
        System.out.println(email);
        System.out.println(oldPasswd);
        System.out.println(newPasswd);
        ServiceResult member = memberService.updatePasswd(email, oldPasswd,newPasswd);

        if (member.isSuccess()){
            return ApiResponse.ofSuccess(member.getResult());
        }
        return ApiResponse.ofStatus(ApiResponse.Status.NOT_FOUND);
    }

    /**
     * 修改头像
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param servletRequest
     * @param file
     * @param email
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/admin/member/upload/img" , method = RequestMethod.POST)
    public @ResponseBody
    Map<String, Object> upload(HttpServletRequest servletRequest, @RequestParam("file") MultipartFile file, String  email) throws IOException {
        //如果文件内容不为空，则写入上传路径
        if (!file.isEmpty()) {
            //上传文件路径
            String filepath = FileUtils.saveFile(file, servletRequest, "avatar");
            Map<String, Object> res = new HashMap<>();
            //返回的是一个url对象
            res.put("url", filepath);
            String ress=res.toString();
            System.out.println();
            memberService.updateAuatar(email, filepath);
            return res;
        } else {
            return null;
        }
    }
}

