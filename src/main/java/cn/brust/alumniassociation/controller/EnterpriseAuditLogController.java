package cn.brust.alumniassociation.controller;

import cn.brust.alumniassociation.base.ApiResponse;
import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.entity.EnterpriseAuditLog;
import cn.brust.alumniassociation.entity.User;
import cn.brust.alumniassociation.service.EnterpriseAuditLogService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Administrator 2018/8/13 14:41
 */
@Controller
public class EnterpriseAuditLogController {

    EnterpriseAuditLogService enterpriseAuditLogService;

    @PostMapping("/enter/queryAnditLog")
    @ResponseBody
    public ApiResponse queryAnditLog(String  eid){
        System.out.println(eid);
        System.out.println("++++++++++++");
        ServiceMultiResult serviceMultiResult = enterpriseAuditLogService.selectByEnterpriseId(eid);
        System.out.println(serviceMultiResult);
        List<EnterpriseAuditLog> row = serviceMultiResult.getResult();
        if(row.size() == 0){
            return ApiResponse.ofMessage(1024,"空空");
        }
        return ApiResponse.ofSuccess(row);
    }

}