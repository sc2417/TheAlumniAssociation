package cn.brust.alumniassociation.config;

import com.github.pagehelper.PageHelper;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;
/**
 * <pre>
 *     @author 郭逸峰
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Configuration
@AutoConfigureAfter()
public class MybatisConf {  
        @Bean
        public PageHelper pageHelper() {
           System.out.println("MyBatisConfiguration.pageHelper()");
            PageHelper pageHelper = new PageHelper();  
            Properties p = new Properties();
            p.setProperty("offsetAsPageNum", "true");
            p.setProperty("rowBoundsWithCount", "true");
            p.setProperty("reasonable", "true");
            pageHelper.setProperties(p);  
            return pageHelper;  
        }

}  