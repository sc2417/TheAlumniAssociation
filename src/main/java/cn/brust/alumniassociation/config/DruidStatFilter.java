package cn.brust.alumniassociation.config;

import com.alibaba.druid.support.http.WebStatFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
/**
 * <pre>
 *     @author 郭逸峰
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@WebFilter(filterName="druidWebStatFilter",urlPatterns="/*",
    initParams={
        @WebInitParam(name="exclusions",value="*.js,*.gif,*.jpg,*.bmp,*.png,*.personalCenter,*.ico,/druid/*")// 忽略资源
})
public class DruidStatFilter extends WebStatFilter {

}