package cn.brust.alumniassociation.wx.po;
import java.io.Serializable;
/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public class GetAccessTokenRsp implements Serializable {
 
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7021131613095678023L;
 
	private String accessToken;
 
	public String getAccessToken() {
		return accessToken;
	}
 
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
 
	@Override
	public String toString() {
		return "GetAccessTokenRsp [accessToken=" + accessToken + "]";
	}
 
}