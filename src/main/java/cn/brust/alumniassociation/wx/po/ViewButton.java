package cn.brust.alumniassociation.wx.po;

/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public class ViewButton {
 private String type;
 private String name;
 private String url;
  
 public String getType() {
 return type;
 }
  
 public void setType(String type) {
 this.type = type;
 }
  
 public String getName() {
 return name;
 }
  
 public void setName(String name) {
 this.name = name;
 }
  
 public String getUrl() {
 return url;
 }
  
 public void setUrl(String url) {
 this.url = url;
 }
  
}