package cn.brust.alumniassociation.wx.wxmenu;

import com.alibaba.fastjson.JSONObject;
import net.sf.json.JSONArray;
/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public class MenuMain {

	public static void main(String[] args) {

		String APP_ID = "wx94b9f9848e25f2e5";
		String SECRET = "1c94653d0a6fa9df77b549017045acf4";
		/*右边菜单定义*/
		ClickButton cbt=new ClickButton();
		cbt.setKey("image");
		cbt.setName("营销同学会");
		cbt.setType("click");

		/*自定义菜单*/
		/*左边菜单定义*/
		ViewButton vbt=new ViewButton();
		vbt.setUrl("http://www.chenjunqiu.cn");
		vbt.setName("营销同学会");
		vbt.setType("view");

		JSONArray sub_button=new JSONArray();
		sub_button.add(cbt);
		sub_button.add(vbt);

		JSONObject buttonOne=new JSONObject();
		buttonOne.put("name", "营销同学会");
		buttonOne.put("sub_button", sub_button);

		JSONArray button=new JSONArray();
		button.add(vbt);
		button.add(buttonOne);
		button.add(cbt);

		JSONObject menujson=new JSONObject();
		menujson.put("button", button);
		System.out.println(menujson);

	 /*GetAccessTokenRsp token = Token.getWeiXinAccessToken(APP_ID, SECRET);
     System.out.println(token.getAccessToken());*/
		//这里为请求接口的url +号后面的是token，这里就不做过多对token获取的方法解释
		String url="https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+"12_jdLKsHQzglidxisBBpvN2Mnlp7aDVRlWSwiNU0LlFgdzCIOazie2sHClxqeT4lYwJlnmA2Zl2b3OPPkaBneL7D7eSjbQr6JHEaGAs0c2ug-krev4QRWhTcEfk-DDgMXdfYAKqDumAi2KUDkLOIAcABAFZR";

		try{
			String rs=HttpUtils.sendPostBuffer(url, menujson.toJSONString());
			System.out.println(rs);
		}catch(Exception e){
			System.out.println("请求错误！");
		}
	}
}