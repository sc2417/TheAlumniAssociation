package cn.brust.alumniassociation.wx.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.brust.alumniassociation.wx.po.TextMessage;
import org.dom4j.DocumentException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@RestController
@RequestMapping("weiXinServlet")
public class WeiXinServlet {

	@RequestMapping("/weixinTest1")
	public void weixinTest(HttpServletRequest request, HttpServletResponse response,String msg) throws ServletException, IOException {
		boolean isGet = request.getMethod().toLowerCase().equals("get");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
        if (isGet) {
            // 微信加密签名
            String signature = request.getParameter("signature");
            // 时间戳
            String timestamp = request.getParameter("timestamp");
            // 随机数
            String nonce = request.getParameter("nonce");
            // 随机字符串
            String echostr = request.getParameter("echostr");
            // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
            if (signature != null && CheckoutUtil.checkSignature(signature, timestamp, nonce)) {
                out.write(echostr);
                out.flush();
            }
        }
        try {
            Map<String, String> map = MessageUtil.xmlToMap(request);
            String fromUserName = map.get("FromUserName");
            String toUserName = map.get("ToUserName");
            String msgType = map.get("MsgType");
            String content = map.get("Content"); //用户在微信回复的消息就是这个,打印可看见内容
            String message = null;
            String eventType = map.get("Event"); // 事件分成多种，分别判断处理
            if (MessageUtil.MESSAGE_SUBSCRIBE.equals(eventType)) { // 这里先写一个关注之后的事件
                message = MessageUtil.initText(toUserName, fromUserName, MessageUtil.menuText());
            }else{
                if("text".equals(msgType)){
                    TextMessage text = new TextMessage();
                    text.setFromUserName(toUserName); //原来的信息发送者，将变成信息接受者
                    text.setToUserName(fromUserName); //原理的接受者，变成发送者
                    text.setMsgType("text"); //表示消息的类型是text类型
                    text.setCreateTime(new Date().getTime());
                    text.setContent("您发送的信息是：" + msg);
                    message = MessageUtil.textMessageToXml(text); //装换成 xml 格式发送给微信解析

                    System.out.println(message);
                }
                out.print(message);
            }
            System.out.println("123"+message);
            out.print(message);
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
}