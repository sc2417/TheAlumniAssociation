package cn.brust.alumniassociation.wx.menus;
  
import cn.brust.alumniassociation.wx.po.ClickButton;
import cn.brust.alumniassociation.wx.po.ViewButton;
import com.alibaba.fastjson.JSONObject;
import net.sf.json.JSONArray;
/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public class MenuMain {

    public static void main(String[] args) {

        String APP_ID = "wx5bce48f209c76022";
        String SECRET = "68b0416d41c31f9e008c17ea62875206";
        /*右边菜单定义*/
        ClickButton cbt=new ClickButton();
        cbt.setKey("\ue112");
        cbt.setName("关于同学会");
        cbt.setType("click");

        /*自定义菜单*/
        /*左边菜单定义*/
        ViewButton vbt=new ViewButton();
        vbt.setUrl("http://www.chenjunqiu.cn");
        vbt.setName("营销同学会");
        vbt.setType("view");

        JSONArray sub_button=new JSONArray();
        sub_button.add(cbt);
        sub_button.add(vbt);

        JSONObject buttonOne=new JSONObject();
        buttonOne.put("name", "企业展示");
        buttonOne.put("sub_button", sub_button);

        JSONArray button=new JSONArray();
        button.add(vbt);
        button.add(buttonOne);
        button.add(cbt);

        JSONObject menujson=new JSONObject();
        menujson.put("button", button);
        System.out.println(menujson);

        /*动态获取acces_token 把链接的access_token值换成token.getAccessToken() */
	 /*GetAccessTokenRsp token = Token.getWeiXinAccessToken(APP_ID, SECRET);
     System.out.println(token.getAccessToken());*/
        //这里为请求接口的url +号后面的是token，这里就不做过多对token获取的方法解释
        String url="https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+"12_oT4k6DkVAvUbaD4rYLQoqDfWR9UXozPFTxGdPxfnnGtofoOFfrV1Eizx_phTmURprJgxp2R_85xZMo03FyzNidvISwJrMjx6Doev1cYo6eFcCTe-GtNi4CefQfQi8Uwpw7RiZeJcXxzIhk1QGUHgAAALZM";
        try{
            String rs=HttpUtils.sendPostBuffer(url, menujson.toJSONString());
            System.out.println(rs);
        }catch(Exception e){
            System.out.println("请求错误！");
        }
    }
}