package cn.brust.alumniassociation.base;

import com.github.pagehelper.Page;

import java.util.List;


/**
 * <pre>
 *     @author 郭逸峰
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public class ServiceMultiResult<T> {
    private long total;
    private List<T> result;
    private Page<T> page;

    public  ServiceMultiResult(long total, List<T> result) {
        this.total = total;
        this.result = result;
    }

    public ServiceMultiResult() {
    }

    public ServiceMultiResult(Page<T> page) {
        this.page = page;
    }

    public Page<T> getPage() {
        return page;
    }

    public void setPage(Page<T> page) {
        this.page = page;
    }


    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }

    public int getResultSize() {
        if (this.result == null) {
            return 0;
        }
        return this.result.size();
    }
    public int getPageSize() {
        if (this.page == null) {
            return 0;
        }
        return this.page.size();
    }
}
