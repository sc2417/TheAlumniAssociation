package cn.brust.alumniassociation.base;

import com.github.pagehelper.Page;


/**
 * <pre>
 *     @author 郭逸峰
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public class ApiResponse {
    /**
     * 状态码
     */
    private int code;
    /**
     * 请求返回参数
     */
    private String msg;
    /**
     * 请求返回数据
     */
    private Object data;

    private long count;

    public ApiResponse(int code, String msg, Object data, long count) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count;
    }

    public ApiResponse(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ApiResponse() {
        this.code = Status.SUCCESS.getCode();
        this.msg = Status.SUCCESS.getStandardMessage();
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


    public static ApiResponse ofMessage(int code, String message) {
        return new ApiResponse(code, message, null);
    }

    public static ApiResponse ofSuccess(Object data) {
        return new ApiResponse(Status.SUCCESS.getCode(), Status.SUCCESS.getStandardMessage(), data);
    }
    public static ApiResponse ofPage(Page page) {
        return new ApiResponse(Status.SUCCESS.getCode(), Status.SUCCESS.getStandardMessage(), page, page.getTotal());
    }
    public static ApiResponse ofStatus(Status status) {
        return new ApiResponse(status.getCode(), status.getStandardMessage(), null);
    }

    /**
     * 枚举类
     */
    public enum Status {
        SUCCESS(200, "OK"),//一个成功的请求
        BAD_REQUEST(400, "Bad Request"),//错误请求
        NOT_FOUND(404, "Not Found"),//NOT_FOUND
        INTERNAL_SERVER_ERROR(500, "Unknown Internal Error"),//发生错误
        NOT_VALID_PARAM(40005, "Not valid Params"),//参数不正确(无效的参数)
        NOT_SUPPORTED_OPERATION(40006, "Operation not supported"),//不支持的操作  比如(没有权限或者其他)
        NOT_LOGIN(50000, "Not Login");//没有登录

        private int code;
        private String standardMessage;

        Status(int code, String standardMessage) {
            this.code = code;
            this.standardMessage = standardMessage;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getStandardMessage() {
            return standardMessage;
        }

        public void setStandardMessage(String standardMessage) {
            this.standardMessage = standardMessage;
        }
    }
}
