package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;

import cn.brust.alumniassociation.entity.Classy;
import org.apache.ibatis.annotations.Param;

import javax.xml.ws.Service;
import java.util.Date;
import java.util.List;
/**
 * <pre>
 *     @author 马敏婷、郑锦波
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public interface ClassyService {
    /**
     * 分页展示班级列表
     * @param limit
     * @param page
     * @return
     */
    ServiceMultiResult queryClassy(Integer limit, Integer page);

    /**
     * 查询所有班级信息
     * @return
     */
    ServiceMultiResult queryAllClassy();

    /**
     * 查询所有班级信息
     * @return
     */
    List<Classy> selectAll();

    /**
     * 查询所有班级信息(分组)
     * @param page 页数
     * @param limit 条数
     * @return
     */
    ServiceMultiResult selectAllLimit(Integer page, Integer limit);

    /**
     * 添加班级信息
     * @return
     */
    ServiceResult addClassy(Classy classy);

    /**
     * 根据 班级id查询班级信息
     * @param id 编号
     * @return
     */
    Classy selectById(Integer id);

    /**
     * 根据班级id 修改班级信息(班级名称、班级状态)
     * @param name 名称
     * @param userId 管理员id
     * @param isEnable 状态
     * @param id    编号
     * @return
     */
    ServiceResult updateClassy(String name, String userId, Integer isEnable, Integer id);

    /**
     * 根据id删除班级信息(软删)
     * @param id 编号
     * @return
     */
    ServiceResult delClassy(Integer id);

    /**
     *  根据id查询班级信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param id 编号
     * @return
     */
    Classy queryById(Integer id);
}
