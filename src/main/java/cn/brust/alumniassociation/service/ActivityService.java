package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Activity;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     @author 黄华德、孙超
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public interface ActivityService {

    /**
     * 发布动态
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @param userId :
     * @param activityId :
     * @param name :
     * @param level :
     * @param content :
     * @return : cn.brust.alumniassociation.base.ServiceResult
     */
    ServiceResult addDynamics(String id,String userId,String activityId,String name,String level, String content);

    /**
     *  动态评论（wx）
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     */
    ServiceMultiResult dynamicQuery(@Param("id")Integer id);

    /**
     *  根据活动id查询活动详情联合班级
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     */
    ServiceMultiResult lookupActivityDetails (Integer id);

    /**
     * 点赞修改
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param zan :
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceResult
     */
    ServiceResult updateFabulous(@Param("zan") int zan,@Param("id")String id);

    /**
     *  查看评论
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceResult
     */
    ServiceResult queryComment(int id);

    /**
     * 查看活动（评论）
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     */
    ServiceMultiResult detailsFindById(int id);

    /**
     * 编辑活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param title :
     * @param cover :
     * @param address :
     * @param type :
     * @param createDate :
     * @param endDate :
     * @param content :
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceResult
     */
    ServiceResult modifyActivity(String title, String cover, String address, String type, String createDate, String endDate, String content, String id);

    /**
     *  ID 查询
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceResult
     *
     */
    ServiceResult selectById(String id);

    /**
     *  修改报名状态
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceResult
     */
    ServiceResult modifyEnroll(String id);

    /**
     *  删除活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param id :
     * @return : cn.brust.alumniassociation.base.ServiceResult
     *
     */
    ServiceResult delActivity(String id);

    /**
     *  添加活动
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param title :
     * @param cover :
     * @param type :
     * @param createDate :
     * @param endDate :
     * @param address :
     * @return : cn.brust.alumniassociation.base.ServiceResult
     */
    ServiceResult addActivity(String title,String cover,String address,String type,String createDate,String endDate,String content);

    /**
     *  分页查询活动信息
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param page :
     * @param limit :
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     *
     */
    ServiceMultiResult queryVague(Integer page, Integer limit);

    /**
     *  筛选查询活动信息Time
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @param classId :
     * @param title :
     * @param time :
     * @param page :
     * @param limit :
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     *
     */
    ServiceMultiResult queryVagueTime(Integer classId,String title,String time,Integer page,Integer limit,Integer Identification);

    /**
     * 活动列表分页
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     */
    ServiceMultiResult pageQuery(Integer page, Integer limit);

    /**
     * 活动列表展示
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     */
    ServiceMultiResult findAll();


    /**  <pre>
     * 根据班级查询进行中活动
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param page :页码
     * @param limit :每页显示条数
     * @param cid  :班级id
     * @return :
     * </pre>
     */
    ServiceMultiResult queryConductActivity(Integer page, Integer limit, Integer cid);


    /**  <pre>
     * 根据班级查询已结束活动
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param page :页码
     * @param limit :每页显示条数
     * @param cid  :班级id
     * @return :
     * </pre>
     */
    ServiceMultiResult queryEndActivity(Integer page, Integer limit, Integer cid);

    /**  <pre>
     * 根据活动id查询活动详情
     * @author 黄华德
     * @since 1.0
     * @since JDK 8.0
     * @param id :活动id
     * @return : null
     * </pre>
     */
    ServiceResult<Activity> queryActivityDetails (Integer id);


    /**
     *
     * @param page 页码
     * @param limit 页显示数
     * @param status 状态标示
     * @param cid 班级ID   此时用户登录状态 controller从session查user拿出cid
     * @return
     */
    ServiceMultiResult queryActivityByStates(Integer page, Integer limit, Integer status, Integer cid);

    /**
     * 根据用户ID查询活动详情
     * @param userId 用户ID
     * @return
     */
    ServiceResult queryActivityById(Integer userId);

    /**
     * 条件查询活动
     * @param cid 班级DI
     * @param title 活动标题
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */

    ServiceMultiResult queryByKeyword(Integer cid, String title, String startTime, String endTime);


    /**
     * 添加活动
     *  必须字段  活动标题、活动封面、活动地点、开始时间、报名截止时间、班级id
     *  非必须字段 活动性质、显示设置、正文是富文本格式
     *  参考后台管理添加页面
     * @param activity
     * @return
     */
    ServiceResult add(Activity activity);

    /**
     * 修改活动
     *  同上  mapper需要判断null值
     *  参考后台管理添加页面
     * @param activity
     * @return
     */
    ServiceResult update(Activity activity);

    /**
     * 软删
     * 根据活动ID删除
     * @param id 活动ID
     * @return
     */
    ServiceResult deleteActivityByid(Integer id);

    /**
     * 根据活动ID修改活动状态
     * @param id 活动id
     * @param status 活动状态
     * @return
     */

    ServiceResult updateStates(Integer id, Integer status);
    /**  <pre>
     *
     * @author 微信首页活动展示1条
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    ServiceResult getActivityOne(String cId);

    /**  <pre>
     *  查询用户进行中活动
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */
    ServiceMultiResult getMyInActivity(Integer page, Integer limit,String uId);

    /**  <pre>
     *  查询用户活动回顾
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */
    ServiceMultiResult getMyEndActivity(Integer page, Integer limit,String uId);
}
