package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Platform;

/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public interface PlatformService {

    ServiceResult queryPlatformInfo();

    ServiceResult update(Platform platform);
}