package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Enterprise;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * <pre>
 *     @author 马敏婷
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public interface EnterpriseService {

    /**
     *  企业展示（weixin）
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @return : java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     */
    ServiceMultiResult queryEnterprise();

    /**
     * 分页展示企业（weixin）
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     */
    ServiceMultiResult findPageEnterprise(Integer page, Integer limit);

    /**
     * 查看企业详情
     * @author 马敏婷
     * @since   1.0
     * @since   JDK 8.0
     * @param id :企业ID
     * @return 封装数据后的结果对象
     */
    ServiceResult queryEnterpriseInfoById(String id);

    /**
     * 企业认证/添加企业
     * 必须字段 企业名称、企业网站、企业logo、企业电话1、
     * 企业电话2、主营业务、企业简介、企业所属班级、企业推介人、
     * 职位、手机号1、手机号2、是否显示、是否置顶
     * @author  马敏婷
     * @since   1.0
     * @since   JDK 8.0
     * @param enterprise 企业对象
     * @return : 封装数据后的结果对象
     */
    ServiceResult add(Enterprise enterprise, MultipartFile file, HttpServletRequest request) throws IOException;

    /**
     * 编辑企业
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param enterprise : 企业对象
     * @return : 封装数据后的结果对象
     */
    ServiceResult update(Enterprise enterprise, MultipartFile file, HttpServletRequest request) throws IOException;


    /**  <pre>
     * 分页查询符合条件的企业信息
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param cid : 班级ID
     * @param name : 企业名称
     * @param uid : 企业推介人
     * @param releaseTime : 发布时间
     * @param page : 显示第几页的数据
     * @param limit : 一次显示多少条数据
     * @return : 封装数据后的结果对象
     * </pre>
     */
    ServiceMultiResult query(String cid, String name, String uid, String releaseTime, Integer page, Integer limit);

    /**  <pre>
     * 分页查询符合条件的企业审核信息
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param cid : 班级ID
     * @param name : 企业名称
     * @param uid : 企业推介人
     * @param releaseTime : 发布时间
     * @param page : 显示第几页的数据
     * @param limit : 一次显示多少条数据
     * @return : 封装数据后的结果对象
     * </pre>
     */
    ServiceMultiResult queryEnterprisesByAudit(String cid, String name, String uid, String releaseTime, Integer page, Integer limit);
    /**
     * 企业审核
     * @author  马敏婷
     * @since   1.0
     * @since   JDK 8.0
     * @param id 企业ID
     * @param examine 企业审核：审核通过、审核不通过
     * @param examineDetails 审核详情
     * @return 封装数据后的结果对象
     */
    ServiceResult updateExamine(String id, String examine, String examineDetails);

    /**
     * 删除企业
     * @author  马敏婷
     * @since   1.0
     * @since   JDK 8.0
     * @param id : 企业ID
     * @return 封装数据后的结果对象
     */
    ServiceResult delete(String id);

    /**
     * 审核企业
     * @author  王永泽
     * @since   1.0
     * @since   JDK 8.0
     * @param id : 企业ID
     * @param auditStatus : 审核状态
     * @param auditlog : 审核信息
     * @return 封装数据后的结果对象
     */

    ServiceResult updateAuditStatus(String eid, Integer auditStatus, String auditlog, HttpSession session);


    /**
     * <pre>
     *  修改状态：上架、下架
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @param status : 企业状态
     * @return : 返回受影响的行数
     * </pre>
     */

    ServiceResult updateStatus(String id, String status);

    /**  <pre>
     *  根据ID查询  微信端显示
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param id : 企业ID
     * @return : 返回查询的数据
     * </pre>
     */
    ServiceResult queryEnterprisesId(String id);

    /**  <pre>
     *  微信端 根据用户ID查询
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param userId : 用户ID
     * @return : 返回查询的数据
     * </pre>
     */
    ServiceResult queryInfo(String userId);

    /**  <pre>
     *  微信端 修改用户修改的企业信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param enterprise 要修改的企业对象
     * @return :
     * </pre>
     */
    ServiceResult updateInfo(Enterprise enterprise);


    /**  <pre>
     *  微信端 用户添加企业信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param enterprise 要添加的企业对象
     * @return :
     * </pre>
     */
    ServiceResult insertInfo(Enterprise enterprise);


    ServiceResult selectById(String id);
}
