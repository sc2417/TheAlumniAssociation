package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceResult;

/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public interface HomecomingService {

    /**
     * 根据field去修改
     * 比如 field是details 学院介绍 那么 则 display和content是修改details对应字段的
     *            classmate 同学会介绍
     *            statutes  同学会章节
     * @param field details学院介绍、classmate同学会介绍、statutes同学会章节
     * @param display 是否显示
     * @param content 文本内容(富文本格式)
     * @return
     */

    ServiceResult update(String field,Integer display,String content);

    /**
     * 查询全部
     * @return
     */
    ServiceResult queryAll();
}
