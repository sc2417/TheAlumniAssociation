package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Notice;

import java.util.List;

/**
 * <pre>
 *     @author 陈佳婷
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public interface NoticeService {


    /**
     * 微信首页轮播广告
     * @return
     */
    List<Notice> findAll();

    /**
     * 根据筛选的条件查询系统公告
     * 前面三个字段为空就是普通分页查询
     * @param title 消息标题
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param page
     * @param limit
     * @return
     */

    ServiceMultiResult queryNoticeByKeyWord(String title, String startTime, String endTime, Integer page, Integer limit);

    /**
     * 根据公告ID查询
     * @param id 公告ID
     * @return
     */
    ServiceResult queryNoticeById(Integer id);

    /**
     * 发布公告
     * @param notice 公告
     * @return
     */

    ServiceResult add(Notice notice);

    /**
     * 根据公告ID删除公告
     * @param id 公告ID
     * @return
     */
    ServiceResult deleteById(Integer id);

    /**
     * 根据筛选的条件查询系统公告
     * 前面三个字段为空就是普通分页查询
     * @param title 消息标题
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param page
     * @param limit
     * @return
     */

    ServiceMultiResult getNotices(Integer page, Integer limit, String title, String startTime, String endTime);


    ServiceResult updateDel(String id);
    /**
     * 查询公告详情
     * @param id 公告id
     * @return
     */
    ServiceResult queryNoticeInfoById(String id);
}
