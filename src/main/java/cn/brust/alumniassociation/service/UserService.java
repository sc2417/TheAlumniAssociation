package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.User;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Map;


public interface UserService {


    /**  <pre>
     *  根据手机号查询用户
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param mobile :手机号
     * @return :  ServiceResult
     * </pre>
     */
    ServiceResult selectByMobile(String mobile);
    /**
     * 申请加入
     *
     * @param user
     * @return
     */
    ServiceResult applyToJoin(User user);

    /**
     * 登录
     *
     * @param phone
     * @param passwd
     * @param classId
     * @return
     */
    ServiceResult login(String phone, String passwd, Integer classId);
    /**  <pre>
     *  查询用户是否存在
     *    @author 黄华德
     *    @since 1.0
     *    @since JDK 8.0
     * </pre>
     */
    ServiceResult queryByWxId(String openId);
    /**
     * <pre>
     *  查看个人信息/查看名片
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId : 用户ID
     * @return : 个人信息
     * </pre>
     */
    ServiceResult myInfo(String wxId);

    /**
     * <pre>
     * 账号设置-修改密码
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email          邮箱
     * @param oldPasswd     旧密码
     * @param newPasswd     新密码
     * @return :
     * </pre>
     */

    ServiceResult updatePasswd(String email, String oldPasswd, String newPasswd);

    /**
     * 个人信息查询
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email          邮箱
     * @param oldPasswd     旧密码
     * @return
     */
    ServiceResult selectUsers(String email, String oldPasswd);

    /**
     * 修改图片路径
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param email
     * @param avatar
     * @return
     */
    ServiceResult updateAvatar(String email, String avatar);


    /**
     * 会员管理-会员列表信息-筛选查询
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param name        用户名
     * @param mobile      手机号
     * @param classId     班级Id
     * @param createTime  注册时间
     * @param suditStatus 审核状态
     * @param suditStatu  审核状态
     * @param createTime
     * @return
     */
    ServiceMultiResult filtrateQuery(String name, String mobile, Integer classId, String createTime,String suditStatus,String  suditStatu, Integer page, Integer limit);

    /**
     * 分类查询会员信息
     *
     * @param page
     * @param limit
     * @return
     */

    ServiceMultiResult queryUsers(Integer page, Integer limit);

    /**
     * 会员列表-查看会员信息-同学会动态
     *
     * @param wxId
     * @return
     */
    ServiceResult queryHomecomingDynamic(String wxId);

    /**
     * 修改会员审核状态
     *
     * @param wxId
     * @param examine
     * @param examineDetails
     * @return
     */
    ServiceResult updateStates(String wxId, String examine, String examineDetails);

    /**
     * 修改成员信息
     *
     * @param wxId
     * @param nickname
     * @param name
     * @param email
     * @param passwd
     * @param rPasswd
     * @param desc
     * @return
     */
    ServiceResult update(String wxId, String nickname, String name, String email, String passwd, String rPasswd, String desc);

    /**
     * 用户-权限设置
     *
     * @param wxId
     * @param rangesManager
     * @param rangesOperation
     * @return
     */
    ServiceResult updateRanges(String wxId, String rangesManager, String rangesOperation);


    /**
     * 查询所有会员列表
     */
    ServiceMultiResult selectAll(Integer page, Integer limit);


    /**
     * 查询班级所有会员列表
     */
    ServiceMultiResult classAllUser(Integer classId, Integer page, Integer limit);

    /**
     * <pre>
     * 根据ID查询 该班级下的会员
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult<cn.brust.alumniassociation.entity.User>
     * </pre>
     */
    ServiceMultiResult<User> selectAllByClassId(Integer classId);

    /**
     * <pre>
     *  添加企业 - 企业所属班级模块
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @return : 所属班级的所有学员
     * </pre>
     */
    ServiceMultiResult classyAllUser(Integer classId);


    /**
     * <pre>
     *  会员管理 - 会员审核
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId : 会员ID
     * @param status    审核状态
     * @return :
     * </pre>
     */
    ServiceResult updateAuditStatus(String wxId, String status);


    /**
     * 会员管理-会员审核
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId        用户Id
     * @param enable      用户冻结状态
     * @return
     */
    ServiceResult updateUserEnable(String wxId, Integer enable);

    /**
     * 会员管理-删除会员信息
     *
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param wxId        用户Id
     * @return
     */
    ServiceResult delUser(String wxId);

    /**  <pre>
     * 增加班级管理员
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param wxId
     * @param name
     * @param email
     * @param classId
     * @param passwd
     * @param describe
     * </pre>
     */
    ServiceResult addClassRole(String wxId, String name, String mobile, String email, Integer classId, String passwd, String describe);

    /**  <pre>
     * 根据班级id查找用户
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param classId
     * </pre>
     */
    ServiceMultiResult<User> queryByClassId(Integer classId);

    /**  <pre>
     * 根据wxid查找用户
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param wxId
     * </pre>
     */
     User findByWxId(String wxId);
    /**
     * 个人中心- 个人信息修改
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId      会员id
     * @param nickname  昵称
     * @param  mobile   手机号码
     * @param  email     邮箱
     * @param  classrole  班级角色名称
     * @return
     */
    ServiceResult updateMyIfon(String wxId,String nickname,String  mobile,String email,String classrole);

    /**
     * <pre>
     *  根据邮箱或手机号码登录
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param accounts 邮箱或手机号码
     * @param passwd 密码
     * @return
     * </pre>
     */
    ServiceResult pwdCorrect(String accounts, String passwd);

    /**  <pre>
     * 根据wxid修改班级管理员信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param name
     * @param mobile
     * @param email
     * @param passwd
     * @param describe
     * @param wxId
     * </pre>
     */
    ServiceResult updateClassRole(String name, String mobile, String email, String passwd, String describe, String wxId);

    ServiceResult addWXUser(String openId);

    /**
     * 查询此邮箱是否有信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param email          邮箱
     * @return
     */
    ServiceResult selectEmail(String email);

    /**
     * 给邮箱发送验证码
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return
     */
    boolean sendEmail(String email, HttpSession session);
    /**
     * 用邮箱修改密码
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @return
     */
    ServiceResult emailUpdatePasswd(String email, String password);

}




