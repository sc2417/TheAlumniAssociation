package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.EnterpriseAuditLog;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * @author 马敏婷
 * @since 1.0
 * @since jdk8
 */
public interface EnterpriseAuditLogService {

    ServiceResult add(EnterpriseAuditLog enterpriseAuditLog);

    ServiceMultiResult selectByEnterpriseId(String enterpriseId);

    ServiceResult selectById(String id);

    ServiceResult addAuditLog(String  id,String enterpriseId,String msg,Integer auditStatus,String  auditDate,  String createPerson);
}
