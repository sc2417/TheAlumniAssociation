package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Information;

/**
 * <pre>
 *     @author 陈润颖
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public interface InformationService {
    /**
     * 分页查询资讯信息
     * @param page 页数
     * @param limit 页显示数
     * @param cid 班级ID;从session中取出cid
     * @return
     */
    ServiceMultiResult queryInformation(Integer page,Integer limit,Integer cid,String title,String time);

    /**
     * 分页查询资讯信息
     *  查询资讯前5条供首页展示
     */
    ServiceMultiResult queryTheLatestInformation();

    /**
     * 分页查询资讯信息
     *  查询资讯前5条供首页展示
     */
    ServiceMultiResult queryAllInfo();

    /**
     * 添加资讯
     * 必须字段 文章标题、文章封面、正文('富文本'),班级ID
     * 非必须  文章来源、显示设置、外部URL
     * @param information
     * @return
     */
    ServiceResult addInformation(Information information);

    /**
     * 修改资讯
     * 同上
     * @param information 资讯
     * @return
     */
    ServiceResult update(Information information);
    /**
     * 根据ID查询资讯信息
     * @param id 资讯ID
     * @return
     */
    Information queryInformationById(Integer id);

    /**
     * 资讯列表 – 筛选查询
     * @param cid 班级ID
     * @param title 资讯标题
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */


    ServiceMultiResult queryByKeyWord(Integer cid, String title, String startTime, String endTime);


    /**
     * 根据资讯ID删除资讯
     * @param id 资讯
     * @return
     */
    ServiceResult deleteInformationById(String id);

    /**
     * 资讯的显示与隐藏
     * @param id 资讯ID
     * @param display 是否显示 0 1 表示
     * @return
     */


    ServiceResult updateDisplay(Integer id, Integer display);

    /**
     * 根据id修改显示状态
     * @param id 资讯ID
     * @param enable 前台状态值修改
     */
    int updStaById(Integer id,Integer enable);

    /**
     * 修改资讯
     * 同上
     * @param information 资讯
     * @return
     */
    int updateInfomationById(Information information);

    /**
     *
     * @param page
     * @param limit
     * @param cid
     * @param title
     * @param time
     * @param topicId
     * @return
     */

    ServiceMultiResult queryInformation2(Integer page,Integer limit,Integer cid,String title,String time,String topicId);

    /**
     * 修改资讯访问量+1
     */
    int updVisitById(String id);
}
