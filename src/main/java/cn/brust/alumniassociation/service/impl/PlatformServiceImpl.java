/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: PlatformServiceImpl
 * Author:   Administrator
 * Date:     2018/7/25 20:44
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Platform;
import cn.brust.alumniassociation.mapper.PlatformMapper;
import cn.brust.alumniassociation.service.PlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <pre>
 *     @author 洪升奇
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Service
public class PlatformServiceImpl implements PlatformService {

    @Autowired
    PlatformMapper platformMapper;

    @Override
    public ServiceResult queryPlatformInfo() {
        Platform platform = platformMapper.queryPlatformInfo();
        if (platform == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(platform);
    }

    @Override
    public ServiceResult update(Platform platform) {
        int update = platformMapper.updateByPrimaryKey(platform);
        if (update == 0) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(update);
    }

}