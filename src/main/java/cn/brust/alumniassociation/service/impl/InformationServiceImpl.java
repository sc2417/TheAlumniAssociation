/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: InformationServiceImpl
 * Author:   Administrator
 * Date:     2018/7/26 9:43
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Information;
import cn.brust.alumniassociation.mapper.InformationMapper;
import cn.brust.alumniassociation.service.InformationService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     @author 陈润颖
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Service
public class InformationServiceImpl implements InformationService {

    @Autowired
    InformationMapper informationMapper;

    @Override
    public ServiceMultiResult queryInformation(Integer page,Integer limit,Integer cid,String title,String time) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = null;
        if( time ==  null || time == ""){
            list = informationMapper.queryInfomation(cid, title);
        }else{
            String[] times = time.split(" - ");
            list = informationMapper.queryInfoWithTime(cid,title,times[0],times[1]);
        }
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }
    @Override
    public ServiceMultiResult queryInformation2(Integer page,Integer limit,Integer cid,String title,String time,String topicId) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = null;
        if( time ==  null || time == ""){
            list = informationMapper.queryInfomation(cid, title,topicId);
        }else{
            String[] times = time.split(" - ");
            list = informationMapper.queryInfoWithTime(cid,title,times[0],times[1],topicId);
        }
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public int updVisitById(String id) {
        return informationMapper.updVisitById(id);
    }

    @Override
    public ServiceMultiResult<Information> queryTheLatestInformation() {
        List<Information> info = informationMapper.queryTheLatestInformation();
        return new ServiceMultiResult<Information>(1,info);
    }

    @Override
    public ServiceMultiResult queryAllInfo() {
        return null;
    }

    @Override
    public ServiceResult addInformation(Information information) {
        informationMapper.addInformation(information);
        return null;
    }

    @Override
    public ServiceResult update(Information information) {

        return null;
    }

    @Override
    public Information queryInformationById(Integer id) {
        return informationMapper.queryInformationById(id);
    }

    @Override
    public ServiceMultiResult queryByKeyWord(Integer cid, String title, String startTime, String endTime) {
        return null;
    }

    @Override
    public ServiceResult deleteInformationById(String id) {
        int i = informationMapper.deleteInformationById(id);
        return new ServiceResult(i>0);
    }

    @Override
    public ServiceResult updateDisplay(Integer id, Integer display) {
        return null;
    }

    @Override
    public int updStaById(Integer id, Integer enable) {
        return informationMapper.updStaById(id,enable);
    }

    @Override
    public int updateInfomationById(Information information) {

        return informationMapper.updateInfomationById(information);
    }

}