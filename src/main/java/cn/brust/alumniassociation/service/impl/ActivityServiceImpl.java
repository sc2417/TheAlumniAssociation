/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: ActivityServiceImpl
 * Author:   Administrator
 * Date:     2018/7/26 9:30
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Activity;
import cn.brust.alumniassociation.entity.Enterprise;
import cn.brust.alumniassociation.mapper.ActivityMapper;
import cn.brust.alumniassociation.service.ActivityService;
import cn.brust.alumniassociation.utils.FileUtils;
import cn.brust.alumniassociation.utils.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * <pre>
 *     @author 黄华德
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    ActivityMapper activityMapper;

    @Override
    public ServiceResult addDynamics(String id, String userId, String activityId, String name, String level, String content) {
       return ServiceResult.success();
    }

    @Override
    public ServiceMultiResult dynamicQuery(Integer id) {
        List<Map<String, String>> maps = activityMapper.dynamicQuery(id);
        if(maps == null){
            return  new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(maps.size(),maps);
    }

    @Override
    public ServiceResult updateFabulous(int zan, String id) {
        int result = activityMapper.updateFabulous(zan, id);
        if(result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(result);
    }

    @Override
    public ServiceResult queryComment(int id) {
        Activity activity = activityMapper.queryComment(id);
        if (activity == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(activity);
    }

    @Override
    public ServiceMultiResult lookupActivityDetails(Integer id) {
        List<Map<String,String>>record = activityMapper.lookupActivityDetails(id);
        return new ServiceMultiResult(record.size(),record);
    }

    @Override
    public ServiceMultiResult detailsFindById(int id) {
        List<Map<String, String>> all = activityMapper.detailsFindById(id);
        return new ServiceMultiResult(all.size(),all);
    }

    @Override
    public ServiceResult modifyActivity(String title,String cover,String address,String type,String createDate,String endDate,String content,String id) {
        String startDate = createDate;
        int result = activityMapper.modifyActivity(title, cover,address,type,createDate,startDate,endDate,content,id);
        if(result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(result);
    }

    @Override
    public ServiceResult selectById(String id) {
        Activity activity = activityMapper.selectById(id);
        if (activity == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(activity);
    }

    @Override
    public ServiceResult modifyEnroll(String id) {
        int result = activityMapper.modifyEnroll(id);
        if(result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(result);
    }

    @Override
    public ServiceResult delActivity(String id) {
        int result = activityMapper.delActivity(id);
        if(result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(result);
    }

    @Override
    public ServiceResult addActivity(String title,String cover,String address,String type,String createDate,String endDate,String content) {
        UUIDUtils.getUUID();
        String startDate = createDate;
        int result = activityMapper.addActivity(UUIDUtils.getUUID(),title,cover,address,type,createDate,startDate,endDate,content);
        if(result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(result);
    }


    @Override
    public ServiceMultiResult queryVague(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<Map<String,String>> all = activityMapper.findAll();
        Page<Map<String, String>> result = (Page<Map<String, String>>) all;
        return  new ServiceMultiResult(result);
    }

    @Override
    public ServiceMultiResult queryVagueTime(Integer classId, String title, String time,Integer page,Integer limit,Integer Identification) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = null;
        if(time == null || time ==""){
            list = activityMapper.queryVague(classId,title);
        }else{
            String[] times = time.split(" - ");
            list = activityMapper.queryVagueTime(classId,title,times[0],times[1]);
        }
        list.forEach(System.out::println);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceMultiResult pageQuery(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = activityMapper.findAll();
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if(page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceMultiResult findAll() {
        List<Map<String, String>> all = activityMapper.findAll();
        return new ServiceMultiResult(all.size(),all);
    }


    @Override
    public ServiceMultiResult queryConductActivity(Integer page, Integer limit, Integer cid) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = activityMapper.queryConductActivity(cid);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceMultiResult queryEndActivity(Integer page, Integer limit, Integer cid) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = activityMapper.queryEndActivity(cid);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceResult<Activity> queryActivityDetails(Integer id) {
        Activity record=activityMapper.queryActivityDetails(id);
        if(record==null){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(record);
    }


    @Override
    public ServiceMultiResult queryActivityByStates(Integer page, Integer limit, Integer status, Integer cid) {
        return null;
    }

    @Override
    public ServiceResult queryActivityById(Integer userId) {
        return null;
    }

    @Override
    public ServiceMultiResult queryByKeyword(Integer cid, String title, String startTime, String endTime) {
        return null;
    }

    @Override
    public ServiceResult add(Activity activity) {
        return null;
    }

    @Override
    public ServiceResult update(Activity activity) {
        return null;
    }

    @Override
    public ServiceResult deleteActivityByid(Integer id) {
        return null;
    }

    @Override
    public ServiceResult updateStates(Integer id, Integer status) {
        return null;
    }
    @Override
    public ServiceResult<Activity> getActivityOne(String cId) {
        Activity record=activityMapper.getActivityOne(cId);
        if(record==null){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(record);
    }

    @Override
    public ServiceMultiResult getMyInActivity(Integer page, Integer limit,String uId) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = activityMapper.getMyInActivity(uId);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceMultiResult getMyEndActivity(Integer page, Integer limit,String uId) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = activityMapper.getMyEndActivity(uId);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }
}