package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Member;
import cn.brust.alumniassociation.entity.User;
import cn.brust.alumniassociation.mapper.MemberMapper;
import cn.brust.alumniassociation.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author
 * @since
 * @since
 */
@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    MemberMapper  memberMapper;

    @Override
    public ServiceResult updatePasswd(String email, String oldPwd,String newPwd) {
        System.out.println(email);
        System.out.println(oldPwd);
        Member member = memberMapper.findMember(email, oldPwd);
        System.out.println(member);
        if (member == null) {
            return ServiceResult.notFound();
        } else {
            int i = memberMapper.updatePwd(email, newPwd);
            return ServiceResult.of(i);
        }
    }

    @Override
    public ServiceResult findMember(String email, String pwd) {
        Member member = memberMapper.findMember(email, pwd);
        if (member == null) {
            return ServiceResult.notFound();
        } else {

            return ServiceResult.of(member);
        }
    }

    @Override
    public ServiceResult updateAuatar(String email, String avatar) {
        int i = memberMapper.updateAuatar(email, avatar);
        if (i == 1) {
            return ServiceResult.notFound();
        } else {

            return ServiceResult.of(i);
        }
    }

    @Override
    public ServiceResult findMemberemail(String email) {
        Member member = memberMapper.findMemberEmail(email);
        if (member == null) {
            return ServiceResult.notFound();
        } else {

            return ServiceResult.of(member);
        }
    }
    /**  <pre>
     *  根据手机号密码班级ID查询用户
     * @author 陈佳婷
     * @since 1.0
     * @since JDK 8.0
     * @param mobile :手机号
     * @param passwd :密码
     * @param classId :班级ID
     * @return :  ServiceResult
     * </pre>
     */
    @Override
    public ServiceResult login(String mobile, String passwd, String classId) {
        Member user  = memberMapper.selectPhonePasswdClassId(mobile,passwd,classId);
        if (user == null) {
            return ServiceResult.of(null);
        }
        return ServiceResult.of(user.getId());
    }
}
