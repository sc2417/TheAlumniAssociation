package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Role;
import cn.brust.alumniassociation.mapper.RoleMapper;
import cn.brust.alumniassociation.service.RoleService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Administrator 2018/8/13 16:04
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleMapper roleMapper;

    @Override
    public ServiceResult selectById(String id) {
        Role role = roleMapper.selectById(id);
        if (role == null ){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(role);
    }

    @Override
    public ServiceMultiResult selectAll(Integer page,Integer limit,String classId) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = roleMapper.selectAll(classId);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages())
        {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceResult update(Role role, String id) {
        role.setId(id);
        int result = roleMapper.update(role);
        return result > 0 ? ServiceResult.success() : ServiceResult.notFound();
    }

    @Override
    public ServiceResult delete(String id) {
        int result = roleMapper.delete(id);
        return result > 0 ? ServiceResult.success() : ServiceResult.notFound();
    }

    @Override
    public ServiceResult add(Role role) {
        role.setId("R"+ UUID.randomUUID().toString().replace("-",""));
        int result = roleMapper.add(role);
        return result > 0 ? ServiceResult.success() : ServiceResult.notFound();
    }

    @Override
    public ServiceMultiResult selectPermissionByRoleId(String id) {
        List<Map<String, String>> list = roleMapper.selectPermissionByRoleId(id);
        if (list == null){
            return new ServiceMultiResult(0,null);
        }
        return new ServiceMultiResult(list.size(),list);
    }
}