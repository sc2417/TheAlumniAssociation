/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: FeedbackServiceImpl
 * Author:   Administrator
 * Date:     2018/7/26 9:43
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Feedback;
import cn.brust.alumniassociation.mapper.FeedbackMapper;
import cn.brust.alumniassociation.service.FeedbackService;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <pre>
 *     @author
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    FeedbackMapper feedbackMapper;

    @Override
    public ServiceMultiResult queryFeedback(Integer limit, Integer page) {
        return null;
    }

    @Override
    public ServiceResult delete(Integer id) {
        int delete = feedbackMapper.deleteByPrimaryKey(id);
        if (delete == 0) {
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult handle(Integer id, String handle) {
        int update = feedbackMapper.update(id, handle);
        if (update == 0) {
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult add(String opinion, String userId) {
        String UUIDs = UUID.randomUUID().toString();
        List<Map<String, String>> maps = feedbackMapper.queryId(UUIDs);
        int insert = 0;
        if (maps.toString().equalsIgnoreCase("[]")){
            insert = feedbackMapper.insert(UUIDs,opinion, userId);
        }else{
            add(opinion, userId);
        }
        if (insert == 0) {
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceMultiResult queryAll(String releaseTime, Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = null;

        if( releaseTime ==  null || releaseTime == ""){
            list = feedbackMapper.queryAll();
        }else{
            String[] times = releaseTime.split(" - ");
            list = feedbackMapper.queryTimeAll(times[0], times[1]);
        }
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }
}