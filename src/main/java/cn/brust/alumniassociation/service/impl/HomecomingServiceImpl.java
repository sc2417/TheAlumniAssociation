package cn.brust.alumniassociation.service.impl;


import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Homecoming;
import cn.brust.alumniassociation.mapper.HomecomingMapper;
import cn.brust.alumniassociation.service.HomecomingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * <pre>
 *     @author 洪升奇
 *     @since   id.0
 *     @since   JDK 8.0
 * </pre>
 */
@Service
public class HomecomingServiceImpl implements HomecomingService {

    @Autowired
    HomecomingMapper homecomingMapper;

    @Override
    public ServiceResult update(String field, Integer display, String content) {
        Homecoming record = new Homecoming();
        int update = 0;
        String id = "HC"+ UUID.randomUUID().toString().replace("-","");
        if("学院介绍".equalsIgnoreCase(field)){
            record.setId(id);
            record.setDetails(content);
            record.setDetailsEnable(display);
            update = homecomingMapper.updateDetailsByPrimaryKey(record);
        }else if("同学会介绍".equalsIgnoreCase(field)){
            record.setId(id);
            record.setClassmate(content);
            record.setClassmateEnable(display);
            update = homecomingMapper.updateClassmateByPrimaryKey(record);
        }else if("同学会章程".equalsIgnoreCase(field)){
            record.setId(id);
            record.setStatutes(content);
            record.setStatusEnable(display);
            update = homecomingMapper.updateStatutesByPrimaryKey(record);
        }

        if (update == 0){
            return ServiceResult.notFound();
        }else{
            return ServiceResult.of(update);
        }
    }

    @Override
    public ServiceResult queryAll() {
        Homecoming homecoming = homecomingMapper.query();
        if (homecoming == null){
            return ServiceResult.notFound();
        }else{
            return ServiceResult.of(homecoming);
        }
    }


}
