/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: ClassyServiceImpl
 * Author:   Administrator
 * Date:     2018/7/26 9:31
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Classy;
import cn.brust.alumniassociation.mapper.ClassyMapper;
import cn.brust.alumniassociation.service.ClassyService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br> 
 * 〈〉
 *
 * @author Administrator
 * @create 2018/7/26
 * @since 1.0.0
 */
@Service
public class ClassyServiceImpl implements ClassyService {

    @Autowired
    ClassyMapper classyMapper;

    @Override
    public ServiceMultiResult queryClassy(Integer limit, Integer page) {
        return null;
    }

    @Override
    public ServiceMultiResult queryAllClassy() {

        List<Classy> result = classyMapper.queryAll();
        if(result==null){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result.size(),result);
    }

    @Override
    public List<Classy> selectAll() {
        return classyMapper.selectAll();
    }

    @Override
    public ServiceMultiResult selectAllLimit(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        List<Classy> list = classyMapper.selectAll();
        Page<Classy> result = (Page<Classy>) list;
        if(page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceResult addClassy(Classy classy) {
        int add = classyMapper.addClassy(classy);
        if(add == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public Classy selectById(Integer id) {
        return classyMapper.selectById(id);
    }

    @Override
    public ServiceResult updateClassy(String name,String userId, Integer isEnable, Integer id) {
        int upd = classyMapper.updateClassy(name, userId,isEnable, id);
        if(upd == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult delClassy(Integer id) {
        int del = classyMapper.delClassy(id);
        if(del == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public Classy queryById(Integer id) {
        return classyMapper.queryById(id);
    }


}