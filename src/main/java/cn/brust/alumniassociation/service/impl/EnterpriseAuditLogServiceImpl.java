package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.EnterpriseAuditLog;
import cn.brust.alumniassociation.entity.UserAuditLog;
import cn.brust.alumniassociation.mapper.EnterpriseAuditLogMapper;
import cn.brust.alumniassociation.service.EnterpriseAuditLogService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator 2018/8/12 15:17
 */
@Service
public class EnterpriseAuditLogServiceImpl implements EnterpriseAuditLogService {

    @Autowired
    EnterpriseAuditLogMapper enterpriseAuditLogMapper;

    @Override
    public ServiceResult add(EnterpriseAuditLog enterpriseAuditLog) {
        int result = enterpriseAuditLogMapper.add(enterpriseAuditLog);
        return result > 0 ? ServiceResult.notFound() : ServiceResult.of(result);
    }

    @Override
    public ServiceMultiResult selectByEnterpriseId(String eid) {
        List<EnterpriseAuditLog> enterpriseAuditLog = enterpriseAuditLogMapper.selectByEnterpriseId(eid);
        System.out.println(enterpriseAuditLog);
        return new ServiceMultiResult<EnterpriseAuditLog>(enterpriseAuditLog.size(), enterpriseAuditLog);
    }

    @Override
    public ServiceResult selectById(String id) {
        EnterpriseAuditLog enterpriseAuditLog = enterpriseAuditLogMapper.selectById(id);
        return enterpriseAuditLog != null ? ServiceResult.notFound() : ServiceResult.of(enterpriseAuditLog);
    }

    @Override
    public ServiceResult addAuditLog(String id, String enterpriseId, String msg, Integer auditStatus, String auditDate, String createPerson) {
        int i = enterpriseAuditLogMapper.addAuditLog(id, enterpriseId, msg, auditStatus, auditDate, createPerson);
        if (i == 0) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(i);
        }
    }
}