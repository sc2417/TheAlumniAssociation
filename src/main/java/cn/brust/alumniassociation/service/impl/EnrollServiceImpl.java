package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Activity;
import cn.brust.alumniassociation.entity.Enroll;
import cn.brust.alumniassociation.mapper.EnrollMapper;
import cn.brust.alumniassociation.service.EnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by Administrator on 2018/8/16 0016.
 */
@Service
public class EnrollServiceImpl implements EnrollService {
    @Autowired
    EnrollMapper enrollMapper;

    @Override
    public ServiceResult queryById(String wxId, String acId) {
        System.out.println(wxId+"-"+acId);
        Enroll enroll = enrollMapper.queryById(wxId,acId);
        if (enroll == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(enroll);
    }

    @Override
    public int activityEnroll(String wxId, String acId) {
        String replace = UUID.randomUUID().toString().replace("-", "")+"";
        if(enrollMapper.queryById(wxId,acId)==null){
            return enrollMapper.activityEnroll(replace,wxId,acId);
        }
        return 0;
    }
}
