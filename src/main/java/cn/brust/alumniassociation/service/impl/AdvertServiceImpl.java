package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Advert;
import cn.brust.alumniassociation.mapper.AdvertMapper;
import cn.brust.alumniassociation.service.AdvertService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     @author 郑锦波、孙超
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Service
public class AdvertServiceImpl implements AdvertService {

    @Autowired
    AdvertMapper advertMapper;


    @Override
    public ServiceMultiResult queryAdverts() {
        List<Advert> all = advertMapper.findAll();
        return new ServiceMultiResult(all.size(),all);
    }

    @Override
    public List<Advert> findAll() {
      return advertMapper.findAll();
    }

    @Override
    public ServiceMultiResult findAllLimit(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        List<Advert> list = advertMapper.findAll();
        Page<Advert> result = (Page<Advert>) list;
        if(page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }
    @Override
    public ServiceMultiResult vagueQuery(String title, Integer site,Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = advertMapper.vagueQuery(title, site);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if(page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceMultiResult vagueQueryAll(String title,Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = advertMapper.vagueQueryAll(title);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if(page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceMultiResult queryAdvert(Integer limit, Integer page) {
        return null;
    }

    @Override
    public ServiceResult updateStates(Integer id, Integer states) {
        return null;
    }

    @Override
    public ServiceResult delAdvert(int id) {
        int del = advertMapper.delAdvert(id);
        if(del == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult addAdvert(Advert advert) {
        int add = advertMapper.addAdvert(advert);
        if(add == 0){
            return  ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public Advert queryById(Integer id) {
       return advertMapper.queryById(id);
    }

    @Override
    public ServiceResult updateAdvert(String title, Integer site, String cover,String url, String desc, Integer isStatus,Integer isTop, Integer id) {
        int update = advertMapper.updateAdvert(title,site,url,cover,desc,isStatus,isTop,id);
        if(update == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult settop(Integer id) {
        int top = advertMapper.settop(id);
        if(top == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult canceltop(Integer id) {
        int top = advertMapper.canceltop(id);
        if(top == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public Advert queryCount() {
        return advertMapper.queryCount();
    }

}
