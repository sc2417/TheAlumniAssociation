package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.UserAuditLog;
import cn.brust.alumniassociation.mapper.UserAuditLogMapper;
import cn.brust.alumniassociation.service.UserAuditLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author
 * @since
 * @since
 */
@Service
public class UserAuditLogServiceImpl implements UserAuditLogService {
    @Autowired
    UserAuditLogMapper userAuditLogMapper;

    @Override
    public ServiceResult addAuditLog(String id, String userId, String msg, Integer auditStatus, String auditPerson, String createDate) {
        int auditLog = userAuditLogMapper.insertAuditLog(id, userId, msg, auditStatus, auditPerson, createDate);
        if (auditLog == 0) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(auditLog);
        }
    }

    @Override
    public ServiceMultiResult queryUserAuditLog(String userId) {
        List<UserAuditLog> userAuditLogs = userAuditLogMapper.queryUserAuditLog(userId);
        return new ServiceMultiResult<UserAuditLog>(userAuditLogs.size(), userAuditLogs);
    }
}
