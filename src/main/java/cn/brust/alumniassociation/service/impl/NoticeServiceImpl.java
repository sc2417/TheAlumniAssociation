/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: NoticeServiceImpl
 * Author:   Administrator
 * Date:     2018/7/26 9:44
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;

import cn.brust.alumniassociation.entity.Notice;
import cn.brust.alumniassociation.mapper.NoticeMapper;
import cn.brust.alumniassociation.service.NoticeService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * <pre>
 *     @author 陈佳婷、马丹萍
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
@Service
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    NoticeMapper noticeMapper;

    @Override
    public ServiceMultiResult getNotices(Integer page, Integer limit,String title,String startTime,String endTime) {
        PageHelper.startPage(page, limit);
        List<Map<String, String>> list = noticeMapper.queryNotices(title,startTime,endTime);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        result.setPageSize(result.size());
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public List<Notice> findAll() {
        return noticeMapper.findAll();
    }


    @Override
    public ServiceMultiResult queryNoticeByKeyWord(String title, String startTime, String endTime, Integer page, Integer limit) {
        return null;
    }

    @Override
    public ServiceResult queryNoticeById(Integer id) {
        return null;
    }

    /**
     * 发布公告
     * @param notice 公告
     * @return
     */
    @Override
    public ServiceResult add(Notice notice) {
        int isSuccess = noticeMapper.add(notice);
        if(isSuccess == 0 ){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(isSuccess);
    }

    @Override
    public ServiceResult deleteById(Integer id) {
        return null;
    }
    @Override
    public ServiceResult updateDel(String id) {
        int del = noticeMapper.updateDel(id);
        if (del == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult queryNoticeInfoById(String id) {
        Map<String, String> notice  = noticeMapper.selectById(id);
        if (notice == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(notice);
    }


}