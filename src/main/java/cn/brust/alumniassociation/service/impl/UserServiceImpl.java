/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: UserServiceImpl
 * Author:   Administrator
 * Date:     2018/7/26 9:46
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.User;
import cn.brust.alumniassociation.mapper.UserMapper;
import cn.brust.alumniassociation.service.UserService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     @author 王永泽
 *     @since 1.0
 *     @since JDK 8.0
 * </pre>
 */
@Service
public class UserServiceImpl implements UserService {



    @Autowired
    UserMapper userMapper;
    /**  <pre>
     * 注册
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param user
     * </pre>
     */
    @Override
    public ServiceResult applyToJoin(User user) {
        int isSuccess = userMapper.applyToJoin(user);
        if(isSuccess == 0 ){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(isSuccess);
    }


    /**  <pre>
     *  根据手机号查询用户
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param mobile :手机号
     * @return :  ServiceResult
     * </pre>
     */
    @Override
    public ServiceResult selectByMobile(String mobile){
        User user  = userMapper.selectByMobile(mobile);
        if (user == null) {
            return ServiceResult.of(null);
        }
        /* return ServiceResult.of(user.getAuditStatus());*/
        return ServiceResult.of(user);
    }

    /**
     * <pre>
     *  登录
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param null :
     * @return : null
     * </pre>
     */
    @Override
    public ServiceResult login(String phone, String passwd, Integer classId) {
        User user = userMapper.selectPhonePasswd(phone, passwd);
        if (user == null) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(user);
        }
    }

    @Override
    public ServiceResult queryByWxId(String openId) {
        User user = userMapper.queryByWxId(openId);
        if (user == null) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(user);
        }

    }

    @Override
    public ServiceResult myInfo(String wxId) {
        Map<String, String> user = userMapper.queryHomecomingDynamic(wxId);
        System.out.println(user);
        if (user == null) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(user);
        }
    }

    /**
     * 修改密码
     *
     * @param email
     * @param oldPasswd
     * @param newPasswd
     * @return
     */
    @Override
    public ServiceResult updatePasswd(String email, String oldPasswd, String newPasswd) {
        User use = userMapper.selectUser(email, oldPasswd);
        if (use == null) {
            return ServiceResult.notFound();
        } else {
            User user1 = new User();
            user1.setPasswd(newPasswd);
            user1.setEmail(email);
            int line = userMapper.updatePassword(user1);
            return ServiceResult.of(line);
        }
    }

    /**
     * 查询
     *
     * @param email
     * @param oldPasswd
     * @return
     */
    @Override
    public ServiceResult selectUsers(String email, String oldPasswd) {
        User user = userMapper.selectUser(email, oldPasswd);
        if (user == null) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(user);
        }
    }

    /*修改图片路径*/
    @Override
    public ServiceResult updateAvatar(String email, String avatar) {
        User user = new User();
        user.setEmail(email);
        user.setAvatar(avatar);
        int link = userMapper.updateAvatar(email, avatar);

        if (link == 0) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(link);
        }
    }


    @Override
    public ServiceMultiResult filtrateQuery(String name, String mobile, Integer classId, String createTime,String  suditStatus,String suditStatu, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        String beginTime = null;
        String endTime = null;
        List<Map<String, String>> list = null;
        if (createTime == null || createTime == "") {
            beginTime = "0000-00-00";
            endTime = "2099-12-31";
            list = userMapper.filterQuery(name, mobile, classId, beginTime, endTime,suditStatus,suditStatu);
            Page<Map<String, String>> result = (Page<Map<String, String>>) list;
            return new ServiceMultiResult(result);
        } else {
            String[] time = createTime.split(" - ");
            beginTime = time[0] ;
            endTime = time[1];
            list = userMapper.filterQuery(name, mobile, classId, beginTime, endTime,suditStatus,suditStatu);
            Page<Map<String, String>> result = (Page<Map<String, String>>) list;
            return new ServiceMultiResult(result);
        }
    }

    @Override
    public ServiceMultiResult queryUsers(Integer page, Integer limit) {
        return null;
    }


    @Override
    public ServiceResult updateUserEnable(String wxId, Integer enable) {

        int link = userMapper.updateUserEnable(wxId,enable);
        if (link   ==1) {
            return ServiceResult.notFound();
        } else {
            System.out.println("冻结状态操做");
            return ServiceResult.of(link);
        }
    }

    @Override
    public ServiceResult delUser(String wxId) {
        int del = userMapper.delUser(wxId);
        if(del == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }
    @Override
    public ServiceResult addClassRole(String wxId, String name,String mobile, String email, Integer classId, String passwd, String describe) {
        int add = userMapper.addClassRole(wxId,name,mobile,email,classId,passwd,describe);
        if(add == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }
    @Override
    public ServiceResult updateMyIfon(String wxId, String nickname, String mobile, String email, String classrole) {
        int upd=userMapper.updateMyInfo(wxId,nickname,mobile,email,classrole);
        if(upd == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(upd);
    }

    @Override
    public ServiceMultiResult<User> queryByClassId(Integer classId) {
        List<User> user = userMapper.queryByClassId(classId);
        if(user == null){
            return  new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(user.size(),user);
    }

    @Override
    public User findByWxId(String wxId) {
        return userMapper.queryByWxId(wxId);
    }

    @Override
    public ServiceResult queryHomecomingDynamic(String wxId) {
        Map<String, String> user = userMapper.queryHomecomingDynamic(wxId);
        if (user == null) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(user);
        }
    }


    @Override
    public ServiceResult updateStates(String id, String examine, String examineDetails) {
        return null;
    }

    @Override
    public ServiceResult update(String id, String nickname, String name, String email, String passwd, String rPasswd, String desc) {
        User user = new User();
        user.setWxId(id);
        user.setNickname(nickname);
        user.setName(name);
        user.setEmail(email);
        user.setPasswd(passwd);
        return null;
    }

    @Override
    public ServiceResult updateRanges(String id, String rangesManager, String rangesOperation) {
        return null;
    }

    /*查询所有会员*/
    @Override
    public ServiceMultiResult selectAll(Integer page, Integer limit) {

        PageHelper.startPage(page, limit);
        List<Map<String, String>> list = userMapper.selectAll();
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceMultiResult classAllUser(Integer classId, Integer page, Integer limit) {
        System.out.println(classId);
        PageHelper.startPage(page, limit);
        List<Map<String, String>> list = userMapper.classyAllUser(classId);
        System.out.println(list);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        System.out.println(result);
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    /**
     * <pre>
     *  添加企业 - 企业所属班级模块
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @return : 所属班级的所有学员
     * </pre>
     */
    @Override
    public ServiceMultiResult classyAllUser(Integer classId) {
        ServiceMultiResult<Object> result = new ServiceMultiResult<>();
        List<Map<String, String>> list = userMapper.classyAllUser(classId);
        ArrayList<User> arrayList = new ArrayList<User>();
        if (list == null) {
            result.setResult(null);
        } else {
            for (Map<String, String> map : list) {
                User user = new User();
                user.setWxId(map.get("wx_id"));
                user.setName(map.get("name"));
                arrayList.add(user);
            }
        }
        return new ServiceMultiResult(arrayList.size(), arrayList);
    }

    /**
     * <pre>
     *  会员管理 - 会员审核
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param wxId : 会员ID
     * @param status    审核状态
     * @return :
     * </pre>
     */
    @Override
    public ServiceResult updateAuditStatus(String wxId, String status) {
        int count=userMapper.updateAuditStatus(wxId, status);
        if (count == 1) {
            return ServiceResult.of(count);
        } else {
            return ServiceResult.notFound();
        }
    }

    /**
     * <pre>
     * 根据ID查询 改班级下的会员
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult<cn.brust.alumniassociation.entity.User>
     * </pre>
     */
    @Override
    public ServiceMultiResult<User> selectAllByClassId(Integer classId) {
        List<User> users = userMapper.selectAllByClassId(classId);
        return new ServiceMultiResult<User>(users.size(), users);
    }

    /**
     * <pre>
     *  通过邮箱和密码登录
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param accounts 邮箱或手机号码
     * @param passwd 密码
     * @return
     * </pre>
     */
    @Override
    public ServiceResult pwdCorrect(String accounts, String passwd) {
        Map map = userMapper.pwdCorrect(accounts, passwd);
        if (map == null) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(map);
        }
    }

    @Override
    public ServiceResult updateClassRole(String name, String mobile, String email, String passwd, String describe, String wxId) {
        int update = userMapper.updateClassRole(name, mobile, email, passwd, describe, wxId);
        if (update == 0) {
            ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult addWXUser(String openId) {
        int result = userMapper.addWXUser(openId);
        if(result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(result);
    }

    /**
     * 查询此邮箱是否有信息
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param email
     * @return
     */
    @Override
    public ServiceResult selectEmail(String email) {
        User user = userMapper.selectEmail(email);
        if (user == null) {
            return ServiceResult.notFound();
        } else {
            return ServiceResult.of(user);
        }
    }

    /**
     * 给邮箱发送验证码
     * @author 洪升奇
     * @since 1.0
     * @since JDK 8.0
     * @param email 用户邮箱
     * @return
     */
    @Override
    public boolean sendEmail(String email, HttpSession session) {
        try {
            int code = (int) ((Math.random()*9+1)*100000);
            HtmlEmail htmlEmail = new HtmlEmail();//不用更改
            htmlEmail.setHostName("smtp.qq.com");//需要修改，126邮箱为smtp.126.com,163邮箱为163.smtp.com，QQ为smtp.qq.com
            htmlEmail.setCharset("UTF-8");
            htmlEmail.addTo(email);// 收件地址

            htmlEmail.setFrom("1442966006@qq.com", "同学会管理员");//此处填邮箱地址和用户名,用户名可以任意填写

            htmlEmail.setAuthentication("1442966006@qq.com", "bsuzjuqeegzrjjic");//此处填写邮箱地址和客户端授权码

            htmlEmail.setSubject("同学会验证码");//此处填写邮件名，邮件名可任意填写
            htmlEmail.setMsg("尊敬的用户您好,您本次注册的验证码是:" + code);//此处填写邮件内容
            session.setAttribute("code", code);
            htmlEmail.send();
            return true;
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    /**
     * 修改密码

     */
    @Override
    public ServiceResult emailUpdatePasswd(String email, String password) {
        User user1 = new User();
        user1.setPasswd(password);
        user1.setEmail(email);
        int line = userMapper.updatePassword(user1);
        if (line == 1) {
            return ServiceResult.of(line);
        } else {
            return ServiceResult.notFound();
        }
    }
}


