package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Topic;
import cn.brust.alumniassociation.mapper.TopicMapper;
import cn.brust.alumniassociation.service.TopicService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/8/2 0002.
 */
@Service
public class TopicServiceImpl implements TopicService {
    @Autowired
    TopicMapper topicMapper;
    @Override
    public ServiceMultiResult getTopics(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        List<Map<String, String>> list = topicMapper.getTopics();
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        result.setPageSize(result.size());
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceMultiResult getAllTopic() {
        List<Topic> allTopic = topicMapper.getAllTopic();
        if(allTopic == null){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(allTopic.size(),allTopic);
    }


    @Override
    public ServiceResult updateEnable(Integer enable,String id) {
        int result = topicMapper.updateEnable(enable,id);
        if (result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult updateDel(String id) {
        int result = topicMapper.updateDel(id);
        if (result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult add(String id,String title,Integer sort,Integer enable) {
        int isSuccess = topicMapper.add(id,title,sort,enable);
        if(isSuccess == 0 ){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(isSuccess);
    }
    /**
     * 根据分类ID查询分类详情
     *
     * @param id 分类ID
     * @return
     */
    @Override
    public ServiceResult queryTopicInfoById(String id) {
        Map<String, String> notice  = topicMapper.selectById(id);
        if (notice == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(notice);
    }

    @Override
    public ServiceResult updateTopic(Integer enable, String id, Integer sort,String title) {
        int result = topicMapper.updateTopic(enable,id,sort,title);
        if (result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult selectTopic() {
        List topic=topicMapper.selectTopic();
        if(topic==null){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(topic);
    }
}
