/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: ProjectServiceImpl
 * Author:   Administrator
 * Date:     2018/7/26 9:45
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Project;
import cn.brust.alumniassociation.mapper.ProjectMapper;
import cn.brust.alumniassociation.service.ProjectService;
import cn.brust.alumniassociation.utils.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br> 
 * 〈〉
 *
 * @author Administrator
 * @create 2018/7/26
 * @since 1.0.0
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectMapper projectMapper;

    /**
     * 查询项目(分页)
     *
     * @param page  页数
     * @param limit
     * @param classId   班级ID
     * @return
     */
    @Override
    public ServiceMultiResult getProjects(Integer page, Integer limit, String classId) {
        PageHelper.startPage(page, limit);
        List<Map<String, String>> list = projectMapper.queryProjects(classId);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
//        System.out.println(result);
//        list.forEach(System.out::println);
//        return new ServiceMultiResult(10,list);
        return new ServiceMultiResult(result);
    }

    /**
     * <pre>
     *  后台管理 - 项目列表
     *  根据关键字查询,如果条件为空即为无条件查询 仅分页
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param page :分页
     * @param limit :分页
     * @param classId : 班级ID 可为空
     * @param budget : 项目预算 可为空
     * @param nameOrMobile : 会员名称/手机号 可为空
     * @param startTime : 发布时间-开始 可为空
     * @param endTime : 发布时间-结束 可为空
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     * </pre>
     */
    @Override
    public ServiceMultiResult getProjectsByKeyWord(Integer page, Integer limit, String classId, Integer budget, String nameOrMobile, String startTime, String endTime) {
        PageHelper.startPage(page, limit);
        List<Map<String, String>> list = projectMapper.adminQueryProjectByKeyWord(classId, budget, nameOrMobile, startTime, endTime);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    /**
     * <pre>
     *
     * /**  <pre>
     *  后台管理 - 项目审核列表
     *  根据关键字查询,如果条件为空即为无条件查询 仅分页
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param page :分页
     * @param limit :分页
     * @param classId : 班级ID 可为空
     * @param budget : 项目预算 可为空
     * @param nameOrMobile : 会员名称/手机号 可为空
     * @param startTime : 发布时间-开始 可为空
     * @param endTime : 发布时间-结束 可为空
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     * </pre>
     */
    @Override
    public ServiceMultiResult getAuditProjectsByKeyWord(Integer page, Integer limit, String classId, Integer budget, String nameOrMobile, String startTime, String endTime) {
        PageHelper.startPage(page, limit);
        List<Map<String, String>> list = projectMapper.adminQueryAuditProjectByKeyWord(classId, budget, nameOrMobile, startTime, endTime);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }


    /**
     * 查询用户所有项目
     *
     * @param page   页数
     * @param limit
     * @param userid 用户ID
     * @return
     */
    @Override
    public ServiceMultiResult getProjectsByUserid(Integer page, Integer limit, String userid) {
        PageHelper.startPage(page,limit);
        List<Map<String, String>> list = projectMapper.getProjectsByUserid(userid);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()) {
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    /**
     * 添加项目
     *
     * @param project
     * @return
     */
    @Override
    public ServiceResult addProject(Project project, String path) {
        String id = UUIDUtils.getUUID();
        project.setEnclosure(path);
        project.setId(id);
        int i = projectMapper.addProject(project);
        if(i>0){
            return ServiceResult.of(id);
        }else{
            return new ServiceResult(false);
        }

    }

    /**
     * 根据项目ID查询项目详情
     *
     * @param id 项目ID
     * @return
     */
    @Override
    public ServiceResult getProjectById(String id) {
        Map project = projectMapper.getProjectById(id);
        if(project==null){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(project);
    }

    /**
     * 根据项目ID查询项目跟进状态
     *
     * @param id 项目跟进
     * @return
     */
    @Override
    public ServiceResult getProjectRecord(String id) {
        Map record = projectMapper.getProjectRecord(id);
        if(record==null){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(record);
    }

    /**
     * <pre>
     *  后台管理 - 项目列表 - 项目跟进 - 录入进度
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @param status :项目状态 0未发布 1已取消 2发布中 3跟进中 4已签单
     * @param msg :记录
     * @return : cn.brust.alumniassociation.base.ServiceResult
     * </pre>
     */
    @Override
    public ServiceResult updateProjectRecord(String id, Integer status, String msg) {

        int i = projectMapper.updateProjectRecord(id, status, msg);
        return new ServiceResult(i>0);
    }

    /**
     * 更改项目状态
     *
     * @param id     项目ID
     * @param status 项目状态 0未发布 1已取消 2发布中 3跟进中 4已签单
     * @return
     */
    @Override
    public ServiceResult updateStatus(String id, Integer status) {
//        这里需要判断id是否存在  后面添加DTO再加上
        int i = projectMapper.updateStatus(id, status);
        return new ServiceResult(i>0);
    }

    /**
     * 修改项目
     *
     * @param project
     * @return
     */
    @Override
    public ServiceResult updateProjectById(Project project) {
        int i = projectMapper.updateProject(project);
        return new ServiceResult(i>0);
    }

    /**
     * 软删
     *
     * @param id 项目id
     * @return
     */
    @Override
    public ServiceResult deleteProjectById(String id) {
        int i = projectMapper.deleteProjectById(id);
        return new ServiceResult(i>0);
    }

    /**
     * 修改项目审核状态并添加审核记录
     *
     * @param id
     * @param auditStatus 0未审核 1审核不通过 2审核通过
     * @param msg         审核详情
     * @return
     */
    @Override
    public ServiceResult updateAuditStatu(String id, Integer auditStatus, String msg) {
        int i = projectMapper.updateAudit(id, auditStatus, msg);
        return new ServiceResult(i>0);
    }

    /**
     * 查看项目审核记录
     *
     * @param id 项目ID
     * @return
     */
    @Override
    public ServiceResult getAuditLog(String id) {
        Map logs = projectMapper.getAuditLogById(id);
        if(logs==null){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(logs);
    }

    /**
     * 添加项目浏览记录 +1
     *
     * @param id
     * @return
     */
    @Override
    public ServiceResult addVisit(String id) {
        int i = projectMapper.addVisit(id);
        return new ServiceResult(i>0);
    }

    @Override
    public ServiceResult wxUpdateProject(Project project) {
        int result = projectMapper.wxUpdateProject(project);
        if(result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

}
