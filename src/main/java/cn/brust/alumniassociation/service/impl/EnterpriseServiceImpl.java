/**
 * Copyright (C), 2015-2018, XXX有限公司
 * FileName: EnterpriseServiceImpl
 * Author:   Administrator
 * Date:     2018/7/26 9:34
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package cn.brust.alumniassociation.service.impl;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Enterprise;
import cn.brust.alumniassociation.entity.EnterpriseAuditLog;
import cn.brust.alumniassociation.mapper.EnterpriseAuditLogMapper;
import cn.brust.alumniassociation.mapper.EnterpriseMapper;
import cn.brust.alumniassociation.service.EnterpriseService;
import cn.brust.alumniassociation.utils.FileUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author 马敏婷
 * @since 1.0
 * @since jdk8
 */
@Service
public class EnterpriseServiceImpl implements EnterpriseService {

    @Autowired
    EnterpriseMapper enterpriseMapper;

    @Autowired
    EnterpriseAuditLogMapper enterpriseAuditLogMapper;

    @Override
    public ServiceMultiResult queryEnterprise() {
        return null;
    }

    @Override
    public ServiceMultiResult findPageEnterprise(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<Map<String,String>> list = enterpriseMapper.queryEnterprises(null,null,null,null,null);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        return  new ServiceMultiResult(result);
    }

    /**
     * 企业详情
     * @param id
     * @return
     */
    @Override
    public ServiceResult queryEnterpriseInfoById(String id) {
        Enterprise enterprise = enterpriseMapper.selectById(id);
        if (enterprise == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(enterprise);
    }

    /**
     * 添加企业信息/认证
     * @param enterprise
     * @return
     */
    @Override
    public ServiceResult add(Enterprise enterprise, MultipartFile file, HttpServletRequest request) throws IOException {
        String logo = FileUtils.saveFile(file, request, "enterprise");
        enterprise.setId("EN"+ new SimpleDateFormat("yyyymmddhh").format(new Date())+new Random().nextInt(200));
        Enterprise isExists = enterpriseMapper.selectById(enterprise.getId());
        if (isExists != null){
            add(enterprise,file,request);
        }
        enterprise.setAuditStatus(4);
        enterprise.setLogo(logo);
        enterprise.setCreateTime(new Date());
        int result = enterpriseMapper.add(enterprise);
        if (result == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.of(result);
    }


    /**  <pre>
     *  修改
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param enterprise : 企业信息
     * @param file : 上传的LOGO
     * @param request : HttpServletRequest
     * @return : 返回受影响的行数
     * </pre>
     */
    @Override
    public ServiceResult update(Enterprise enterprise,MultipartFile file,HttpServletRequest request) throws IOException {

        if (file.getOriginalFilename()!=null && file.getOriginalFilename() != ""){
            String logo = FileUtils.saveFile(file,request,"enterprise");
            enterprise.setLogo(logo);
        }

        System.out.println(enterprise.getLogo());
        int update = enterpriseMapper.update(enterprise);
        if (update == 0){
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    /**  <pre>
     *  企业列表-筛选查询
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @param name : 企业名称
     * @param username : 企业推介人
     * @param releaseTime : 发布时间
     * @param page : 从哪一页开始查询
     * @param limit : 每页显示的行数
     * @return : 返回封装后的结果对象
     * </pre>
     */
    @Override
    public ServiceMultiResult query(String classId, String name, String username, String releaseTime, Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        String startTime = null,endTime = null;
        if( releaseTime !=  null && releaseTime != ""){
            String[] times = releaseTime.split(" - ");
            startTime = times[0];
            endTime = times[1];
        }

        List<Map<String, String>> list = enterpriseMapper.queryEnterprises(classId, name, username, startTime, endTime);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }
    /**  <pre>
     *  企业审核列表-筛选查询
     * @author 马敏婷
     * @since 1.0
     * @since JDK 8.0
     * @param classId : 班级ID
     * @param name : 企业名称
     * @param username : 企业推介人
     * @param releaseTime : 发布时间
     * @param page : 从哪一页开始查询
     * @param limit : 每页显示的行数
     * @return : 返回封装后的结果对象
     * </pre>
     */
    @Override
    public ServiceMultiResult queryEnterprisesByAudit(String classId, String name, String username, String releaseTime, Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        String startTime = null,endTime = null;
        if( releaseTime !=  null && releaseTime.trim() != ""){
            String[] times = releaseTime.split(" - ");
            startTime = times[0];
            endTime = times[1];
        }
        System.out.println(classId + "--" + name + "--" + username + "--" + startTime + "--" + endTime);
        List<Map<String, String>> list = enterpriseMapper.queryEnterprisesByAudit(classId, name, username, startTime, endTime);
        Page<Map<String, String>> result = (Page<Map<String, String>>) list;
        if (page > result.getPages()){
            return new ServiceMultiResult(null);
        }
        return new ServiceMultiResult(result);
    }

    @Override
    public ServiceResult updateExamine(String id, String examine, String examineDetails) {
        return null;
    }

    @Override
    public ServiceResult delete(String id) {
        int delete = enterpriseMapper.delete(id);
        if (delete == 0) {
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }


    @Override
    public ServiceResult updateAuditStatus(String eid, Integer auditStatus, String auditMsg, HttpSession session) {
        int result = enterpriseMapper.updateAuditStatus(eid,auditStatus);
        EnterpriseAuditLog enterpriseAuditLog = new EnterpriseAuditLog();
        enterpriseAuditLog.setMsg(auditMsg);
        enterpriseAuditLog.setAuditDate(new Date());
//        enterpriseAuditLog.setCreatePerson(session.getAttribute("username").toString());
        enterpriseAuditLog.setCreatePerson("admin");
        enterpriseAuditLogMapper.add(enterpriseAuditLog);
        if (result == 0) {
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult updateStatus(String id, String status) {
        int result = enterpriseMapper.updateStatus(id,status);
        if (result == 0) {
            return ServiceResult.notFound();
        }
        return ServiceResult.success();
    }

    @Override
    public ServiceResult queryEnterprisesId(String id) {
        List<Map<String, String>> queryID = enterpriseMapper.queryEnterprisesId(id);
        if (queryID == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(queryID);
    }
    @Override
    public ServiceResult selectById(String id) {
        Enterprise enterprise = enterpriseMapper.selectById(id);
        if (enterprise == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(enterprise);
    }
    @Override
    public ServiceResult queryInfo(String userId) {
        System.out.println("+++"+userId);
        Enterprise enterprise = enterpriseMapper.queryInfo(userId);
        if (enterprise == null) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(enterprise);
    }

    @Override
    public ServiceResult updateInfo(Enterprise enterprise) {
        int updateInfo = enterpriseMapper.updateInfo(enterprise);
        if (updateInfo == 0) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(updateInfo);
    }

    @Override
    public ServiceResult insertInfo(Enterprise enterprise) {
        int insertInfo = enterpriseMapper.insertInfo(enterprise);
        if (insertInfo == 0) {
            return ServiceResult.notFound();
        }
        return ServiceResult.of(insertInfo);
    }

}