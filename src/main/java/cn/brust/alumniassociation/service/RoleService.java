package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author 马敏婷
 * @since 1.0
 * @since jdk8
 */
public interface RoleService {
    ServiceResult selectById(String id);

    ServiceMultiResult selectAll(Integer page,Integer limit,String classId);

    ServiceResult update(Role role,String id);

    ServiceResult delete(String id);

    ServiceResult add(Role role);

    ServiceMultiResult selectPermissionByRoleId(String id);

}
