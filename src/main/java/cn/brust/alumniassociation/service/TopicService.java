package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;

/**
 * Created by Administrator on 2018/8/2 0002.
 */
public interface TopicService {

    /**  <pre>
     *  后台管理 -  查询所有分类（分页查询）
     * @author 马丹萍
     * @since 1.0
     * @since JDK 8.0
     * @param page :分页
     * @param limit :分页
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     * </pre>
     */
    ServiceMultiResult getTopics(Integer page, Integer limit);


    /**
     * 提供给前台下拉框使用
     * @return
     */

    ServiceMultiResult getAllTopic();
    /**
     * 更改分类显示状态
     * @param id 分类ID
     * @param enable 0显示 1不显示
     * @return
     */
    ServiceResult updateEnable(Integer enable, String id);


    /**
     * 软删
     * @param id 分类ID
     * @return
     */
    ServiceResult updateDel(String id);

    /**
     * 添加分类
     * @param title :标题
     * @param enable :显示状态
     * @param sort :排序
     * @param title :标题
     * @return
     */
    ServiceResult add(String id,String title, Integer sort, Integer enable);

    /**
     * 根据分类ID查询分类详情
     * @param id 分类ID
     * @return
     */
    ServiceResult queryTopicInfoById(String id);

    /**
     * 更改分类
     * @param id :分类ID
     * @param enable :显示状态
     * @param sort :排序
     * @param title :标题
     * @return
     */
    ServiceResult updateTopic(Integer enable, String id, Integer sort, String title);

    ServiceResult selectTopic();
}
