package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import org.springframework.stereotype.Service;

/**
 * @author
 * @since
 * @since
 */
public interface UserAuditLogService {
    ServiceResult addAuditLog(String id, String userId, String msg, Integer auditStatus, String auditPerson, String createDate);

    /**
     * 会员审核- 审核详细信息
     *
     * @author 王永泽
     * @since 1.0
     * @since JDK 8.0
     * @param userId      会员id
     * @return      审核记录
     */
    ServiceMultiResult queryUserAuditLog(String userId);
}
