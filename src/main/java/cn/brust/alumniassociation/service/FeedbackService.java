package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Feedback;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.ibatis.annotations.Param;

/**
 * <pre>
 *     @author
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */
public interface FeedbackService {

    /**
     * 分页展示反馈列表
     * @param limit 每页显示的条数
     * @param page  第几页
     * @return
     */
    ServiceMultiResult queryFeedback(Integer limit, Integer page);


    /**
     * 删除反馈
     * @param id
     * @return
     */
    ServiceResult delete(Integer id);

    /**
     * 处理反馈
     * @param id
     * @param handle 处理备注
     * @return
     */
    ServiceResult handle(Integer id, String handle);

    /**
     * 添加反馈
     * @param feedback
     * @return
     */

    ServiceResult add(String opinion, String userId);

    /**
     *
     * @param releaseTime
     * @param page
     * @param limit
     * @return
     */
    ServiceMultiResult queryAll(String releaseTime, Integer page, Integer limit);
}
