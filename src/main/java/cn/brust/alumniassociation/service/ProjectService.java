package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Project;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Brust 2018/7/25 21:10
 */
public interface ProjectService {
    /**
     * 查询项目(分页)
     * @param page 页数
     * @param limit
     * @param classId 班级ID
     * @return
     */
    ServiceMultiResult getProjects(Integer page, Integer limit, String classId);

    /**  <pre>
     *  后台管理 - 项目列表
     *  根据关键字查询,如果条件为空即为无条件查询 仅分页
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param page :分页
     * @param limit :分页
     * @param classId : 班级ID 可为空
     * @param budget : 项目预算 可为空
     * @param nameOrMobile : 会员名称/手机号 可为空
     * @param startTime : 发布时间-开始 可为空
     * @param endTime : 发布时间-结束 可为空
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     * </pre>
     */
    ServiceMultiResult getProjectsByKeyWord(Integer page, Integer limit, String classId, Integer budget, String nameOrMobile, String startTime, String endTime);    /**  <pre>

    /**  <pre>
     *  后台管理 - 项目审核列表
     *  根据关键字查询,如果条件为空即为无条件查询 仅分页
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param page :分页
     * @param limit :分页
     * @param classId : 班级ID 可为空
     * @param budget : 项目预算 可为空
     * @param nameOrMobile : 会员名称/手机号 可为空
     * @param startTime : 发布时间-开始 可为空
     * @param endTime : 发布时间-结束 可为空
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     * </pre>
     */
    ServiceMultiResult getAuditProjectsByKeyWord(Integer page, Integer limit, String classId, Integer budget, String nameOrMobile, String startTime, String endTime);
    /**
     * 查询用户所有项目
     * @param page 页数
     * @param limit
     * @param userId 用户ID
     * @return
     */
    ServiceMultiResult getProjectsByUserid(Integer page, Integer limit, String userId);

    /**  <pre>
     *  添加项目
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param project : 项目
     * @param path : 文件路径
     * @return : cn.brust.alumniassociation.base.ServiceResult
     * </pre>
     */
    ServiceResult addProject(Project project,String path);
    /**
     * 根据项目ID查询项目详情
     * @param id 项目ID
     * @return
     */
    ServiceResult getProjectById(String id);

    /**
     * 根据项目ID查询项目跟进状态
     * @param id 项目跟进
     * @return
     */
    ServiceResult getProjectRecord(String id);

    /**  <pre>
     *  后台管理 - 项目列表 - 项目跟进 - 录入进度
     * @author 郭逸峰
     * @since 1.0
     * @since JDK 8.0
     * @param id :项目ID
     * @param status :项目状态 0未发布 1已取消 2发布中 3跟进中 4已签单
     * @param msg :记录
     * @return : cn.brust.alumniassociation.base.ServiceResult
     * </pre>
     */
    ServiceResult updateProjectRecord(String id,Integer status,String msg);
    /**
     * 更改项目状态
     * @param id 项目ID
     * @param status 项目状态 0未发布 1已取消 2发布中 3跟进中 4已签单
     * @return
     */
    ServiceResult updateStatus(String id, Integer status);

    /**
     * 修改项目
     * @param project
     * @return
     */
    ServiceResult updateProjectById(Project project);

    /**
     * 软删
     * @param id 项目id
     * @return
     */
    ServiceResult deleteProjectById(String id);

    /**
     * 修改项目审核状态并添加审核记录
     * @param id
     * @param auditStatus 0未审核 1审核不通过 2审核通过
     * @param msg 审核详情
     * @return
     */
    ServiceResult updateAuditStatu(String id, Integer auditStatus, String msg);

    /**
     * 查看项目审核记录
     * @param id 项目ID
     * @return
     */
    ServiceResult getAuditLog(String id);
    /**
     * 添加项目浏览记录 +1
     * @param id
     * @return
     */
    ServiceResult addVisit(String id);

    ServiceResult wxUpdateProject(Project project);
}
