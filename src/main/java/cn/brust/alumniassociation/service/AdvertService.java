package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceMultiResult;
import cn.brust.alumniassociation.base.ServiceResult;
import cn.brust.alumniassociation.entity.Advert;

import java.util.List;

/**
 * <pre>
 *     @author 郑锦波、孙超
 *     @since   1.0
 *     @since   JDK 8.0
 * </pre>
 */

public interface AdvertService {

    /**
     *  查询所有广告信息(weixin)
     * @author 孙超
     * @since 1.0
     * @since JDK 8.0
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult
     */
    ServiceMultiResult queryAdverts();
    
    /**  <pre>
     *  查询所有广告信息
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @return : 返回广告对象集合
     * </pre>
     */
    List<Advert> findAll();
    
    /**  <pre>
     *  查询所有广告信息(分组)
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param page 页数
     * @param limit 条数
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult;
     * </pre>
     */
    ServiceMultiResult findAllLimit(Integer page,Integer limit);

    /**
     * 分页查询广告列表
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param limit 每页显示的行数
     * @param page  返回第几页
     * @return : cn.brust.alumniassociation.base.ServiceMultiResult;
     */
    ServiceMultiResult queryAdvert(Integer limit, Integer page);

    /**
     * 修改广告状态
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param id 编号
     * @param states : 上线/下线
     * @return 
     */
    ServiceResult updateStates(Integer id, Integer states);

    /**
     * 根据id删除广告信息(软删)
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param id : 编号
     * @return
     */
    ServiceResult delAdvert(int id);

    /**
     * 添加广告
     * 必须字段 - 广告名称、广告位置、广告图片、广告链接、广告备注、
     * 上线/下线、是否置顶
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param advert : 广告对象
     * @return
     */
    ServiceResult addAdvert(Advert advert);

    /**
     * 根据广告名称和广告位置(单独位置)查询
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param title 标题
     * @param site 位置
     * @return
     */
    ServiceMultiResult vagueQuery(String title,Integer site,Integer page,Integer limit);

    /**
     * 根据广告名称和广告位置(全部)查询
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param title 广告名称
     * @return
     */
    ServiceMultiResult vagueQueryAll(String title,Integer page,Integer limit);

    /**
     * 根据广告id查询
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param id 编号
     * @return
     */
    Advert queryById(Integer id);

    /**
     * 根据广告id 修改广告信息(广告名称、地址、连接、置顶、备注)
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * @param title 名称
     * @param site  地址
     * @param cover 广告图片
     * @param url   链接
     * @param desc  备注
     * @param isStatus：广告状态
     * @param isTop：是否置顶
     * @param id    编号
     * @return
     */
    ServiceResult updateAdvert(String title,Integer site,String cover,String url,String desc,Integer isStatus,Integer isTop,Integer id);

    /**
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * 根据id置顶广告
     * @param id
     */
    ServiceResult settop(Integer id);

    /**
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * 根据id取消置顶
     * @param id
     */
    ServiceResult canceltop(Integer id);

    /**
     * @author 郑锦波
     * @since 1.0
     * @since JDK 8.0
     * 查询置顶条数
     */
    Advert queryCount();
}