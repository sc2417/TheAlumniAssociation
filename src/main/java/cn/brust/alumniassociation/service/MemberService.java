package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceResult;

/**
 * @author
 * @since
 * @since
 */
public interface MemberService {
    ServiceResult updatePasswd(String email, String lodPwd,String newPwd);

    ServiceResult findMember(String email,String  pwd);

    ServiceResult updateAuatar(String email,String avatar);

    ServiceResult findMemberemail(String email);
    /**
     * 登录
     * @param mobile
     * @param passwd
     * @param classId
     * @return
     */
    ServiceResult login(String mobile, String passwd, String classId);
}
