package cn.brust.alumniassociation.service;

import cn.brust.alumniassociation.base.ServiceResult;
import org.apache.ibatis.annotations.Param;

/**
 * Created by Administrator on 2018/8/16 0016.
 */
public interface EnrollService {
    ServiceResult queryById(String wxId, String acId);
    int activityEnroll(String wxId, String acId);
}
