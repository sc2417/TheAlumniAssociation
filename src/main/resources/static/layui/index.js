var page=1;
var limit=10;

layui.use(['laydate', 'laypage', 'layer', 'table'], function(){
    var laydate = layui.laydate //日期
        ,laypage = layui.laypage //分页
        ,layer = layui.layer //弹层
        ,table = layui.table //表格
        ,element = layui.element //元素操作
        , $ = layui.jquery
        , from = layui.from;

    //执行一个 table 实例
    table.render({
        elem: '#tab'
        ,url: '/advert/findAll'
        ,page: true //开启分页
        ,id:"tab"
        ,cols:[[
            {type:'checkbox',width:50}
            ,{ field:'id', title: '编号', sort: true,align:'center'}
            ,{field: 'title', title: '广告名称',align:'center'}
            ,{field: 'site', title: '广告位置',align:'center',
                templet:function (data) {
                    if (data.site == 1) {
                        return "<span>APP首页顶部</span>";
                    } else if (data.site == 2) {
                        return "<span>APP首页中部</span>";
                    } else if(data.site == 3){
                        return "<span>APP首页底部</span>";
                    }
                }
            }
            ,{field: 'cover',align:'center' ,title: '广告图片',templet:'<div><img src="{{d.cover}}" style="width: 110px;height: 100px"></div>'}
            ,{field: 'cteateDate', title: '上线时间',align:'center'}
            ,{field: 'isStatus', title: '上线/下线',align:'center',
                templet:function (data) {
                    var str = ['open','close'];
                    if(data.isStatus == 0){
                        return "<input type='checkbox' checked='' name='"+str[data.isStatus]+"' lay-skin='switch' lay-filter='switchTest'/>";
                    }else{
                        return "<input type='checkbox' name='"+str[data.isStatus]+"' lay-skin='switch'/>";
                    }
                }
            }
            ,{field: 'point', title: '点击次数',align:'center'}
            ,{fixed: 'right',title:'操作',align:'center', toolbar: '#barDemo'}
        ]]
        ,response: {
            statusName: 'code' //数据状态的字段名称，默认：code
            ,statusCode: 200 //成功的状态码，默认：0
            ,msgName: 'msg' //状态信息的字段名称，默认：msg
            ,countName: 'count' //数据总数的字段名称，默认：count
            ,dataName: 'data' //数据列表的字段名称，默认：data
        }
    });

    $(".layui-btn").on("click",function () {
        table.reload("tab",{
            url:'/advert/vagueQuery',
            where :{
                title:$("#demoReload").val(),
                site:$("#site").val()
            }
        });
    });

    table.on('tool(list-tab)',function(obj){
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）

        if(layEvent == 'del'){
            layer.confirm('确定删除此行？',function(){
                window.location.href = "/advert/del?id="+data.id;
            });
        }else if(layEvent == 'settop'){
            layer.confirm('确定置顶此行？',function(){
                window.location.href = "/advert/settop?id="+data.id;
            });
        }else if(layEvent == 'canceltop'){
            layer.confirm('确定取消此行置顶？',function(){
               window.location.href = "/advert/canceltop?id="+data.id;
            });
        }
    });
    var $ = layui.$, active = {
        getCheckData: function() {
            var checkStatus = table.checkStatus('tab'),
                data = checkStatus.data;
            layer.alert('选中了：' + data.length + '个');
        }
    }
    $('.layui-input-inline .layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
});