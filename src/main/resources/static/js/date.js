Date.prototype.format = function(a) {
    var d = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        S: this.getMilliseconds()
    };
    /(y+)/.test(a) && (a = a.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length)));
    for (var c in d)
        (new RegExp("(" + c + ")")).test(a) && (a = a.replace(RegExp.$1, 1 == RegExp.$1.length ? d[c] : ("00" + d[c]).substr(("" + d[c]).length)));
    return a
}